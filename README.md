# O, Toto!
```
Voglio andare a vivere in campagna
Voglio la rugiada che mi bagna
Ma vivo qui in città, e non mi piace più
In questo traffico bestiale
La solitudine ti assale e ti butta giù
Che bella la mia gioventù

Voglio ritornare alla campagna
Voglio zappar la terra e fare legna
Ma vivo qui in città, che fretta sta tribù
Non si può più comunicare
Qui non si può più respirare il cielo non e più blu
E io non mi diverto più

Al mio paese si balla, si balla, si balla
In questa notte un po' gitana di luna piena
Al mio paese c'è festa che festa che festa
Tutti in piazza ed affacciati alla finestra
E un sogno e niente più
Che bella la mia gioventù

Io che sono nato in campagna
Ricordo nonno Silvio e la vendemmia
Ma vivo qui in città, dove sei nata tu
Ma la nevrosi e generale
La confusioni che ti assale ti butta giù
E io non mi diverto più

Al mio paese si balla, si balla, si balla
In questa notte un po' ruffiana di luna piena
Al mio paese c'è festa che festa che festa
Tutti in piazza ed affacciati alla finestra
Rivoglio il mio paese la chiesa le case
E la maestra che coltiva le sue rose
Rivoglio il mio paese, la vecchia corriera
Che risaliva lenta sbuffando a tarda sera
Ma e solo un sogno e niente più
Che bella la mia gioventù

Al mio paese si balla si balla si balla
Dalla notte fino all'alba con la luna piena
Rivoglio il mio paese, quella gente che respira amore
E quello stagno che per noi bambini sembrava il mare

Al mio paese c'è festa che festa che gran festa
Tutti vestiti per bene un po' fuori di testa
Rivoglio il mio paese, la giostra il barbiere
E il dottore di tutti, il prete e il carabiniere
Ma e solo un sogno e niente più
Che bella la mia gioventù
```
