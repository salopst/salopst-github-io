---
title: "{{ .Name  | humanize | title}}"
slug: {{ .Name }}
author: {{ .Site.Author.name }}
origin: hugo
date: {{ .Date }}
lastMod: {{ .Date }}
draft: false
toc: false
noLicense: false
weight: 1001
images: null
categories:
  - Uncategorized
tags:
  - untagged
description: "DESCRIPTION_CHANGE_ME"
---