---
author: admin
origin: wordpress
comments: true
date: 2013-04-02T00:16:02+00:00
layout: post
slug: all-about-the-great-wall
title: All about the Great Wall (by Sam)
wordpress_id: 408
---

{{< image src="/wp-uploads/Great-Wall-sleeping-dragon.jpg" alt="Great Wall Sleeping Dragon" position="center" style="border-radius: 50px;" >}}

This is a picture of my aunt Beth on a broken part of the Great Wall. My mom took this picture. This is the wall at [Shanhaiguan](http://www.travelchinaguide.com/china_great_wall/scene/hebei/shanhaiguan/). That is where the wall meets the sea. This does not look like pictures of the Great Wall I have seen because it does not really look like the Great Wall. It does not have the watch towers or the fence-like things on the sides of the wall. It looks like an ordinary rock bunched into a wall. My mom used to have a piece of the wall from here, but she can't find it now.

{{< image src="/wp-uploads/Great-Wall-Auntie-Beth.jpg" alt="Auntie Beth on the Great Wall of China" position="center" style="border-radius: 50px;" >}}

This is a picture of the Great Wall of China that is not crumbling. This is on a piece of money. I think that the Ming built the wall that is on the picture of the wall on the money.

{{< image src="/wp-uploads/Great-Wall-money.jpg" alt="Great wall money" position="center" style="border-radius: 50px;" >}}

This is an old map of China showing the Great Wall and it winds around the top of China. And you can see how it kept the Mongols out.

{{< image src="/wp-uploads/Great-Wall-old-map.jpg" alt="Great Wall old map" position="center" style="border-radius: 50px;" >}}

My grandmother also went to the Great Wall. She went to a different part. She was sick and the people on the bus said she needed a partner to take her around the wall. My grandmother wanted to go one way and her partner wanted to go another way. My grandmother and her partner lost each other.

My Auntie Beth says "there's a Chinese saying that you're not a man until you go to the Great Wall, and yet your uncle, who grew up in China, has never been." One day I would like to climb the Great Wall.

{{< image src="/wp-uploads/Great-Wall-drawing.jpg" alt="Me on the Great Wall of China" position="center" style="border-radius: 50px;" >}}


### ~Sam Yearl
