---
title: Gāyatrī Mantra (Sāvitri Mantra)
slug: gayatri-mantra
author: yearluk
origin: hugo
date: 2021-06-30T13:06:15+01:00
lastMod: 2021-09-01T02:30:00+00:00
draft: false
noLicense: false
weight: 1001
categories:
  - Blog
  - Language
  - Media
tags:
  - hindi
  - hugo
  - song
  - lyrics
  - music
  - mantra
summary: >-
  The Gāyatrī Mantra, also known as the Sāvitri Mantra, is a highly revered
  mantra from the Rig Veda (Mandala 3.62.10),[1] dedicated to the Vedic deity
  Savitr
description: >-
  Gāyatrī Mantra, also known as the Sāvitri Mantra, is a highly revered mantra
  from the Rig Veda (Mandala 3.62.10), dedicated to the Vedic deity Savitr.
---


Mera bhai from The Sanskrit Channel has hot takes on this mantra: https://www.youtube.com/watch?v=mxLv8KnBfjg

Text from the youtube video scanned using [Text Scanner (OCR)
by Govarthani Rajesh](https://apps.apple.com/us/app/text-scanner-ocr/id1225032527) for iOS. Text is near perfcet despite it coming from a phone's picture of a laptop screen. Also manages to capture Denanagaari pretty well, maybe not surprising give that the developer is an Indian.

Scans are limited to three per day for the non subscription version. Shame that there is not just a 'buy it' option since that would be worth a fair chunk, but subscription models can just suck on a bag of roman numeral 509! (don't factorialise me)


{{<yt-media id="6Kb0q9J8lPA" yt_start="10" autoplay="true">}}


```devanagari
ॐ भूर्भुवः स्वः 
तत्सवितुर्वरेण्यं 
भर्गो देवस्य धीमहि 
धियो यो नः प्रचोदयात् ॥
```

In IAST:

>#### oṃ bhūr bhuvaḥ svaḥ
>#### tat savitur vareṇyaṃ
>#### bhargo devasya dhīmahi
>#### dhiyo yo naḥ pracodayāt
>##### ~~ Rigveda 3.62.10

----
----
----

### 1. OM 
The Supreme name of God. The single word OM (or AUM) is known as the pranava mantra and is perhaps the most significant mantra in hinduism. It is known as the primary (or seed) or pranava mantra (or bija mantra) because it contains the seed for all other mantras within itself. OM is also referred to in some texts as the primordial sound.

### 2. BHOOR  
Firstly, the word **Bhoor** implies existence. God is self-existent and independent of all. He is eternal and unchanging. Without beginning and without end, God exists as a continuous, permanent, constant entity. Secondly, the word Bhoor can also mean the Earth, on which we are born and sustained. God is the provider of all, and it is through His divine will that we our blessed with all that we require to maintain us through our lives. Finally, Bhoor signifies Prana, or life (literally, breath).

### 3. BHUVAH 
**Bhuvah** describes the absolute Consciousness of God. God is self-Conscious as well as being Conscious of all else, and thus is able to control and govern the Universe. Also, the word Bhuvah relates to God's relationship with the celestial world. It denotes God's greatness -  greater than the sky and space, He is boundless and unlimited. Finally, Bhuvah is also indicative of God's role as the remover of all pain and sufferings (Apaana).

### 4. SVAH  
**Svah** indicates the all-pervading nature of God. He is omnipresent and pervades the entire multi-formed Universe. Without Form Himself, He is able to manifest Himself through the medium of the physical world, and is thus present in each and every physical entity. In this way, God is able to interact with the Universe created by Him, and thus sustain and control it, ensuring its smooth and proper running and function.

### 5. TAT  
Literally, this word means "that", being used in Sanskrut to denote the third person. It is also mentioned in the Bhagavad Gita by Sri Krishna Himself, where He implies the selfless nature of the word. Being used in the third person, the word had implicit in it the idea of selflesness. Tat then is used here in the Gayatri Mantra to indicate that the worshipper is referring to [that] God, and that the praise being offered to God in the prayer is purely directed towards Hom, without thought of gaining and benefit from that praise.

### 6. SA-VI-TUR  
**Savita**, from which Savitur is derived, is another name of God, this being the reason that the Gayatri Mantra is often known as the Savitri Mantra. The implication of Savita is of God's status as the fountain, the source of all things. It is through His Divine Grace that the Unniverse exists, and so this word sums up the Mahavyahriti, by describing God's ability to create the Universe and sustain it, as well as, at the right time, bring about its dissolution.

### 7. VA-RE-NY-AM  
**Varenyam** signifies our acceptance of God, and can be translated as "who is worthy". Ever ready to obtain all the material riches of the world, more often than not, they are a dissapointment once they have been acheived. God however is the one who, once realised and acheived, has the ability to truly satisfy. We therefore accept Him as the Highest reality, and it is to Him that we dedicate our efforts.

### 8. BHAR-GO  
**Bhargo** is taken to mean the Glorious Light that is God's love and power. It indicates His complete purity-- being absolutely pure Himself, God also has the ability to purify those that come into contact with Him. 

### 9. DE-VAS-YA
The word Deva, from which this word is derived, has been translated by different people in different ways. It is generally thought of as  simply "God". However, it's meaning is more complex than that. Deva, which forms the root of "Devata" and "Devi", means "quality" or "attribute", and can be thought of as another word for "Guna".

### 10. DHI-MA-HI
Meaning to meditate and focus our mind on God. Meditation on God implies that we remove all other thoughts from our mind, since thoughts of the world render our mind impure, and thus we are unable to conceptualize the absolute purity of God. We must be able to concentrate, and dorect our mental energoes to the task in hand-- which is communion with God.

### 11. DHI-YO
sanskrit for "intellect", this is the essence of this part of the Gyatri Mantra. Having firmly set God in our hearts, we must now try to mphasize His presenceand influence on our mind and intellect.

Material prosperity has no true meaning for the person who is truly devoted to God. Pain and suffering are of no consequence to him as, touched by God, he is imbued with God's own Divine Bliss, and all wordly sorrows pale to nothingness in comparison. However, still the individual must live in the world.

### 12. YO
Meaning "Who" or "That", **Yo** signifies yet again that it is not to anyone else we direct these prayers, but to God alone. Only God is worthy of the highest adoration, only God is perfect and free from all defects. It is God to Whom we offer these prayers.

### 13. NAH
**Nah** means "Ours" and signifies the selflessness of the reques we make of God in this part of the Gyatri Mantra. We offer this prayer, and make the request of God, not simply for ourselves, but for the whole of humanity. We seek the uplift of the whole of society. Hindu philosophy has since the beginning recognized the concept of "Vasudhaiva Kutumbakam" -- "The whole world is one big family". Thus, we pray not just for ourselves, but for each and every memeber of that great family, that we may all benefit from the greatness and generosity of the All-loving God.

### 14. PRA-CHO-DA-YAT
**Prachodayat**, the final word of the Gyatri Mantra, round off the whole mantra, and completes the request we make of God in this final part. This word is a request from God, i which we ash Hin for Guidance, and Inspiration. We ask that, by showing us His Divine and Glorious Light (cf. BHARGO), He removes the darkness of Maya from our paths, that we are able to see the way, and in this manner, we ask Him to direct our energies in the right way, guiding us through the chaos of this world, to find sanctuary in the peace and tranquility of God Himself, the root of all Happiness, and the source of true Bliss.