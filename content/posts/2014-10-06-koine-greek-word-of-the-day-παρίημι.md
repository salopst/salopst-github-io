---
author: kword
comments: true
date: 2014-10-06T13:15:00+00:00
layout: post
link: http://stephen.yearl.us/koine-greek-word-of-the-day-παρίημι
slug: koine-greek-word-of-the-day-παρίημι
title: 'Koinē Greek Word of the Day: παρίημι'
wordpress_id: 773
categories:
- Language
tags:
- Greek
- Bible
- words
- koine
- κοινή
---

# παρίημι

pariémi
Verb
*I let pass, neglect, omit, disregard, (b) I slacken, loosen; pass: I am wearied.*
Strong's: **3935**

2 occurrences in GNT


```text
παρειμένας (V-RPM/P-AFP), Hebrews 12:12
παρεῖναι (V-ANA), Luke 11:42
```

# Luke 11:42

```text
Ἀλλὰ οὐαὶ ὑμῖν τοῖς Φαρισαίοις, ὅτι ἀποδεκατοῦτε τὸ ἡδύοσμον καὶ τὸ πήγανον καὶ πᾶν λάχανον, καὶ παρέρχεσθε τὴν κρίσιν καὶ τὴν ἀγάπην τοῦ θεοῦ· ταῦτα δὲ ἔδει ποιῆσαι κἀκεῖνα μὴ παρεῖναι.
```
~~SBL Greek New Testament (SBLGNT)

```text
sed vae vobis Pharisaeis quia decimatis mentam et rutam et omne holus et praeteritis iudicium et caritatem Dei haec autem oportuit facere et illa non omittere
```
~~Biblia Sacra Vulgata (VULGATE)


```text
But woe unto you, Pharisees! for ye tithe mint and rue and all manner of herbs, and pass over judgment and the love of God: these ought ye to have done, and not to leave the other undone.
```
~~King James Version (KJV)

* * *
kwords 0.3
