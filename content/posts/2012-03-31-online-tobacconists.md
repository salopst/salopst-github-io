---
author: admin
origin: wordpress
comments: true
date: 2012-03-31T18:45:02+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: online-tobacconists
title: Online tobacconists
wordpress_id: 202
categories:
- Uncategorized
tags:
- pipes
- tobacco
---

## Online retailers I have used thus far:

## UK

  * James Barber ... [http://www.smoke.co.uk/](http://www.smoke.co.uk/)

  * Black Swan Shoppe ...[www.thebackyshop.co.uk](http:// <a href=)

  * Blakemar Briars (stem replacement) ... [http://www.blakemar.co.uk/](http://www.blakemar.co.uk/)

  * [http://www.the-tobacconist.co.uk/tobacco/](http://www.the-tobacconist.co.uk/tobacco/)

  * [http://www.smoke-king.co.uk/acatalog/home-page.html](http://www.smoke-king.co.uk/acatalog/home-page.html)

  * [http://www.mysmokingshop.co.uk/index2.php?mod=catman&cat=69](http://www.mysmokingshop.co.uk/index2.php?mod=catman&cat=69)

  * [http://www.cigar-connoisseur.co.uk/acatalog/Tobacco.html](http://www.cigar-connoisseur.co.uk/acatalog/Tobacco.html)


## USA

  * Pipes and Cigars ... [http://www.pipesandcigars.com/](http://www.pipesandcigars.com/)

  * Connecticut Valley Tobacconists ... [http://www.cvtobacco.com/](http://www.cvtobacco.com/)

  * L. J. Peretti (Boston, MA) ..[ ](http://www.ljperetti.com/tobacco.html)[http://www.ljperetti.com/tobacco.html](http://www.ljperetti.com/tobacco.html)

  * [http://www.smokingpipes.com](http://www.smokingpipes.com)

  * [http://www.4noggins.com](http://www.4noggins.com)

  * [http://www.marscigars.com](http://www.marscigars.com)

  * [http://www.jrcigars.com](http://www.jrcigars.com)

  * [http://cigarsandpipes.com/tobacco](http://cigarsandpipes.com/tobacco)


## DE

  * [http://www.danpipe.de](http://www.danpipe.de)
