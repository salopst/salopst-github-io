---
author: salopst
origin: posterous
comments: true
date: 2011-06-18T18:11:23+00:00
lastMod: 2021-08-31T06:23:00+01:00
layout: post
slug: he-net-ipv6-certification-questions-and-answers-newbie
title: HE.net IPV6 Certification questions and answers -- \#0 Newbie
wordpress_id: 141
categories:
- networking
- tech
tags:
- ipv6
- networking
- he.net
---


## [http://ipv6.he.net/certification/](http://ipv6.he.net/certification/)

	
  * [1. Enthusiast Test](/he-net-ipv6-certification-questions-and-answers-enthusiast/)

	
  * [2. Administrator Test](/he-net-ipv6-certification-questions-and-answers-administrator/)

	
  * [3. Professional Test](/he-net-ipv6-certification-questions-and-answers-professional/)

	
  * [4. Guru Test](/he-net-ipv6-certification-questions-and-answers-guru/)

	
  * [5. Sage Test](/he-net-ipv6-certification-questions-and-answers-sage/)


## Covers some of the basics; an introduction as the name suggests


#### 1. How many bits are in an IPv6 address?
- 128   ✅
- 64
- 32
- 48

#### 2. Which of the following choices is a valid IPv6 address?
- 2001:0DB8:HE:6939::1
- ipv6.example.com
- 192.168.0.1
- 2001:0DB8::0::FF2e:1
- 2001:0DB8:0:FF2E::1   ✅

#### 3. How many /64 subnets are available in a /48 prefix?
- 256
- 340,282,366,920,938,463,463,374,607,431,768,211,456
- 18,446,744,073,709,551,616
- 65536   ✅

#### 4. How many available IPv6 addresses are there in a /64 allocation?
- 18,446,744,073,709,551,616   ✅
- 65536
- 4,294,967,296
- 2^128

#### 5. What operating systems currently support IPv6?
Linux
BSD
MacOSX
Vista/Windows 2008
All of the these support IPv6   ✅
