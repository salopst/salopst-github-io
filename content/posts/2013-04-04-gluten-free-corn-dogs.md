---
author: admin
origin: wordpress
comments: true
date: 2013-04-04T22:32:18+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: gluten-free-corn-dogs
title: Gluten Free Corn Dogs
wordpress_id: 455
categories:
- Gluten free
- Recipes
tags:
- food
- recipes
---

{{< image src="/wp-uploads/GF-corn-dogs-fihished.jpg" alt="Masa dogs" position="center" style="border-radius: 50px;" >}}

Hmmm. 45 minutes until soccer/football practice. Warm-ish spring evening. We need something filling, something quick, so why not try something I've never made before: corn dogs. So, with Simply Red's [Fairground](https://www.youtube.com/watch?v=KiRyiVgWj6g) on loop in the background we begin...

Slice 4 lamb sausages lengthways, and fry. Remove them before they are fully cooked, and set aside to cool on an absorbent material to soak up the excess fat.

{{< image src="/wp-uploads/GF-corn-dog-sausages.jpg" alt="Sausages" position="center" style="border-radius: 50px;" >}}

That was easy. Actually it is not _that_ easy; it depends on where you live. Getting decent sausage can be devilishly hard sometimes.

## Now the wrapping:
- 250g [Maseca masa harina](http://www.mimaseca.com/es/productos-maseca/d/maseca-maiz-regular/1)
- 250-ish ml chicken stock
- herbs and spice to taste


I mixed the harina with freshly ground black pepper, smoked paprika, salt and thyme, then added the stock (at room temperature) bit-by-bit until I had a masa/dough with roughly the consistency of kids' play dough. This turned out to be about 250ml, but this will vary, obviously, on the ambient humidity and such.

Take one young child, and have him press the masa around the sausages:

{{< image src="/wp-uploads/GF-corn-dogs-wrapping.jpg" alt="Wrapping the dog" position="center" style="border-radius: 50px;" >}}

Next we'll deep-fry the "wrapped" sausages. I used peanut oil as it has a higher smoking point than other common oils, but I guess whatever you have to hand will do...sunflower, rape seed (canola), anything except perhaps olive oil. (Yes that's a bottle of 95% ABV ethanol in the foreground-- THE best grease/crayon/marker remover!)

{{< image src="/wp-uploads/GF-corn-dogs-ready-for-frying.jpg" alt="Dogs ready for deep frying" position="center" style="border-radius: 50px;" >}}

-----
-----
-----

{{< image src="/wp-uploads/GF-corn-dogs-frying.jpg" alt="" position="center" style="border-radius: 50px;" >}}

And that, we thought, was that. Except eldest child had a wonderful idea. The wrapping around the sausage seems to quite easily slip off, not so much that one cannot eat these dogs off of chopsticks a lá fairground dogs, but easily enough for them to keep their shape, resulting in a semi-closed tube of cooked masa. Fill that tube with ice cream/sweet of choice and one has.... Mexican [cannoli](http://en.wikipedia.org/wiki/Cannoli)!!

{{< image src="/wp-uploads/GF-corn-dogs-mexican-cannoli.jpg" alt="Mexican cannoli" position="center" style="border-radius: 50px;" >}}

All in all, for something so fast and so simple, the results were fantastic. The squeals of delight coming from the boys when they discovered that the masa wrapping would serve as a ice cream cone of sorts was nothing short of fantastic. We'll be making this again!
