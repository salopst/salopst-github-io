---
author: "yearluk"
origin: "hugo"
title: "Science jokes"
date: 2012-05-08T15:33:57+01:00
lastMod: 2021-09-01T02:30:00+01:00
draft: false
noLicense: false
weight: 1001
summary: A sucking fummary of the most awesomest collection of science-based jokes on the Interwebz...in a SFW
categories:
- Humour
- Random
tags:
- jokes
- science
- orgmode
---


- You matter!    
Unless you multiply yourself by the speed of light...  
then you energy!  

- Just got my ticket to the Fibonacci convention!  
I hear this year is going to be as big as the last 2 put together.  

- A hundred kilopascals walk into a bar...  

- They only use nitrogen when all the other inert gases argon  

- Velociraptor = Distraptor / Timeraptor  

- Imagine if Americans switched from pounds to kilograms overnight...  
There would be mass confusion

- I made a graph showing my past relationship...  
It has an ex-axis and a why-axis

- Q: What's new?  
A: C over lambda.

- Why do mathematicians confuse Halloween and Christmas?  
Because 31 Oct = 25 Dec.   

- What does the “B” in Benoit B Mandelbrot stand for?  
Benoit B Mandelbrot.

- Did you hear that joke about sodium hypobromite?  
NaBrO!

- Why is 69 afraid of 70?  
Because they once had a fight and 71

- How can you tell a chemist from a plumber?  
Ask them to pronounce "unionized."

- Did you hear about the man who got cooled to absolute zero?  
He’s 0K now.  

- A programmer’s wife tells him: “Run to the store and pick up a loaf of bread. If they have eggs, get a dozen.”  
The programmer comes home with 12 loaves of bread.  

- "Dad why is my sister's name *Rose*?"  
"Because your Mother loves Roses"  
"Thanks, Dad"  
"No problem, Laminar Flow"﻿

- A photon checks into a hotel and the porter asks him if he has any luggage...   
The photon replies: “No, I’m travelling light.”  

- A TCP packet walks into a bar, and says to the barman: “Hello, I’d like a beer.” The barman replies: “Hello, you’d like a beer?” “Yes,” replies the TCP packet, “I’d like a beer.”  

- Pavlov is enjoying a pint in the pub. The phone rings. He jumps up and shouts: “Hell, I forgot to feed the dog!”

- I was sitting at a cafe, and I noticed in the display case behind me someone had put pasta and antipasto right next to each other...  
Are they out of their Vulcan minds?

- There are 10 types of people in this world. Those that know binary, and those that don’t.  

- if you are not part of the solution...  
you are part of the precipitate

- I heard that oxygen and magnesium hooked up...  
I was like OMg.  

- What's the difference between a wet brunette and a wet blonde?  
One smells like H2O, the other smells like H2O2.

- I went to a magnet school for bipolar students.

- An photon is driving down a motorway, and a policeman pulls him over...  
The policeman says: “Sir, do you realise you were travelling at 300 000 km/s?”  
The electron goes: “Oh great, now I’m lost.”  

- A TCP packet walks into a bar, and says to the barman:  
“Hello, I’d like a beer.”  
The barman replies: “Hello, you’d like a beer?”  
“Yes,” replies the TCP packet, “I’d like a beer.”   

- Pavlov is enjoying a pint in the pub. The phone rings. He jumps up and shouts: “Hell, I forgot to feed the dog!”  

- I was sitting at a cafe, and I noticed in the display case behind me someone had put pasta and antipasto right next to each other...  
Are they out of their Vulcan minds?

- A student travelling on a train looks up and sees Einstein sitting  next to him...  Excited, he asks:  “Excuse me, professor. Does Boston stop at this train?”  

- Werner Heisenberg, Kurt Gödel, and Noam Chomsky walk into a bar...  
Heisenberg turns to the other two and says: “Clearly this is a joke, but how can we figure out if it’s funny or not?”  
Gödel replies: “We can’t know that because we’re inside the joke.”  
Chomsky says: “Of course it’s funny. You’re just telling it wrong.”  

- An infinite number of mathematicians walk into a bar...  
The bartender says: “What’ll it be, boys?”  
The first mathematician: “I’ll have one half of a beer.”  
The second mathematician: “I’ll have one quarter of a beer.”  
The third mathematician: “I’ll have one eight of a beer.”  
The fourth mathematician: “I’ll have one sixteenth of a…”
The bartender interrupts: “Know your limits, boys” as he pours out a single beer.  

- A biologist, a physicist, and a mathematician are all eating on the patio of a restaurant. Across the street, they see two people walk into a building, and a few moments later three people walk out.    
The biologist says, "Oh, they must have reproduced."  
The physicist remarks, "There must have been some type of statistical error."  
All are quiet for a long while before the mathematician says, "You know, if one more person walks into that building it will be empty.
