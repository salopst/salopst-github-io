---
author: yearluk
origin: hugo
title: Historical UK inflation rates and calculator
slug: uk-historical-inflation-rates-and-calculator
date: 2020-05-18T14:33:57+01:00
lastMod: 2021-09-18T02:30:00+01:00
draft: false
weight: 1001
noLicense: false
description: "UK historical inflation rate calculator"
categories:
  - Blog
  - Reference
tags:
  - inflation
  - money
---

Since I'm getting all old and remember days and prices from long ago, when big hair and awesome music was all the rage, and you had to have a licence to keep a dog, Imma constantly searching for roughly equivalent prices over the years.

This then is lifted squarely (and fairly) from [https://inflation.iamkate.com/](https://inflation.iamkate.com/), for which, 🙏 thanks!

## Historical prices can be converted into equivalent present-day prices by using historical inflation rates.

<iframe src="https://inflation.iamkate.com/calculator/"
  width="264" height="205"
  sandbox="allow-scripts allow-top-navigation"></iframe>

## Inflation rate history

- Data for 1949 onwards comes from the Office for National Statistics document [https://www.ons.gov.uk/economy/inflationandpriceindices/timeseries/czbh/mm23"](https://www.ons.gov.uk/economy/inflationandpriceindices/timeseries/czbh/mm23")

- Data for 1751 to 1948 comes from the 2004 paper *Consumer Price Inflation Since 1750* (ISSN: 0013-0400) **Economic Trends No. 604**, pp 38-46) by Jim O’Donoghue, Louise Goulding, and Grahame Allen.
