---
author: salopst
origin: posterous 
comments: true
date: 2011-06-29T15:32:16+00:00
lastMod: 2021-08-31T06:23:00+01:00
layout: post
slug: he-net-shindig
title: he.net shindig
wordpress_id: 134
categories:
- networking
- tech
- travel
tags:
- ipv6
- networking
- california
- san fran
- alameda
- selfie
- food
---

# Partaaay time with he.net on the Hornet

So, these are the only few pics I took of the shindig aboard the [🇺🇸 USS Hornet](https://en.wikipedia.org/wiki/USS_Hornet_(CV-8)) last night. Plenty of others of the Hornet and us on it, and I'm sure there will be plenty in teh days to come, but these are the very few taken during the event itself. 

**2012-03-04 update:** 74111101's journal for the trip has been transcribed @ [http://mary.yearl.us/visiting-the-uss-hornet-junejuly-2011/](http://stephen.yearl.us/visiting-the-uss-hornet-junejuly-2011/)


![](/wp-uploads/img_2864-jpg-scaled1000.jpg)
![](/wp-uploads/img_2865-jpg-scaled1000.jpg)
![](/wp-uploads/img_2869-jpg-scaled1000.jpg)
![](/wp-uploads/img_2871-jpg-scaled1000.jpg)


I am pretty sure that is a good thing... it meant I spent more time with people than in behind a lens.

Cocktail.... the Electric Blue Hurricane:
- Blue Curacao
- Vodka
- White rum
- 7-Up
- Splash pineapple juice

----
----
----
- [US.Yearl.Mary](http://mary.yearl.us)               
- [Hurricane Electric IPv6](http://ipv6.he.net/)
- [SixXz](http://sixxs.net)
