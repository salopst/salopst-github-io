---
author: admin
origin: wordpress
comments: true
date: 2013-07-02T19:10:53+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: antebrachium-cubital-fossa
title: Antebrachium/ cubital fossa
wordpress_id: 363
categories:
- Uncategorized
tags:
- anatomy
- medical
- wrist
---

Prosection by me, 1994. All this wrist surgery nonsense got me to dig around old files...

{{< image src="//wp-uploads/brachium_prosection.jpeg" alt="antebrachium prosection cubital fossa" position="center" style="border-radius: 50px;" >}}

We weren't allowed to take pics without consent, obviously, but this made it into my BSc thesis so was legitimately took. And obviously it was not taken on a cell phone or digital camera. We could barely dream of such tech in the olden days.
