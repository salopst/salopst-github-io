---
author: admin
origin: wordpress
comments: true
date: 2012-03-30T18:49:23+00:00
layout: post
slug: current-blends
title: Current Pipe Tobacco Blends
wordpress_id: 205
categories:
- Uncategorized
tags:
- tobacco
- pipes
---

A listing of the tobacco blends tried, roughly in order.
Some of my [reviews](http://tobaccoreviews.com/reviewer_detail.cfm?UID=12203)

\* == unsmoked, but in the 'cellar'.

 1. [Seven Seas Royal Blend](http://tobaccoreviews.com/blend_detail.cfm?ALPHA=7&TID=3677)(MacBaren)

	
 2. [Chairmans flake](http://www.smoke.co.uk/acatalog/Loose_Flake_Tobacco_To_Weigh_Out.html) (James Barber)

	
 3. [My Mixture 965](http://tobaccoreviews.com/blend_detail.cfm?ALPHA=M&TID=458)(Dunhill)

	
  4. [Squadron Leader](http://tobaccoreviews.com/blend_detail.cfm?ALPHA=S&TID=1040) (Samuel Gawith)

	
  5. [Nightcap](http://tobaccoreviews.com/blend_detail.cfm?ALPHA=N&TID=459) (Dunhill)

	
  6. Eureka (?? Aromatic from the US.. a gift)

	
  7. Aromatic #7 (?? Aromatic from the US.. a gift)

	
  8. [Irish Whiskey](http://tobaccoreviews.com/blend_detail.cfm?ALPHA=I&TID=930) (Peterson)

	
  9. [Full Virginia Flake](http://tobaccoreviews.com/blend_detail.cfm?ALPHA=F&TID=1020) (Samuel Gawith)

	
  10. Peterson's Perfect Plug (Peterson)

	
  11. Skiff Mixture (Samuel Gawith)

	
  12. Early Morning Pipe (Dunhill)

	
  13. Hal o' the Wynd (Rattray's)

	
  14. Old Joe Krantz (Cornell & Diehl)

	
  15. Marlin Flake (Rattray's)

	
  16. Blue Mountain (McClelland)

	
  17. Tree Mixture (Robert Lewis)

	
  18. [Balkan Mixture](http://tobaccoreviews.com/blend_detail.cfm?ALPHA=B&TID=1980) (Gawith Hoggarth)

	
  19. [Balkan Supreme](http://tobaccoreviews.com/blend_detail.cfm?ALPHA=B&TID=2176) (Peter Stokkebye)

	
  20. Harkness Tower (Owl Shop, New Haven, CT)

	
  21. Bull's Eye (Orlik)

	
  22. Tudor Castle (McClelland)

	
  23. Revor Plug (Gawith Hoggarth)

	
  24. Royal Yacht (Dunhill) *

	
  25. Burley Slice (Wessex)*

	
  26. Scottish Flake (Robert McConnell)*


