---
author: "yearluk"
origin: "hugo"
title: "Transcribing Enrico Vasaio for fun and no profit"
slug: "Transcribing Enrico Vasaio for fun and no profit"
date: 2021-07-31T20:13:58+01:00
lastMod: 2021-09-01T02:30:00+01:00
draft: false
noLicense: false
weight: 1001
summary: "A quick look at Italian; actually some transcription."
description: "A quick look at Italian; actually some transcription."
categories:
- Language
tags:
- Italian
- literature
- Hugo
---


Following the illustrious Johnson Government's recent wailing about Latin, I decided to do the Duolingo Latin tree to see how much I could remember from school. Quite a bit as it happened since I managed to complete the tree with only a handful of errors over about two or three days of pretty casual, bored clicking, tapping and swiping.

OK then, what about modern Latin (AKA Italian 🤣)? Rather than Duolingo, I'd dive straight into the sweet waters of *Harry Potter e la Pietra Filosofale*. So, with [audiobook from youtube](https://www.youtube.com/watch?v=W8WKLnNf4EA&origin=https://stephen.yearl.us) in ear, and a copy of the English edition nearby in case of dire stuckage, I began a [transcription](/pietra-filosofale/).

Not sure how far I'll get with this, especially considering that I'm no fan of HP anyway, but hey, it's something to do. Already I can feel myself drifting away from the language aspect of this and more toward the markup of the text. This is now just HTML, but I'm getting into old habits thinking wistfully about the [TEI](https://www.tei-c.org). At the very least wondering  how I can extract bits from the text programmatically to, say, make Anki flashcards or summut.

----
----
----
 # 🇮🇹 🍕 🍷Trascrivendo Harry Potter per divertimento e senza profitto

In seguito al recente lamenti dell'illustro governo di Johnson, ho deciso di fare l'albero del latino di Duolingo per vedere quanto riuscivo a ricordare dalla scuola. Un bel po'! Specialmente visto che sono riuscito a completare l'albero con solo una manciata di errori in circa due o tre giorni di click, tap e swiping abbastanza casuale e annoiato.

Beh allora, che dire del latino moderno (AKA italiano 🤣)? Piuttosto che Duolingo, mi ho tuffereto direttamente nelle dolci acque di "Enrico Vasaio" e la Pietra Filosofale. Così, con [l'audiolibro da youtube](https://www.youtube.com/watch?v=W8WKLnNf4EA&origin=https://stephen.yearl.us) all'orecchio, e una copia dell'edizione inglese nelle vicinanze in caso di terribile blocco, ho iniziato una [trascrizione](/pietra-filosofale/).

Non sono sicuro di quanto andrò lontano con questo, specialmente considerando che non sono comunque un fan di "EV". E ehi, è qualcosa da fare. Già mi sento allontanare dall'aspetto linguistico di questo e più verso il markup del testo. Questo ora è solo HTML, ma sto riprendendo le vecchie abitudini pensando malinconicamente al [TEI](https://www.tei-c.org). Come minimo mi chiedo come posso estrarre bit dal testo in modo programmatico per, diciamo, fare flashcards Anki o qualsiasi cosa.
