---
author: admin
origin: wordpress
comments: true
date: 2012-10-08T12:44:44+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: gluten-free-cornbread-with-almond-meal
title: Gluten-free cornbread (with almond meal)
wordpress_id: 395
categories:
- Gluten free
- Recipes
tags:
- bread
- gluten-free
- recipe
- food
---

{{< image src="/wp-uploads/GF-Cornbread-final-product.jpg" alt="Gluten-free cornbread" position="center" style="border-radius: 50px;" >}}

## One can't have Thanksgiving without cornbread!

This recipe comes as a surprise: I picked up a bag of almond meal whilst shopping and decided to try it as a straight substitute for flour in cornbread. Normally, I use some combination of tapioca flour, garbanzo flour, masa, or any of the other GF powders I have lying around.

With almond meal in hand, I surfed up a couple of recipes, and used [this](http://shecookshecleans.net/2011/10/11/gluten-free-skillet-cornbread/) one for inspiration. The result? A nice, surprisingly moist, cornbread that was good enough to reheat and enjoy the following day. This one really hit the spot.

## Ingredients
- 100g (1/2 cup) each of white and yellow cornmeal (could use 200g/1cup of either)
- 75g (3/4 cup) almond meal
- 1 tsp. baking powder
- 1 tsp. salt
- 2 large eggs
- 250 ml (1 cup) buttermilk
- 25g (2-3 tbsp.) honey
- 85g unsweetened applesauce [NOTE: Recipe calls for: 60g (4 tbsp) unsalted butter, melted and really this should have meant about 70g applesauce, but I had extra to use up.]
- grease for the skillet (I used ~ 1 tbsp bacon fat)
- Other possible additions: bacon bits, cheese, corn, hot peppers...anything else that strikes your fancy.


## Method

{{< image src="/wp-uploads/GF-Cornbread-batter.jpg" alt="" position="center" style="border-radius: 50px;" >}}

1. Preheat the oven to 425F/218C
1. Put 1 tbsp fat in the skillet and place in the oven to heat
1. Whisk together the dry ingredients in a large bowl
1. Mix the eggs, milk, and honey in a separate bowl
1. Stir the wet mixture into the dry, then add the melted butter and mix thoroughly
1.  When the fat in the skillet is sizzling hot, pour in the batter
1. Cook until the bread is golden on top and firm in the middle. (The recipe said 15-20 mins, but with the extra applesauce, I cooked ours for 30 mins.)
1. Serve straight from the pan! (Or not.)

And, voilà!

{{< image src="/wp-uploads/GF-Cornbread-slice.jpg" alt="" position="center" style="border-radius: 50px;" >}}
