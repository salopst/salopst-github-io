---
title: "Emacs Wizard parody song"
slug: emacs-wizard-parody-song
author: yearluk
origin: hugo
date: 2021-10-01T11:20:02+01:00
lastMod:
draft: false
toc: false
noLicense: false
weight: 1001
images: null
categories:
- Tech
- Humour
tags:
- emacs
- media
- parody
- latex
- lyrics
---

Found this classic from yesteryeard lying around in my `_scratch` notes directory. It's a murky directory, but every once in a while I come across something fun. I think this counts.

It should, of course, be sung to the tune of The Who's [Pinball Wizard](https://www.youtube.com/watch?v=4AKbUm8GrbM).

```json
{
  "title": "Pinball Wizard",
  "band": "The Who",
  "album": "Pinball Wizard",
  "released": 1975,
  "yturl": "https://www.youtube.com/watch?v=4AKbUm8GrbM",
  "type": ["70s", "Rock"]
}
json

-----
-----
-----

```lyrics
Ever since I was a young boy
I've played with each O.S
From Unix down to Kronos
I've crashed them I confess
But I ain't seen nothing like him
Not even in VMS
That set-mark and bind kid
Sure strokes a mean Emacs.

He sits there never blinking
Becomes part of the machine
Controls with either pinkie
A virtual typing stream
He optimizes keystrokes
Swamps your Microvax
That set-mark and bind kid
Sure strokes a mean Emacs.

He's an Emacs wizard
Without a binding list
An Emacs wizard
s' got such a calloused wrist.

How do you think he does it? I don't know!
What makes him so good?

He ain't got no distractions
He refuses warning bells
He heeds no cursor flashing
Plays by sense of smell
He never needs to undo
Knows all of Stallman's hacks
That set-mark and bind kid
Sure strokes a mean Emacs.

I thought I was
The keyboard-macro kid
But I just handed
My Emacs crown to him.

Even my usual bindings
He prefixed all my best
His disciples feed him Coke
And he just does the rest
He's got super-meta-fingers
Never hits the cracks
That set-mark and bind kid
Sure strokes a mean Emacs.
```

Found here: <https://web.archive.org/web/19970329133553/http://web.shorty.com:80/geeks/96/sep/msg00154.html>

-----
-----

And then there's "[I Tried Emacs](https://www.youtube.com/watch?v=oWxtBVT9C_s)" (Pit Party '08) from The University of Washington Computer Science and Engineering (CSE) Band at the annual CSE pit party.

They send apologies to Katy Perry.
