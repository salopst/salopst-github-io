---
author: admin
origin: wordpress
comments: true
date: 2015-03-30T11:09:10+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: atom-themes-and-packages-note-to-self
title: Atom themes and packages; note to self
wordpress_id: 841
categories:
- Tech
tags:
- Atom
- editors
---

I never really did manage to get into the swing of things with [Sublime Text](http://www.sublimetext.com/) everyone's favourite general-purpose editor since, well [TextMate](https://macromates.com/) fell out of favour ca. 2009-ish, and generally stuck to TextMate. Sure there's [VIM](http://www.vim.org/) (and it is awesome), but really? Life *is* better with a pretty and functional editor.

And then along came [Atom](https://atom.io/)... as well as a few others: Brackets and Light Table to name the other two newbies, all better described at [SitePoint](http://www.sitepoint.com/sitepoint-smackdown-atom-vs-brackets-vs-light-table-vs-sublime-text/). Suffice to say that Atom won for me. Despite it being very Sublime-like, I felt comfortable with it. So here, culled from .zsh_history, those Themes and Packages that I have installed and not uninstalled over the last eight months or so:

[Themes](https://atom.io/themes) (used under bright ambient light, also Atom's built-in "One Light" UI)

```bash
❯ apm install unity-ui
❯ apm install pen-paper-coffee-syntax
```

Themes (used under bright ambient light, also Atom's built in "One Dark" UI)

```
❯ apm install isotope-ui
❯ apm install zenburn
```

Themes (basically unused; railscasts for nostalgia; github, because wihout it there would be no Atom)

```bash
❯ apm install inspired-github
❯ apm install railscasts-theme

```

[Packages](https://atom.io/packages)
```bash
❯ apm install open-in-browser
❯ apm install color-picker
❯ apm install csslint
❯ apm install linter-rubocop
❯ apm install linter-csslint
❯ apm install emmet
❯ apm install autocomplete-plus
❯ apm install atom-beautify
❯ apm install latex
❯ apm install language-latex
❯ apm install fancy-new-file
❯ apm install file-icons
❯ apm install filetype-color
```
