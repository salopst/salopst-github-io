---
layout: post
title: Little River Band -- Listen-to-your-heart
slug: little-river-band-listen-to-your-heart
date: 2021-09-20
draft: false
categories:
  - Media
tags:
  - music
  - lyrics
  - 80s
  - JSON
---
```json
{
    "title": "Listen to your heart",
    "band": "Little River Band",
    "album": "The Karate Kid, Part III: Original Motion Picture Soundtrack",
    "released": 1989,
    "yturl": "https://www.youtube.com/watch?v=RRCXeWV-mEE",
    "type": "80s"
  }
```
<div class="success"></div>

```lyrics
Listen to your heart,
It knows right from wrong,
Let it guide you,
Listen to your heart,
It will make you strong,
Look inside of yourself and listen,
Listen to your heart.

You sure looked defeated,
When you came home tonight,
I know that you're discouraged
And tired of the fight,
Sometimes it gets so hard that you don't know what to do,
If you close your eyes the truth is there for you ...

Listen to your heart,
It knows right from wrong,
Let it guide you,
Listen to your heart,
It will make you strong,
Look inside of yourself and listen,
Listen to your heart.

There are so many temptations in the crazy world today,
And there are so many people tryin' to lead you astray,
Whenever you're confused about all the things you see,
You can't tell a friend from the enemy.

Listen to your heart,
It knows right from wrong,
Let it guide you,
Listen to your heart,
It will make you strong,
Look inside of yourself and listen,
Listen to your heart,
Listen to your heart,
It knows right from wrong,
Let it guide you,
Listen to your heart,
It will make you strong,
Look inside of yourself and listen,
Listen to your heart.
```
