---
author: admin
origin: wordpress
comments: true
date: 2013-07-13T19:29:40+00:00
lastMod: 2021-08-31T06:23:00+01:00
layout: post
slug: narcojudicial-complex
title: Narcojudicial complex
wordpress_id: 357
categories:
- random
- words
tags:
- quote
- words
- politics
---

In his exit speech on Jan. 17, 1961 [Dwight D. Eisenhower](https://en.wikipedia.org/wiki/Dwight_D._Eisenhower) warned us to be wary of the growing military industrial complex. Prescient words:

>In the councils of government, we must guard against the acquisition of unwarranted influence, whether sought or unsought, by the military-industrial complex. The potential for the disastrous rise of misplaced power exists and will persist.
>
> We must never let the weight of this combination endanger our liberties or democratic processes. We should take nothing for granted. Only an alert and knowledgeable citizenry can compel the proper meshing of the huge industrial and military machinery of defense with our peaceful methods and goals, so that security and liberty may prosper together.
>
> ~~ Dwight D. Eisenhower

Well, I am hereby sort of claiming "narcojudicial complex", and I am more than a little surprised that it or its hyphenated form "narco-judicial" does not produce many Google results. I don't think I really need to define it. We all are aware of the huge size of the US prison population, the demographics of which include a very great number of non-violent, poor, drug offenders. Are dugs a social scourge? Very probably. Is mandatory incarceration for "dime-bag" worth it? I doubt it, but there see to be companies doing very well out of the privately run for-profit gaol system.

And don't get me started on the 13th fscking Amendment, which is, IMHO, pure evil...

> "Neither slavery nor involuntary servitude, except as a punishment for crime whereof the party shall have been duly convicted, shall exist within the United States, or any place subject to their jurisdiction."
