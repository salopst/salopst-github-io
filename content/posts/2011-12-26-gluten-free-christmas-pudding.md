---
author: mary.yearl
origin: wordpress
comments: true
date: 2011-12-26T17:50:34+00:00
layout: post
slug: gluten-free-christmas-pudding
title: Gluten-free Christmas pudding
wordpress_id: 381
categories:
- Gluten free
- Recipes
tags:
- Christmas
- food
- baking
- gluten-free
- pudding
- recipe
---


Even though this is our first gluten-free Christmas, we didn't want to miss out on the pudding. So, we made our own. I was apprehensive, but need not have been.

{{< image src="/wp-uploads/GF_Xmas_pud.jpg" alt="Gluten-fre Christmas Pudding" position="center" style="border-radius: 50px;" >}}

The basic recipe I followed was [this](http://www.jamieoliver.com/recipes/member-recipes/Gluten%20Free%20Christmas%20Pudding/1414) one. Note that you have to scroll down the page quite a ways to actually reach the recipe.

I also had [this one](http://britishfood.about.com/od/christmas/r/xmaspud.htm) on hand and my final result combined aspects of both and may have included some improvisation re: dried fruits.

For the breadcrumbs, I followed a recipe very similar to the one for cornmeal croutons listed on [this](http://www.celiac.com/articles/340/1/Corn-Bread-Stuffing-Gluten-Free/Page1.html) page.

If you need a shortcut, I have had some success with Glutino GF crumbs elsewhere: we used them in some fantastic crabcakes, but that's another story.

For the proper flavour in any Christmas pudding, it is important to start early. I did not, so I can say that 2 weeks of maturing time was just about enough. I did top up with brandy every few days to help the process! If you want a nice, rich, flavour, the cooked pudding should mature for at least a month. The great thing about this process is that you can make them several months in advance and they will continue to mature. Just check on them from time to time, add a bit more brandy along the way, and you'll have a warm belly and happy tastebuds come Christmas.

On the day itself, we plopped the pudding in the steamer and let it sit for a couple of hours (really, it only needs heating, so half an hour should do it) and it was fine. When we were ready, we turned out the lights, poured on the brandy, and flambéed the pudding. We had ours with whipped cream this year, though normally we have brandy butter.

For those unaccustomed to a Christmas pudding, it is very rich and you'll want to eat only a small amount at a time. Just a few bites and you should be sated.
