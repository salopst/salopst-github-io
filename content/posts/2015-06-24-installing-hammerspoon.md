---
author: admin
origin: wordpress
comments: true
date: 2015-06-24T19:03:04+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: installing-hammerspoon
title: Installing hammerspoon
wordpress_id: 944
categories:
- Tech
- Apple
- OS X
tags:
- automation
- sysadmin
- OS X
---

>
> ### What is Hammerspoon?
>
>

A tool for powerful automation of OS X. At its core, Hammerspoon is just a bridge between the operating system and a Lua scripting engine. What gives Hammerspoon its power is a set of extensions that expose specific pieces of system functionality, to the user.

{{< image src="/install_hammerspoon.png" alt="Install Hammerspoon" position="center" style="border-radius: 50px;" >}}
