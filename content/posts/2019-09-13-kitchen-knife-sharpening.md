---
title: "Kitchen Knife Sharpening"
slug: "kitchen-knife-sharpening"
author: "yearlus"
origin: "hugo"
date: 2019-09-13T05:24:09+01:00
lastMod: 2021-08-10T18:30:00+01:00
draft: false
toc: false
noLicense: false
weight: 1001
images:
categories:
  - "Food and Recipies"
tags:
  - cooking
  - knife
  - steel
  - tools
  - kitchen
description: "Some semi random notes on knife sharpening"
---


## 0. Equipment

Ideally I'll be needing three stones: a coarse-grit whetstone for making a new bevel (ca. 400 grit), a medium-grit whetstone (ca. 1000) for making a new edge, and a fine-grit stone (4000/6000) for finishing and polishing the edge.

The harder the steel, the finer the grit I'll need to use for finishing, polishing and stropping, even to the point, perhaps of finishing on leather as with a straight razor. My leather strop I left stateside, so, dunno. Maybe I'll pass on that. Finishing at 1000 will probably be sufficient for my non-pro needs anyway. After all a sharp edge gets duller faster, right?

Typical Euro knives are in the 52/58 HRC range on the [Rockwell Hardness Scale](https://en.wikipedia.org/wiki/Rockwell_scale) and probably need to be finsished with stones in the 2k/4k range. Japaense sushi high carbon knives are in 58/68 Rowkwell range. I *had* one of these, but no longer. I hope that it's being used and enjoyed, though. It was a beaut, in the nakiri cleaver style ordered from Japan in ca. 2000. Rusted as soon as you looked at it, though!

- Glass is about **HRC 60**.
- Very hard steel (e.g. chisels, quality knife blades): **HRC 55–66**
- Hardened High Speed Carbon and Tool Steels such as M2, W2, O1, CPM-M4, and D2, as well as many of the newer powder metallurgy Stainless Steels such as CPM-S30V, CPM-154, ZDP-189.
- There are alloys that hold a **HRC 68-70**, such as the Hitachi developed HAP72. These are extremely hard, but also somewhat brittle.)
- Axes: about **HRC 45–55**

Last purchased a KitchenCraft MasterClass dual 400/1000 whetstone (18 x 6.5 cm) on 2015-06-06. About £16... so Definitely not spank worthy, but prolly better than the average treament most knives get. Then again, I really am not *happy* with the MasterClass. Perhaps it's time to upgrade.


### Update August 2021

Out of my price range or degree of interest... knife sharpening gets "culty" rather quickly.

- [Burrfection on YT](https://www.youtube.com/channel/UCOluHMoKJ6CrS0kcybhaThg) is wicked entertaining
[Murray Carter Blade Sharpening Fundamentals](https://www.youtube.com/watch?v=Yk3IcKUtp8U)
- Medium stone: https://amzn.to/3xAUPvA
- Fine stone: https://amzn.to/3tSsmyU
- Diamond plate for leveling: https://amzn.to/3aJbLpE
- Sink bridge: https://amzn.to/3dPTOrE


## 1. Basic instructions

1. Soak stones until air bubbles have stopped escaping, and place on stable surface like damp tea towel

1. With blade facing toward me. It's drawn backwards, i.e. the edge trails) at 15-20º  relative to the surface of the stone (with length of the blade at 45º angle relative to the length of the stone). Fingers of your left hand on the face of the blade used to push the edge gently into the stone.

1. Work the blade to and fro like feels like shaving sand sand off the stoneD. Draw the blade orthogranally across the surface to cover the whole blade, avoilding bolster. Constantly rewet the stone, to clear the swarf and the "sand" slurry.

1. Eventually a burr will form along the whole edge. 🎲! 'Tis time to sharpen the opposite side. Blade faces away, and is the length I hold at 90º angle perpendicular to the length of the stone.Repeat.

1. Another (?) burr! Back to the first side and repeat, but with progressively fewer passes and lighter pressure.

1. keep repeating until time to switch to the finer grit. And repeat.

1. Stropping. V. light pressure and single sweeping arcs with the edge leading. First  10 each side, then 8 each side, then 6... etc. until done.
