---
author: admin
origin: wordpress
comments: true
date: 2012-10-07T12:12:20+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
slug: lime-meringue-pie
title: Lime meringue pie
wordpress_id: 392
draft: false
categories:
- Gluten free
- Recipes
tags:
- gluten-free
- meringue
- pie
- recipe
- food
---

We can thank [STS-133](http://www.nasa.gov/mission_pages/shuttle/shuttlemissions/sts133/main/index.html) for our family's appreciation of key lime pie. While we did see it launch in the end, our first journey to Florida included a trip to the Keys while we waited to hear whether the mission would go ahead. That first trip may end up being the first and last time we sample proper Key Lime Pie, but we'll continue to experiment on our own versions that bring us back to Islamorada. The current recipe is not key lime pie, even if it was inspired by that dessert. First, key limes were out of season and therefore not available. Secondly, we decided to add a meringue using the leftover egg whites. That and Joseph was very much into the 'science' of meringues.

{{< image src="/wp-uploads/GF-lime-meringue-finished.jpg" alt="Key lime meringue pie" position="center" style="border-radius: 50px;" >}}

 The recipe we followed was based on [this](http://allrecipes.com/recipe/easy-key-lime-pie-i/):

## Ingredients
- 5 egg yolks
- 14 oz. (1 can) condensed milk
- 1/2 cup (key) lime juice
- Our [Graham cracker crust](http://stephen.yearl.us/gluten-free-graham-cracker-crust/)

## Method
- Preheat the oven to 375 degrees F/190 degrees C
- Beat the yolks, then add in condensed milk and lime juice and mix thoroughly.
- Pour the mixture into a [Graham cracker crust](http://stephen.yearl.us/gluten-free-graham-cracker-crust/) (we mostly followed the recipe from [Gluten-Free Girl](http://glutenfreegirl.com/gluten-free-graham-crackers/)).
- Bake for 15 minutes, then let cool.
- Whip the egg whites (after adding some green food coloring to keep in theme) and 1/4 cup of icing/confectioners' sugar until stiff, and forming peaks. Spread on top of the pie.
- Bake until golden brown (10-15 minutes)

{{< image src="/wp-uploads/GF-lime-pie-cooked.jpg" alt="Key lime meringue pie" position="center" style="border-radius: 50px;" >}}


Right, so our crust was a bit large, and the consistency of the crust was such that it flopped over onto the pie filling. We rather liked the effect, one child claiming that it looked a bit like some weird planetoid. In any event, we covered all of this with the colored meringue.

## Modifications
For a start, we try to use key limes! That said, the slightly milder taste meant that everyone in the family liked this one. One time we included rind and made an incredibly intense pie. I'd recommend that for anyone who wants pie tasting to be an experience.
