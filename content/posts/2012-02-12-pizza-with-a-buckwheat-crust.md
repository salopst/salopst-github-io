---
author: admin
origin: wordpress
comments: true
date: 2012-02-12T06:08:20+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: pizza-with-a-buckwheat-crust
title: Pizza with a buckwheat crust
wordpress_id: 387
categories:
- Gluten free
- Recipes
- Food
tags:
- buckwheat
- gluten-free
- pizza
---

Shortly after going gluten-free, we tried Bob's Red Mill gluten-free pizza crust. Having lived in New Haven for many years, we like pizza. Bob's didn't measure up, so that was it...until now. This might not be apizza, but as long as we're gluten-free we may have to settle for a bit less.

{{< image src="/wp-uploads/Ready-to-eat.jpg" alt="Ready to eat" position="center" style="border-radius: 50px;" >}}

Having found a hit with buckwheat pancakes, I decided to try a buckwheat pizza crust. The recipe I adopted was basically the one described [here](http://www.nourishingmeals.com/2009/12/thin-buckwheat-pizza-crust-gluten-free.html), but without the garlic powder.


## Ingredients

- 2 ¼ cups (275 g) buckwheat flour
- ½ cup (60 g) tapioca flour
- pinch salt
- ½ tsp. baking soda
- 1 ¼ cup (280 ml) warm water
- ¼ cup (85 ml) olive oil*
- ½ tbsp. honey
- 1 tbsp. cider vinegar
- cornmeal (optional)


**I used rosemary and peppercorn infused olive oil**

## Method

- Preheat oven to 375°F (190°C)

- Grease a baking tray and sprinkle with cornmeal (cornmeal is optional)

- In a mixing bowl, whisk together the flours, baking soda, and salt

- In a medium-sized bowl, combine the water, oil, honey, and vinegar

- Add the wet to the dry ingredients and mix them until well blended

- **The batter should be fairly moist**

- Pour the batter onto the baking tray and spread it until even in thickness

- Pre-bake the crust for 15-20 minutes or until it seems just cooked

- Take it out and add toppings. I first spread olive oil over the crust, this time some I had left over from a tub of marinated mozzarella balls.

- Once you’ve created your pizza, bake until the toppings look done, about 10-15 minutes.


### The batter will be surprisingly runny.

{{< image src="/wp-uploads/Buckwheat-batter.jpg" alt="Buckwheat batter" position="center" style="border-radius: 50px;" >}}                  

### I spread olive oil and cornmeal on the tray

{{< image src="/wp-uploads/Cornmeal-base.jpg" alt="Cornmeal base" position="center" style="border-radius: 50px;" >}}


### This is how the batter looked before it went into the oven:

{{< image src="/wp-uploads/Ready-to-pre-bake.jpg" alt="Ready to pre-bake" position="center" style="border-radius: 50px;" >}}


### and this is how it looked after:

{{< image src="/wp-uploads/Prebaked-crust.jpg" alt="Pre-baked crust" position="center" style="border-radius: 50px;" >}}


I was a little sneaky. My kids don’t normally eat carrots, so I grated one and spread it on top of the crust where I could hide it. I did the same with some diced fresh tomatoes:

{{< image src="/wp-uploads/Carrots-and-tomatoes.jpg" alt="Carrots and tomatoes" position="center" style="border-radius: 50px;" >}}

The sauce I used was a combination of homemade red sauce and Trader Joe’s Organic Marinara with no salt added. Other toppings included fresh mozzarella, grated cheddar, and bacon. I added sliced jalapeños to my corner.

{{< image src="/wp-uploads/Ready-to-eat.jpg" alt="Ready to eat" position="center" style="border-radius: 50px;" >}}

## **The verdict**

Two very happy boys. Neither noticed the carrots and their only complaint was that the toppings fell off too easily. In terms of the crust, it tasted good but was not like a traditional crust because the texture was somehow dense yet light at the same time. It’s possible that I didn’t pre-cook it for long enough, so next time I'll go for a golden look. Overall, I found it to be nice and manageable. Unlike so many other crusts, the boys could cut up their slices without any problem. It was soft, yet not soggy. I’ll have to play around with this one to make it a bit thinner and crispier, but it seems like a winner. Not too shabby the morning after, either.


