---
author: salopst
origin: posterous
comments: true
date: 2011-05-28T07:21:10+00:00
lastMod: 2021-08-31T06:23:00+01:00
layout: post
slug: the-english-mind
title: the English mind
wordpress_id: 1456
categories:
- random
- words
tags:
- quote
- words
- english
---

>Propose to an Englishman any principle, or any instrument, however admirable, and you will observe that the whole effort of the English mind is directed to find a difficulty, a defect, or an impossibility in it. If you speak to him of a machine for peeling a potato, he will pronounce it impossible: if you peel a potato with it before his eyes, he will declare it useless, because it will not slice a pineapple. Impart the same principle or show the same machine to an American or to one of our Colonists, and you will observe that the whole effort of his mind is to find some new application of the principle, some new use for the instrument.</

~~ Charles Babbage, Computer dude.

--- Somewhere in: Collier, B. and MacLachlan, J. --- *Charles Babbage: And the Engines of Perfection.* OUP. 1999.
