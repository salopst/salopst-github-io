---
author: admin
origin: wordpress
comments: true
date: 2013-06-15T21:12:16+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: alfred-2-workflows
title: Alfred 2 workflows
wordpress_id: 313
categories:
- Apple
- OS X
- Tech
tags:
- apps
- OS X
---

## 2016-05-23 update:


Due to [recent woes](http://stephen.yearl.us/fsked-home-el-capitan-home-directory/) I had to revisit Alfred Workflows... basically the below remains the same with the addition of:

[http://www.packal.org/workflow/packal-updater](http://www.packal.org/workflow/packal-updater)
[http://www.packal.org/workflow/datetime-format-converter](http://www.packal.org/workflow/datetime-format-converter)
[http://www.packal.org/workflow/toggle-keyboard-viewer](http://www.packal.org/workflow/toggle-keyboard-viewer)
[http://www.packal.org/workflow/leo-dictionary](http://www.packal.org/workflow/leo-dictionary)
[http://www.packal.org/workflow/eggtimer-2](http://www.packal.org/workflow/eggtimer-2)
[http://www.packal.org/workflow/units](http://www.packal.org/workflow/units)

-----
-----
-----

## Wolfram Alpha:

- Get Alfred 2, buy the Powerpack! [http://www.alfredapp.com/](http://www.alfredapp.com/)

- Get Wolfram Alpha workflow: [http://www.alfredforum.com/topic/655-wolframalpha-workflow/](http://www.alfredforum.com/topic/655-wolframalpha-workflow/)

- Get Wolfram Alpha appID: [https://developer.wolframalpha.com/portal/signin.html](https://developer.wolframalpha.com/portal/signin.html)


And behold! Easy access to a wealth of information, unit conversions, values for scientific formulæ and constants and the...

![](/wp-uploads/wpid-Screen-Shot-2013-07-08-at-17.06.02-5.png)


## Timer:

- [http://dbader.org/blog/alfred-timer-extension](http://dbader.org/blog/alfred-timer-extension)

![](/wp-uploads/wpid-alfred-timer1.png)

Very simple countdown timer. Nothing fancy, practically zero overhead.


## Evernote:

[http://www.alfredforum.com/topic/840-evernote-71-search-create-append-text-preview-and-more-all-within-alfred/](http://www.alfredforum.com/topic/840-evernote-71-search-create-append-text-preview-and-more-all-within-alfred/)

![](/wp-uploads/wpid-Screen-Shot-2013-07-08-at-21.07.09-1.png)


## Onelook:

[https://github.com/ChewingPencils/alfred_workflows](https://github.com/ChewingPencils/alfred_workflows)

Searches [http://www.onelook.com/](http://www.onelook.com/)

![](/wp-uploads/wpid-Screen-Shot-2013-07-08-at-21.35.39-31.png)

So:

- **olq** QUERY == quick definition
- **olb** QUERY == wildcard search [http://www.onelook.com/?c=faq#patterns](http://www.onelook.com/?c=faq#patterns)
- **olr** QUERY == reverse lookup (lets you describe a concept and get back a list of words and phrases related to that concept.

## Google Translate:

[https://github.com/thomashempel/AlfredGoogleTranslateWorkflow](https://github.com/thomashempel/AlfredGoogleTranslateWorkflow)

"Translate" + target lang code + phrase

![](/wp-uploads/wpid-Screen-Shot-2013-07-08-at-22.03.06-.png)
