---
author: kword
comments: true
date: 2014-10-04T13:11:49+00:00
layout: post
slug: koine-greek-word-of-the-day-δημήτριος-ου-ὁ
title: 'Koinē Greek Word of the Day: Δημήτριος, ου, ὁ'
wordpress_id: 769
categories:
- Language
tags:
- Greek
- Bible
- words
- koine
- κοινή
---

# Δημήτριος, ου, ὁ

Démétrios
Noun, Masculine
*Demetrius, a silversmith of Ephesus.*
Strong's: **1216**

3 occurrences in GNT

```text
Δημητρίῳ (N-DMS), 3 John 1:12
Δημήτριος (N-NMS), Acts 19:38
Δημήτριος (N-NMS), Acts 19:24
```

# Acts 19:24

```text
Δημήτριος γάρ τις ὀνόματι, ἀργυροκόπος, ποιῶν ναοὺς ἀργυροῦς Ἀρτέμιδος παρείχετο τοῖς τεχνίταις οὐκ ὀλίγην ἐργασίαν,
```
~~SBL Greek New Testament (SBLGNT)

```text
Demetrius enim quidam nomine argentarius faciens aedes argenteas Dianae praestabat artificibus non modicum quaestum
```
~~Biblia Sacra Vulgata (VULGATE)

```text
For a certain man named Demetrius, a silversmith, which made silver shrines for Diana, brought no small gain unto the craftsmen;
```
~~King James Version (KJV)

* * *
kwords 0.3
