---
author: admin
origin: wordpress
comments: true
date: 2013-10-24T20:58:05+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: rot-n-bested
title: Future cryptanalyst??
wordpress_id: 396
categories:
- Uncategorized
tags:
- boys
---


Joe (after being deflated by my telling him I can defeat his ROT-n substitution cipher with simple letter frequency analysis):

"what if n changed, letter by letter, according to Pi?"

"Knowing you I would probably have found Pi to be the key"

"Not if I used non-consecutive digits... or added even numbers... or switched schemes on vowels... or..."


### Now, where at GCHQ do I apply for his junior COMINT badge?
