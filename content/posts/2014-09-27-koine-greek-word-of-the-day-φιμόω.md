---
author: kword
comments: false
date: 2014-09-27T18:32:49+00:00
layout: post
slug: koine-greek-word-of-the-day-φιμόω
title: 'Koinē Greek Word of the Day: φιμόω'
wordpress_id: 705
categories:
categories:
- Language
tags:
- Greek
- Bible
- words
- koine
- κοινή
---

# φιμόω

phimoó
Verb
*I muzzle, silence.*
Strong's: **5392**

8 Occurrences in GNT

# Matthew 22:12

```text
καὶ λέγει αὐτῷ· Ἑταῖρε, πῶς εἰσῆλθες ὧδε μὴ ἔχων ἔνδυμα γάμου; ὁ δὲ ἐφιμώθη.
```
~~Society of Biblical Languages Greek New Testament

```text
et ait illi amice quomodo huc intrasti non habens vestem nuptialem at ille obmutuit
```
~~Jerome's Vulgate

```text
And he saith unto him, Friend, how camest thou in hither not having a wedding garment? And he was speechless.
```
~~King James Version
