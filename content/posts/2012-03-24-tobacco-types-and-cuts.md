---
author: admin
origin: wordpress
comments: true
date: 2012-03-24T14:34:35+00:00
layout: post
draft: false
slug: tobacco-types-and-cuts
title: Tobacco Types (and cuts)
wordpress_id: 200
categories:
  - Uncategorized 
tags:
  - pipes
  - tobacco
---

## Types

**Virginias** usually represent the highest percentage in a blend. Although there's a wide range of Virginias to choose from, including bright VA's, matured VA's, and stoved VA's, in general the lighter the color the tangier the taste, with the darker VA's lending a deep, rich note to the blend. Unprocessed VA's tend to produce tongue bite, and stoved VA's tend to bite much less.

**Burley**, like VA, is a good base tobacco. It has no sugar of its own, but is relatively bite-free. It should never come to the forefront, but be used sparingly. The "Burley Curse" refers to the habit this tobacco has of overpowering a blend. Burley has a distinct nutty taste, but because of its somewhat bland character, is often flavored.

**Cavendish** generally refers to a blend of various tobaccos that have been sweetened, stoved, and pressed. Unflavored black cavendish, for instance, is made from Kentucky Green River burley, which is sugared, steamed and toasted, yielding a distinct caramel flavor. In a blend, cavendish can be used sparingly, to add body or flavor, or become the base, yielding a sweet tobacco. It goes particularly well with Latakia.

**Perique** is a "spice tobacco", with a distinct peppery flavor. Produced mostly in Louisiana from various base tobaccos, and cured in its own juices, this not only adds spice to a VA blend, but also mitigates a lot of the bite that comes with it. When not part of a straight VA/Perique blend, it should only be added to the point that it becomes noticeable. In a regular VA/Perique blend, it can represent a higher percentage, though 10% is often enough.

**Latakia** is Turkish tobacco flavored with smoke. Syrian Latakia is strong and bright, while Cyprian Latakia has a deeper, mellower flavor. This is another spice tobacco, and needs a base tobacco to keep it in check. Fifty percent Latakia would be considered a "heavy Latakia blend". It's generally wise to use less Syrian than Cyprian.

**Oriental** is spice tobacco from the Eastern Mediterranean (countries such as Turkey or Greece). Somewhat akin to Latakia, this is spicy and sweet. Most oriental tobacco is a blend. Even a label that refers to it as one distinct type (such as basma), is usually referring to its main component. Turkish is often used to fill out the general impression of a Latakia. Again, on its own, 50% would be considered a "heavy Oriental".

**Maryland** and Carolina can be thought of as "filler" tobaccos, used mainly to flesh out the taste of a VA or Burley base. Not terribly distinctive, these can be used to mitigate tongue bite.

### from: [http://tobaccoblending.com/tobacco_blending_how_to.htm](http://tobaccoblending.com/tobacco_blending_how_to.htm)

-----
-----
-----

### Cuts

**Cavendish**- A very important process in today's tobaccos. Cavendishes, in older blends, generally referred to tobaccos which had been treated with flavorings or even sugar water, sometimes steamed (mainly in Black Cavendish), pressed, cut, and rubbed-out. These were the original aromatics. Through the years the term has become watered-down, and is commonly used to refer to any flavored tobacco blend.

**Flake**- Tobaccos, normally whole-leaf, that have been pressed, and usually sliced, are called Flakes. The pressure aids in the maturing process, and brings out a richer flavor. The most common Flakes are based upon Virginias, and Virginia blends.

**Krumble Kake**- Cut tobaccos which have been pressed are sometimes referred to as Krumble Kake. It is so named since a chunk of it can be easily rubbed-out into small pieces.

**Cubed**- Pressed tobacco which has been cut into fine or coarse cube-shaped pieces is called Cubed, with the most common type being Cubed Burley. The thick, chunky pieces burn slowly, so Cubed tobaccos are normally quite cool.

**Rough Cut**- Tobaccos cut into larger flat pieces are called Rough Cut. This cut burns slowly, and can be used to keep hotter tobaccos from burning too fast.

**Broad Cut- Wide, ribbon** -cuts, which burn at an average pace, and pack well, are often called Broad Cut.

**Ribbon**- Narrower than Broad Cut, it burns more readily (a good cut for tobaccos that don't burn easily), and packs well.

**Shag**- A very stringy ribbon cut, Shag can easily pack too tightly, and burns easily. At one time Shag was considered an inferior cut.

**Twist, Roll Cut and Rope**- All are rolled tobaccos, twisted (at least to some degree) to create pressure to help mature the tobacco. Sometimes the tobaccos are cased for flavor. They are normally cut into "coins", and can be packed whole, or rubbed-out.

### And two terms that are important to know-

**Casing**- Referred to earlier, Casings are flavorings, sometimes using an alcohol base, that are added early in the processing. Casings are primarily used to add flavor to a blend.

**Top Dressing**- Top Dressings are added toward the end of processing, and their main purpose is to enhance room note, or aroma.

### from : [http://www.pipesandcigars.com/outocuandpr.html](http://www.pipesandcigars.com/outocuandpr.html)
