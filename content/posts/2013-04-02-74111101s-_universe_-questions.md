---
author: admin
origin: wordpress
comments: true
date: 2013-04-02T14:03:45+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: 74111101s-_universe_-questions
title: 74111101's The Universe questions
wordpress_id: 87
Categories:
- Uncategorized
tags:
- blog
- boys
---

74111101-- yes, my eldest insists on representing himself as the [ASCII](http://en.wikipedia.org/wiki/ASCII) decimal [code point](http://en.wikipedia.org/wiki/Code_point) values for his name-- and I have been watching the History Channel's [The Universe](http://www.imdb.com/title/tt1051155/). Below some of 74's questions (typed verbatim from his notes) that he hopes to have answered after writing to some of the presenters in the series.

### Questions

  * If the Universe keeps on expanding will it eventually rip apart the things that make up [dark energy](http://en.wikipedia.org/wiki/Dark_energy)?

  * Is there a known limit to the repulsive effects of dark energy in the way that we know gravity does operate above or below certain distances?

  * Is dark enery strong enough to rip apart:

  * [dark matter](http://en.wikipedia.org/wiki/Dark_matter) itself?, and

  * the atoms that make up stars and "regular matter"? How broken up will matter become? Can it go beyond [fundamental particles](http://en.wikipedia.org/wiki/Elementary_particle)?

  * If there is antimatter, is there antiDARKmatter?

  * Once the sun becomes a white dwarf, is it possible to combine all the gas in the [planetary nebula](http://en.wikipedia.org/wiki/Planetary_nebula) and the [outer planets](http://en.wikipedia.org/wiki/Outer_planets) to form another sun?

  * Is it possible to use the [sun's](http://en.wikipedia.org/wiki/Sun) energy to start [nuclear fusion](http://en.wikipedia.org/wiki/Nuclear_fusion) and as the sun gets cooler to add more hydrogen to make it hotter and launch it into space?

  * If dark energy is driving the expansion of the Universe will it eventually destroy the particles that cause dark matter? Will this then cause a [big crunch](http://en.wikipedia.org/wiki/Big_Crunch) and possibly a new big bang?


### Presenters
  * [Don lamb](http://astro.uchicago.edu/people/donald-q-lamb.shtml), U. Chicago
  * [Michio Kaku](http://mkaku.org/), CUNY
  * [Alex Filippenko](http://astro.berkeley.edu/people/faculty/filippenko.html), UC Berkeley
  * [Jerry Linenger](http://en.wikipedia.org/wiki/Jerry_M._Linenger), Astronaut
  * [Robert Kirshner](https://www.cfa.harvard.edu/~rkirshner/), Harvard-Smithsonian
  * [Neil deGrasse Tyson](http://www.haydenplanetarium.org/tyson/), Hayden Planetarium




[![Enhanced by Zemanta](http://img.zemanta.com/zemified_e.png?x-id=08227630-8f30-4ff6-884e-da245a86aa94)](http://www.zemanta.com/?px)
