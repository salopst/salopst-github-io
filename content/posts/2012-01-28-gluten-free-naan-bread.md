---
author: admin
origin: wordpress
comments: true
date: 2012-01-28T12:49:51+00:00
layout: post
slug: gluten-free-naan-bread
title: Gluten-free naan bread?
wordpress_id: 383
categories:
- Gluten free
- Recipes
tags:
- gluten-free
- recipe
- baking
- bread
---

There is a chicken curry bubbling on the stove, saffron rice steaming away in the rice cooker: how can I not think of naan bread?

{{< image src="/wp-uploads/gf-naan-bread-finished.jpg" alt="Gluten-free Naan Bread" position="center" style="border-radius: 50px;" >}}

I surfed up a recipe for gluten-free naan bread, and this is what I found: 
 [http://www.food.com/recipe/gluten-free-naan-roti-indian-flat-bread-version-1-189334](http://www.food.com/recipe/gluten-free-naan-roti-indian-flat-bread-version-1-189334)


I spent less than 30 minutes from start to finish, and in the end we had 2 large and 3 smaller naan breads.The final recipe looked like this:

### Ingredients
- 150 ml warm (but not hot) milk	
- 60 g tapioca flour
- 85 g buckwheat flour
- 90 g brown rice flour
- 100 g white rice flour
- 1 tsp gluten free baking powder
- 1/2 tsp salt
- 1 generous tsp xanthan gum
- 1 tsp caster sugar
- 1 packet (about 2 tsp) active dry yeast
- 2 tsp olive oil (infused with rosemary and peppercorns)
- 1 egg, lightly beaten
- 150 ml plain yoghurt
- extra rice flour to prevent sticking



### Process

  1. Preheat the oven to 500 fahrenheit; preheat baking trays
  1. Heat the milk on a simmer burner until it's just warm, but not hot.
  1. Remove from heat and stir in sugar and yeast. Let it sit for about 5 minutes
  1. While waiting for the yeast mixture, combine in a large bowl: flours, baking powder, salt, xanthan gum. If you don't have a sifter, whisk them around until evenly blended.


  1. Add to the flour mixture: yoghurt, egg, oil, and the milk mixture, and combine until smooth.


  1. If the dough is sticky, dust your hands with rice flour and shape the dough into balls, then spread out until they are uniform patties about 1 cm thick. Shape them as you please, but try to make all roughly the same size and thickness.


  1. Place the patties on the pre-heated baking tray and place in the oven for 3 minutes, or until they are puffy and slightly browned.


  1. Broil them for about a minute, or until nicely golden.


  1. Optional: brush them with olive oil, ghee, or anything else that strikes your fancy. As with the mixture, I used olive oil infused with rosemary and peppercorns.




{{< image src="/wp-uploads/gf-naan-bread-pre-bake.jpg" alt="Gluten-free Naan Bread" position="center" style="border-radius: 50px;" >}}



### Ready to bake!


{{< image src="/wp-uploads/gf-naan-bread-baking.jpg" alt="Gluten-free Naan Bread" position="center" style="border-radius: 50px;" >}}



After about 3 minutes, the bread had puffed up nicely and were ready for browning under the broiler.


