---
author: admin
origin: wordpress
comments: true
date: 2014-09-05T17:21:29+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: borschtards
title: Borschtards
wordpress_id: 598
categories:
- Uncategorized
tags:
- language
---

As of posting there is no result from Google search for “borschtard”; I sort of lay claim, therefore, to this portmanteau. This blend of borscht and bastard has no meaning. One could take it as a Russian racial slur, I suppose. Or, it could be indicative of very bad, borschtardized, eastern European cooking. Either way, "I like it” and, like General Sir Cecil Hogmany Melchett, shall “want to use it more often in conversation."
