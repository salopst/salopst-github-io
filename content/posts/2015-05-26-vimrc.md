---
author: admin
origin: wordpress
comments: true
date: 2015-05-26T15:43:25+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: vimrc
title: .vimrc
wordpress_id: 906
categories:
- Tech
tags:
- dotfile
- sysadmin
---

## 2021-09-23 UPDATE

### Hilarious...

```bash
~
 🧠 15GiB/23GiB avec λ=❯ ...
 ✗ ❯❯  vim
Command 'vim' not found, but can be installed with:
# ==> whatever
 ❯❯  vi --version
 VIM - Vi IMproved 8.2 (2019 Dec 12, compiled Feb 15 2021 12:29:39)
 Included patches: 1-2434
 Modified by team+vim@tracker.debian.org
```

Shows just how much I (don't) use the editor.

And no `.vimrc` except for this from an old dropbox backup! Included for historical reasons, and if on the off chance I use vim again.

{{< gist salopst 7f1666d90493faf3a80e775e62195577 >}}
### {{</* gist salopst 7f1666d90493faf3a80e775e62195577 */>}}

## Let's update anyway, since we're here.

```bash
sudo apt install vim
# ==> stuffs
which vim && vim --version
/usr/bin/vim
VIM - Vi IMproved 8.2 (2019 Dec 12, compiled Feb 15 2021 12:29:39)
```

Bunch of issues with the 2015 `.vimrc` obvs. They seem mostly to do with with the [Vundle](https://github.com/VundleVim/Vundle.vim) plugin manager.

Install it:

```bash
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

vim
# ==>
Error detected while processing VIMINIT..script /home/yearluk/.config/vim/vimrc:
line   84:
E185: Cannot find color scheme 'Tomorrow-Night-Eighties'
Press ENTER or type command to continue
```
As we can see Vundle has created a `~./vim` directory, and this is where the gist above (typo notwithstanding)
directs us to sick color themes. Amazingly, Past Me, has a backup of that directory. in `~/Nextcloud/lin-share/vim.colors/colors`

```bash
cp -r /home/yearluk/code/hugo-sites/salopst.github.io/content/posts/2015-05-26-vimrc.markdown ~/.vim
```

### 🎲🎲 DICE!!
... except for, within vim, a function error:
`E117: Unknown function fugitive#statusline`, but running `:BundleInstall!` within  vim gets that sorted.

Now I can party like its two-thousand-fifteen. And thanks 🙏 Past Me for making backups!
