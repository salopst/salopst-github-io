---
author: admin
origin: wordpress
comments: true
date: 2013-07-18T17:37:12+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
slug: oopsies-iii
title: Oopsies III
wordpress_id: 369
draft: false
categories:
- Uncategorized
tags:
- medical
- wrist
---

Butterfly stitches out now.... Still in the Duran-friendly dorsal blocking splint, wrist cocked @ 20º. More exercises! Largely these are Duran/ modified Duran protocol, i.e. passive flexion, semi-active extension. The three main rehab protocols seem to be:

- Duran: [Controlled Passive Motion Methods](http://www.eatonhand.com/thr/thr075.htm)

- Kleinert: Active extension, passive flexion by rubber bands

- Strickland: Early active ROM (range of motion)



With the aim of full ROM by week 12. Basically it's a balancing act. Too much active flexion risks rupture, and too little leads to scaring and adhesion. But I should be getting ultrasound therapy twice weekly starting next week to break up scar tissue. W00t

And it would appear that external stitches of the Bruner incision counted 30, not the 25 I'd mentioned earlier.


{{< image src="/wp-uploads/scar.jpg" alt="Scar" position="center" style="border-radius: 50px;" >}}
