---
author: admin
origin: wordpress
comments: true
date: 2012-10-13T14:16:39+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: gluten-free-tortillas
title: Gluten-free tortillas (tlaxcali)
wordpress_id: 398
categories:
- Gluten free
- Recipes
tags:
- gluten-free
- food
- mexican
- recipe
- tortilla
---

{{< image src="/wp-uploads/antique_tortilla_press.jpg" alt="" position="center" style="border-radius: 50px;" >}}

Corn (maize) tortillas have to be the quintessential American bread. American as in 'from the Americas', not 'USian'. Several highly organized civilizations flourished in North, Central and South America, all without wheat which was unknown to them. Maize was their staple, and alongside beans, a grass somewhat resembling rice, and potatoes in the South. With these sources of complex carbohydrates who would want for wheat (or barley)?

Of these cultures and culinary traditions practically only one survives, that of the Nahua (Aztec, if you insist), and that is amalgamated with Spanish traditions to form the wonder that is Mexican food. Stuck for a G-F meal? Look no further than 🇲🇽 Mexico 🇲🇽.

We bought a tortilla press many years ago from [Zabar](http://www.zabars.com/)'s in NYC. It was made of aluminium, cost about $20 and broke on its first outing. Sad to say we never made tortillas again... until this morning and, like the [granola](http://stephen.yearl.us/gluten-free-granola/) and the [oatcakes](http://stephen.yearl.us/gf-oatcakes/) we are again finding our palms firmly smacking our heads. WHY did we not do this before? The difference in flavor between these puppies and the corn tortillas we have been subsisting on from Trader Joe's is like the difference between cheese and chalk. These tlaxcalli are sublime, especially the first run off the comal, slathered with farm butter.

## Ingredients
- [Masa harina](http://www.mimaseca.com/es/productos-maseca/d/maseca-maiz-regular/1) (very fine maize flour)
- Salt
- Warm water
- Ratio of water to harina was ~ 1.2 : 1 (350ml : 300g)


Lest I upset any real Mexican that happens to find this page, we used Maseca (masa seca = dry dough, if you ask me) brand masa harina, but that is not real masa harina... it is itself an 'instant' dough (masa) flour (harina), that has some other ingredients added, most notably lard. But it is close enough for me. To be any more authentic we'd have to grow our own corn (done that, actually), and develop a nixtamalization process.

## Method
1. Add water bit by bit until a dough resembling kids' playdough is formed
1. Break off small pieces of dough and form golf-ball sized spheres
1. Flatten into a circle to your required thickness (our first batch varied a _lot_)
1. Cook on a comal/griddle* until golden and delicious, or
1. partially cook on a a griddle, making a batch that can be further grilled at a more convenient time.

We were given a very thin steel comal by a Mexican friend several years ago that I swear was beaten out of an old trashcan lid, and this did the trick perfectly as it was very quick to heat up and cool down. That is rusting overseas right now, so we have co-opted an old cast iron griddle that has been used solely for making *sincronizadas* (quesadillas).

Ah, method. #3 is the one to watch out for here. Since we are without a tortilladora two alternate methods were tried.

### 1. Rolling pin
 The first was rolling out a tortilla on a floured surface. This was entirely unacceptable as the dough is very frangible and frequently tore, and even when it did not, the shape is irregular, and it makes a mess. We are looking for a quick and clean method so that making tortillas becomes second nature, making it next to no hassle to make a fresh batch for breakfast before the kids head off to school in the mornings.

### 2. Ad hoc  press
The second method was opening up a 1 gallon (US ~ 3.78L) zip lock bag, and placing a ball of masa between the layers. The ball was then pressed flat with body weight behind a cast iron skillet. This led to circular tortillas, but considerable stress on our work surface. It seems pretty clear that the real way to go is to get a tortilla press. Internet 'research' thus far seems to indicate that a heavy wooden press made of mesquite wood would be ideal-- most reviews of cast iron tortilladoras indicate a problem with the handle snapping under compression, just like our old aluminium press.

{{< image src="/wp-uploads/ad-hoc-skillet-tortilla-press.jpg" alt="Ad hoc skillet tortilla press" position="center" style="border-radius: 50px;" >}}


## And a little light reading: 
[_Breve historia de un invento olvidado: Las máquinas tortilladoras en México_](http://bidi.xoc.uam.mx/tabla_contenido_libro.php?id_libro=284)

{{< image src="/wp-uploads/tortilla_on_comal.jpg" alt="Tortilla grilling on a comal" position="center" style="border-radius: 50px;" >}}
