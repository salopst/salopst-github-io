---
author: admin
origin: wordpress
comments: true
date: 2012-02-18T04:02:34+00:00
layout: post
draft: false
slug: gluten-free-dumplings-x2
title: Gluten free dumplings (x2)
wordpress_id: 389
categories:
- Gluten free
- Recipes
- Food
tags:
- gluten-free
- recipe
---

The first time I tried to make gluten-free dumplings, they worked out beautifully. Indeed, the existence of this blog is largely down to our desire to record those things that go well. For the record: I have not yet replicated those wonderful dumplings, but am working on it.

{{< image src="/wp-uploads/Chicken-and-cornbuck-dumplings.jpg" alt="Cornmeal buckwheat dumplings." position="center" style="border-radius: 50px;" >}}

-----
-----
-----

## Recipe 1: Cornmeal buckwheat soy dumplings

- 1/3 cup (75 g) yellow cornmeal
- 1/3 cup (35 g) soy flour
- 1/3 cup (55 g) buckwheat flour
- 1/2 tsp. baking powder
- 1 cup (60 g) grated suet
- cold water (about 1/3 cup?)
- Salt/pepper/seasoning to taste


## Method

- Whisk together the dry ingredients

- Mix in the suet until evenly blended

- Add cold water bit by bit until the dough just sticks together.

- You should be able to shape the dough into small balls (see picture under recipe 2).

- Drop the balls into a simmering pot of stew or soup

- Heat for 10-15 minutes at least, or until ready to eat

- The dumplings should float when ready


## Verdict
 Tasty. We like the earthiness added by the buckwheat, and the hearty texture from the cornmeal. However, I do not expect these to hold up. If you want something that will turn into a thickener, these are perfect. I still need to work on the balance a bit, but this batch went down well.

-----
-----
-----

## Recipe 2: Masa dumplings

- 1 cup gluten free flour (my mixture: 75 g masa harina; 5 g tapioca flour; 30 g garfava flour)
- 1 cup (50 g) grated suet
- pinch salt
- pepper to taste
- Cold water


## Method

See above: the same method applies for these as for the cornmeal buckwheat dumplings.

### Mix the dough until it just comes together
{{< image src="/wp-uploads/GF-Buckwheat-batter.jpg" alt="Mix the dough until it just comes together" position="center" style="border-radius: 50px;" >}}


### Then form bite-sized balls
{{< image src="/wp-uploads/GF-cornmeal-buckwheat-dumplings.jpg" alt="Then form bite-sized balls" position="center" style="border-radius: 50px;" >}}


## Verdict

I actually made two versions of the masa dumplings: in one batch, I added a bit of xanthan gum and baking powder. Those fell apart immediately. The others had a nice texture and were quite light, but fell apart overnight. I suspect that masa is not the best flour to use but will need to do more tests to see if it is the culprit (a while back I had another batch of masa dumplings that fell apart). In other words, nice taste and texture, but only good for one meal. After that, they become thickener.

## Some notes

  1. The first thing is, note that when using weights there should be about double the weight of flour to fat (so, 100 g flour to 50 g fat). When using measuring cups, the amount of flour is about the same as fat (about 1 cup flour and 1 cup loosely shredded fat).

  2. From (1) it should be clear that the measurements for cups/weights do not always add up. This is because measuring cups are inherently less precise than scales. For instance, shredded suet has a lot of air in it, so don't worry too much that I have noted a 10g difference in weight for the suet used in two recipes where I estimated it to be "about a cup". If in doubt, use scales; in most cases, it is fine to give or take a few grams or milliliters.

  3. I do not normally use soy flour but had it to hand. For decent cornmeal dumplings, 1/3 cup cornmeal to 2/3 cup other gluten-free flour should work just fine. Fool around with different flours until you find a texture/taste that works for you.
	
  4. While I normally season the dumplings with black pepper, sometimes chili pepper, and with a dash of salt. I did not season the cornmeal buckwheat ones, and they tasted fine.

  5. I am not convinced that the xanthan gum is necessary in the second recipe; baking powder may not be, either. Experiment!
	
  6. If you cannot get suet, try freezing and then grating butter. One normally thinks of the suet as being shredded, but I buy a large frozen chunk from a local farm shop and grate it on demand.