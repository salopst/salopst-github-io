---
author: yearluk
origin: scratch
title: Duolingo Levels and XP
slug: duolingo levels and xp
date: 2018-09-23T06:06:12+01:00
lastMod: 2021-09-19T13:19:12+01:00
draft: false
noLicense: false
weight: 1001
categories:
- Language
tags:
 - language
 - duolingo
---




### lifted from https://forum.duolingo.com/comment/2261330/XP-needed-per-level


- Level 1: **0 → 59** (60 point span)
- Level 2: **60 → 119** (60 point span)
- Level 3: **120 → 199** (80 point span)
- Level 4: **200 → 299** (100 point span)
- Level 5: **300 → 449** (150 point span)
- Level 6: **450 → 749** (300 point span)
- Level 7: **750 → 1,124** (375 point span)
- Level 8: **1,125 → 1,649** (525 point span)
- Level 9: **1,650 → 2,249** (600 point span)
- Level 10: **2,250 → 2,999** (750 point span)
- Level 11: **3,000 → 3,899** (900 point span)
- Level 12: **3,900 → 4,899** (1,000 point span)
- Level 13: **4,900 → 5,999** (1,100 point span)
- Level 14: **6,000 → 7,499** (1,500 point span)
- Level 15: **7,500 → 8,999** (1,500 point span)
- Level 16: **9,000 → 10,499** (1,500 point span)
- Level 17: **10,500 → 11,999** (1,500 point span)
- Level 18: **12,000 → 13,499** (1,500 point span)
- Level 19: **13,500 → 14,999** (1,500 point span)
- Level 20: **15,000 → 16,999** (2,000 point span)
- Level 21: **17,000 → 18,999** (2,000 point span)
- Level 22: **19,000 → 22,499** (3,500 point span)
- Level 23: **22,500 → 25,999** (3,500 point span)
- Level 24: **26,000 → 29,999** (4,000 point span)
- Level 25: **30,000** and up