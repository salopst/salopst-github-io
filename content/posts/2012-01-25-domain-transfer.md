---
author: admin
origin: wordpress
comments: true
date: 2012-01-25T14:17:25+00:00
layout: post
slug: domain-transfer
title: Domain transfer
wordpress_id: 106
categories:
- Tech
tags:
- web
- hosting
---

I can't recall through the fog of my own stupidy why I felt it necessary to `whois` yearl.us. Anyway, surprised to see Enom Inc in the response, and I don't recall them from the initial registration although "registry rocket", a link followed from the ever-so-awesome folks at [http://freedns.afraid.org](http://freedns.afraid.org). So, being clouded I thought "scam", "buyout". "Oh, woes!", and cast around for a registrar to which to transfer.

Cloudy fog of indignation made somewhat more insidious in that I could not log into [hregistryrocket.com](http://registryrocket.com) "Error: domain does not exist".

Yeah, well. Turns out that "registry rocket" is an automated system to allow resale of Enom's services, and afraid.org, in order to make a little extra cash, had used this system. Of course, by this point, I'd already gotten my epp code and had selected [http://dynadot.com](http://dynadot.com) to be my new registrar.

Deal, still not done. What to do? Current registration is a little more expensive, but I'm supporting [freedns.afraid.org](http://freedns.afraid.org). Solved with teh resolution that I'll donate some subdomains and send a few $$ in their general direction. Plus, I get to try out this whole transfer business... never done that before.