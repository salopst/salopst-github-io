---
author: admin
origin: wordpress
comments: true
date: 2015-09-15T21:38:40+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: winging-a-damson-apple-and-custard-pie
title: Winging a damson, apple and custard pie
wordpress_id: 994
categories:
- Gluten free
- Recipes
- Food
- Good Life
tags:
- nom-nom
- damsons
---

My mother seems to have had rather a lot of damsons on her tree this year. This heavy cropping combined with heavy rain over the last few days has lead to some immediate load-reduction on the broken limb. Fsck the impecunious nature of the (lack of?) pruning over recent years.

{{< image src="/wp-uploads/2015-09-15-broken-damson-tree-limg.jpg" alt="Broken damson tree limb" position="center" style="border-radius: 50px;" >}}


I think some kind of everything-damson is in order. But let's start with a pie. Now, I cannot bake for toffee so this should be fun...

The process is play-as-you go. The measurements below are play as I went, and are accurate.

I'm gluten intolerant (yeah, yeah... whatever), so I'm using [Dove's GF self-raising flour](https://www.dovesfarm.co.uk/flour-and-ingredients/gluten-and-wheat-free-s-r-white-flour-x-1kg/).

## Pastry Ingredients:
- 100g butter (unsalted)
- 300g Dove's self-raising
- 75g light brown sugar
- 3 large egg yolks

Right. I half remember beating sugar and butter as the basis for pretty much everything that is (non-bread related) baking from home economics class. Yes, I am that old. So beat the sugar and the butter until creamy, then add the egg yolks. Delicious. Then add the sifted flour.

🎲🎲 Dice! Dough. What sort, I do not know.

{{< image src="/wp-uploads/damson-custard-pie.jpg" alt="Damson custard pie" position="center" style="border-radius: 50px;" >}}

Roll out with used wine bottle, and lay over tin/aluminium/kitchen foil, add another layer of foil on top then some bean-like things and bake for 10 minutes at 220 ºC. "It's not smooth," you say? "Bollox!" I say. "I'm new. It's my first day."

{{< image src="/wp-uploads/damson-custard-pie-3.jpg" alt="Damson custard pie (1)" position="center" style="border-radius: 50px;" >}}


#### The jam-like bit:
Take damsons from tree. Rinse etc. Add some unkonwn apple specie from apple tree to the damson's tree's southern side--- the sour-ish one where the tree swing is hanging. No idea how much of each... just mixed and simmered with brown sugar and a little water until it tasted good, and was reduced to jam. I added some xantham gum to thicken because time was running short. Oh, and some fresh vanilla pod-scraping that I'd soaked in water, but I realise that this was a pretty silly place to put it. It should have been in the custard topping.

{{< image src="/wp-uploads/damson-custard-pie-2.jpg" alt="Damson custard pie" position="center" style="border-radius: 50px;" >}}

The spread your jam on your blind-baked pie base. Simples. Meanwhile make custard.


#### Custard:
I was out of cream, so I took:

- 60g of clotted cream, and
- 300 ml of full-fat milk, and

heated both in the microwave, gave it a good beating then added to 3 more eggs yolks and 1 egg (and some more vanilla) and took that to the stove to make custard.

The custard, of course, goes over the top of the jam once it is done. It is sprinkled with more sugar, and the whole thing is baked again for 20 mins at whatever temp. I mentioned above.


#### Result:
{{< image src="/wp-uploads/damson-custard-pie-5.jpg" alt="Damson custard pie-- finished" position="center" style="border-radius: 50px;" >}}

#### Post scriptum:
Sweet base. Sweet jam. Not enough custard to balance the thing.

##### Too much sugar!!

But it is damned tasty. And it is "honest", and homemade.
