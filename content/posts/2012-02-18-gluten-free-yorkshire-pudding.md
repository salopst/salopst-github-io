---
author: admin
origin: wordpress
comments: true
date: 2012-02-18T04:35:07+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: gluten-free-yorkshire-pudding
title: Gluten-free Yorkshire pudding
wordpress_id: 390
categories:
- Gluten free
- Recipes
- Food
tags:
- gluten-free
- recipe
---

As if a Yorkshire pudding can be gluten-free: please send suggestions of a suitable name for a GF version of something akin to a Yorkshire pudding.

{{< image src="/wp-uploads/GF-Yorkshire-Pudding.jpg" alt="The pudding, still reasonably puffed up." position="center" style="border-radius: 50px;" >}}

-----
-----
-----

## Ingredients
	
- 150 g gluten-free flour[^1]
- 50 g buckwheat flour
- 50 g brown rice flour
- 1/2 tsp baking powder
- 1/2 tsp xanthan gum
- 4 eggs
- 450 ml milk[^2]
- tbsp animal fat (beef dripping, lard)


[^1] I had exactly 150 g left of a large batch of GF flour I'd made with: equal parts rice flour and garbanzo flour, plus a bit of corn starch to equal 150 g; 1 tsp each of xanthan gum and baking powder

[^2] The non-GF recipe called for 150 ml milk and this is quite an increase. However, since many GF flours are very fine and have a high surface-to-volume ratio, they absorb more liquid than wheat flours. In order to achieve the proper consistency in the batter before setting, I added quite a bit of extra milk. I would suggest adding the liquid bit by bit-- putting the extra in by additional 50-100 ml measures.

-----
-----
-----

## Method

- Whisk together the dry ingredients

- Make a well in the middle of the flour mix, add the eggs, and beat the eggs together first, then with the flour

- Add the milk and mix until evenly blended and the batter is thin

- Let the batter set for about half an hour[^1]

- In this time, preheat the oven to 245 C or 475 F. [I started at 230/450 but went up to 260/500 after 15 minutes]

- While the oven is heating up, put the animal fat in a cast iron skillet/frying pan

- When the oven is heated and the fat is sizzling, pour in the batter

- Place in the oven and cook for 20-30 minutes (original flour recipe called for 15-20, mine was in for 30)


{{< image src="/wp-uploads/GF-Yorkshire-pud-batter.jpg" alt="The batter should be quite runny" position="center" style="border-radius: 50px;" >}}

[^1] The batter should be quite runny before resting.

-----
-----
-----

{{< image src="/wp-uploads/GF-Yorkshire-slice.jpg" alt="A slice: lighter in taste and texture than it looks" position="center" style="border-radius: 50px;" >}}

