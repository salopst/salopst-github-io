---
author: admin
origin: wordpress
comments: true
date: 2015-08-10T19:33:17+00:00
layout: post
slug: top-100-german-verbs
title: Top 100 German verbs
wordpress_id: 991
categories:
- German
- Language
tags:
- words
- verbs
- German
---

From Randall Jones & Erwin Tschirner’s [A Frequency Dictionary of German: Core Vocabulary for Learners](http://www.amazon.com/Frequency-Dictionary-German-Vocabulary-Dictionaries/dp/0415316332). 

|     | DE infin      | EN infin                         |
|-----|---------------|----------------------------------|
| 1   | sein          | to be                            |
|     | haben         | to have                          |
|     | werden        | to become                        |
|     | können        | can, to be able to               |
|     | müssen        | must, to have to                 |
|     | sagen         | to say                           |
|     | machen        | to do, make                      |
|     | geben         | to give                          |
|     | kommen        | to come                          |
| 10  | sollen        | should, ought to                 |
|     |               |                                  |
|     |               |                                  |
|     | wollen        | to want                          |
|     | gehen         | to go                            |
|     | wissen        | to know                          |
|     | sehen         | to see                           |
|     | lassen        | to let, allow, have done         |
|     | stehen        | to stand                         |
|     | finden        | to find                          |
|     | bleiben       | to stay, remain                  |
|     | liegen        | to lie, be lying                 |
| 20  | heißen        | to be called                     |
|     |               |                                  |
|     |               |                                  |
|     | denken        | to think                         |
|     | nehmen        | to take                          |
|     | tun           | to do                            |
|     | dürfen        | may, to be allowed               |
|     | glauben       | to believe                       |
|     | halten        | to stop, hold                    |
|     | nennen        | to name, to call (a name)        |
|     | mögen         | to like                          |
|     | zeigen        | to show                          |
| 30  | führen        | to lead                          |
|     |               |                                  |
|     |               |                                  |
|     | sprechen      | to speak                         |
|     | bringen       | to bring, take                   |
|     | leben         | to live                          |
|     | fahren        | to drive, ride, go               |
|     | meinen        | to think, have an opinion        |
|     | fragen        | to ask                           |
|     | kennen        | to know                          |
|     | gelten        | to be valid                      |
|     | stellen       | to place, set                    |
| 40  | spielen       | to play                          |
|     |               |                                  |
|     |               |                                  |
|     | arbeiten      | to work                          |
|     | brauchen      | to need                          |
|     | folgen        | to follow                        |
|     | lernen        | to learn                         |
|     | bestehen      | to exist, insist, pass (an exam) |
|     | verstehen     | to understand                    |
|     | setzen        | to set, put, place               |
|     | bekommen      | to get, receive                  |
|     | beginnen      | to begin                         |
| 50  | erzählen      | to narrate, tell                 |
|     |               |                                  |
|     |               |                                  |
|     | versuchen     | to try, attempt                  |
|     | schreiben     | to write                         |
|     | laufen        | to run                           |
|     | erklären      | to explain                       |
|     | entsprechen   | to correspond                    |
|     | sitzen        | to sit                           |
|     | ziehen        | to pull, move                    |
|     | scheinen      | to shine, seem, appear           |
|     | fallen        | to fall                          |
| 60  | gehören       | to belong                        |
|     |               |                                  |
|     |               |                                  |
|     | entstehen     | to originate, develop            |
|     | erhalten      | to receive                       |
|     | treffen       | to meet                          |
|     | suchen        | to search, look for              |
|     | legen         | to lay, put                      |
|     | vor·stellen   | to introduce, imagine            |
|     | handeln       | to deal, trade                   |
|     | erreichen     | to achieve, reach                |
|     | tragen        | to carry, wear                   |
| 70  | schaffen      | to manage, create                |
|     |               |                                  |
|     |               |                                  |
|     | lesen         | to read                          |
|     | verlieren     | to lose                          |
|     | dar·stellen   | to depict, portray               |
|     | erkennen      | to recognize, admit              |
|     | entwickeln    | to develop                       |
|     | reden         | to talk                          |
|     | aus·sehen     | to appear, look (a certain way)  |
|     | erscheinen    | to appear                        |
|     | bilden        | to form, educate                 |
| 80  | an·fangen     | to begin                         |
|     |               |                                  |
|     |               |                                  |
|     | erwarten      | to expect                        |
|     | wohnen        | to live                          |
|     | betreffen     | to affect, concern               |
|     | warten        | to wait                          |
|     | vergehen      | to elapse                        |
|     | helfen        | to help                          |
|     | gewinnen      | to win                           |
|     | schließen     | to close                         |
|     | fühlen        | to feel                          |
| 90  | bieten        | to offer                         |
|     |               |                                  |
|     |               |                                  |
|     | interessieren | to interest                      |
|     | erinnern      | to remember                      |
|     | ergeben       | to result in                     |
|     | an·bieten     | to offer                         |
|     | studieren     | to study                         |
|     | verbinden     | to connect, link                 |
|     | an·sehen      | to look at, watch                |
|     | fehlen        | to lack, be missing, be absent   |
|     | bedeuten      | to mean                          |
| 100 | vergleichen   | to compare                       |