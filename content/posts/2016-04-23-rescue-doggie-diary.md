---
author: admin
origin: wordpress
comments: true
date: 2016-04-23T16:20:44+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: rescue-doggie-diary
title: Rescue Doggie Diary
wordpress_id: 1015
categories:
- Dog
- Good life
tags:
- animal
- behavior
- canine
- dog
---

{{< image src="/wp-uploads/2016-04-14-monty-runs-hillbrae.jpg" alt="Monty the the paddock at Hillbrae" position="center" style="border-radius: 50px;" >}}

## 2016-04-17 Sunday
Rescue Dog event in Shrewsbury.... thought it was at [Grinshall Animal Rescue](http://www.grinshillanimalrescue.co.uk/), but they were only sponsors, and the event itself was at the Quary in in town. Rather too late in the day to head to that, so went to [Hilbrae Pets Hotel](http://www.hilbrae.co.uk/), about 10 mins away. Spent about 30 mins walking "Monty":

- Week two here: [http://stephen.yearl.us/rescue-doggie-diary-week-2](http://stephen.yearl.us/rescue-doggie-diary-week-2)

- Inmate # A1850
- Incarcerated: 2016-03-07
- Microchip ID: 958000001334904
- B&W Collie Cross-- with what? Shepard? Lab? Staffie?
- Not castrated
- Defo not a working dog-- does not have the eyes
- Gun shy

{{< image src="/wp-uploads/2016-04-14-montys-gaol.jpg" alt="Puppy's locus of incarceration" position="center" style="border-radius: 50px;" >}}

- Advised that he was last with a farmer, and Monty took to roaming when not supervised

- Good with older kids and adults. Good with dogs. Likes to chase cats and birds. Nothing erratic and unpredictable in other words

- Walked well on lead. Very interested in field mice, lots of sniffing and marking... prolly because not neutered.

So, I "Reserved" Monty... Animal control person to come by mid-week to check on fencing and such.


## 2016-04-19 Tuesday
Walked with the `Mont`ster, played in the paddock, fetching stick. Came when called. Likes to play tug, not very good at letting go of a throw toy. Completely ignored donkeys in paddock next door, and farmers ploughing.

Stool very runny. Apparently he eats only once in the AM.... mixture of wet cans and kibble... whatever Hilbrae get from donations, really.

Think I may have trodden on his toe at one point-- let out a bit of a yelp.

Seems a little nervous.

Sat with Monty on bench watching other dogs come and go. No bother. Joked with worker that I'd take him home today if I could. That actually happened. £85 adpotion fee.

Some confusion about vaccinations-- 2016-01-18 by Shropshire Vets (+44 1743 860 920) on  the farm. Does he do so many that he cannot issue certifications? How is not issuing a certification not illegal? Surely vet was paid, so where is the invoice?

No problems with him jumping in the hatchback, though he did come over to the back seat and was very interested in being in the front. Drove slowly. Very slowly.

Late in the day, and knowing of fence gaps at home, kept him on leash. Short loop Vicarage Lane and up through the village.

Day bed in living room. No couch. Slept next to me in bedroom, on floor on old duvet.

Tug toy purchased from Whitburn street pet shop; and lamb and rice kibble by James Wellbeloved. Available on [Amazon](https://www.amazon.co.uk/James-Wellbeloved-Food-Adult-Kibble/dp/B002VJR58I/ref=sr_1_2?s=pet-supplies&ie=UTF8&qid=1461436390&sr=1-2&keywords=james+wellbeloved+dog+food), isn't everything?

## 2016-04-20 Wednesday
Early morning walk to the Costcutter. Met dog walkers on the way all good.
Slightly longer loop down Vicarage Lane and up through the village. More livestock. All good.

[TFM](http://www.tfmsuperstore.co.uk/) to get some fencing wire. Better in the car. Less tendency to move to the front where all the action is. Took him out to see the wire... tried to mark territory. Dude who picked up wire and placed it in car was biggish, hi viz. Patted Monts. No bother. Ate some Tyrell's crisps.

Walk by the river in B'north after an appointment. BC had a bark at him. No response. Still lots of sniffing and marking. No interest in ducks and geese. Sat at the back of the Fosters Arms... little boy shouting "Look. Doggie." No issues. Feeling good about this pooch.

Stools much firmer.

Found Sammy's old football. Played tug in the house. Football destroyed. Noticed that Mont's lower canines are somewhat worn.


## 2016-04-21 Thursday

[Happy birthday, Lizzie.](https://www.youtube.com/watch?v=39vcPT7roFU)

Front gate damaged, bent. Put back into rough shape w/ blow torch and sledgehammer. Monts maybe a little freaked because when I called him next he was gone. See Facebook for more details. Apparently he "attacked" another dog. Possible. I do not know for sure.

Woman brought him back. Stuck her face in his and he was not bothered. I was on edge though.

Rest of the day spent with him off-lead in the garden. Seemed to have no interest in the >>cough<< 22 m missing section of hedge. Solar Panel Dude came by... bit of a bark, but came to see us in the shed when SPD gave his presentation. Sniffed, then went outside.

Bark at the postie. Perhaps he is recognising this place as his, wants to protect it? Neither  person seemed threatened.

Had twenty minutes cycles of play and my cutting grass. Seemed a little freaked at the staring of various small engines, but was fine once they were running. No chasing of lawnmower.

Went for a pint in the Bache. Free run outside. Delivered glass to counter, Monts came though. No interest in the people inside.


### 2016-04-22 Friday
I feel like shit for some reason. Very slow day. Morning getting some JSA shit done, Mont's merely had a couple of pee 'n' poos up the yard.

Afternoon up to Bridgnorth and a walk along the river.

Springer comes bounding across an invades our little game of shake hands. Monty slips and there is a bit of a scuffle. Do not know why... the surprise? Does he want to protect me? Does he feel that I  cannot protect him? NO punishment. Who knows if that was the right thing to do. Thought it best just to ignore it and remain calm. Did scramble like a mad bastard when it happened.

Springer owners very apologetic. Mont's space was defo violated.... but still he should not have had that response. Equally, I should be more aware and head these things off before they happen. I AM taller and can see father! My nose is not as awesome though!

Does he not like the dry kibble? Seems not to be eating that much... bought some Tesco "[Chunks in Jelly](http://www.tesco.com/groceries/product/details/?id=274776848)"


### 2016-04-23 Saturday
Came out to greet delivery guy at the gate (fencing tools-- yay)... Monts had a bark, hackles slightly raised. Protecting his property?

About Mid-day went down toward Brook's mouth, practised closer leash walking and "here" to get him to come back and reset before we set off again. I guess if he associates a slack lead with walking, and tight lead with stopping he will soon get the message. Should be careful with over-praise/treats when he does come back to "reset", lest he associate this fuss with tight lead.

And then off to the Brook... first introduction to water. Does not look like he's a swimmer, but he did venture in, digging rocks out of the bed, and fetching sticks in shallow water.

Met no people or other animals. Drove to get us to the two spots. Seems to be getting better and not rushing out of the car. "wait" is not perfect, but it is pretty good.

Went out for about 1/2 hour without him. Just left. No fuss. VERY excited when I got back. Ignored him until I was ready... stuff put away, shoes off, etc. then a simple scratch on the head. My coming and going should not be a big deal deal to him. He's already pretty nervous. Need to build the fellow's confidence! Tomorrow will be interesting since I am out pretty much most of the day playing cricket.

Fir the first time he went and put himself to bed... thus leaving the room I was occupying. That's a good sign.

{{< image src="/wp-uploads/2016-04-23-monty-borle-brook.jpg" alt="Monty at Borle Brook" position="center" style="border-radius: 50px;" >}}

## 2016-04-24 Sunday
Spent about an hour and a half on the Alveley side (mostly) of the Severn Valley Country Park. No drama to be had at all on the meeting with doggies... all of whom were off leash. Montgomery Montesquieu, AKA The Montster, AKA Monty was, of course, on-lead.
* first off: black lab getting out of car on Highley side. Monts by out car. No fuss
* Rescue BC from [Birch Hill Dog Rescue](http://www.birchhilldogrescue.org.uk), Cleobury, female, 7 years. Very nervous she was, tail tucked between legs. She stood some way off not looking at any of us. Monty's hackles slightly raised.  Had him sitting next to me as I shatted with the guy. No sign of Monts be furhter interested in the bitch. Got noticeable calmer with my chatting.
18 mo. German Shepard bitch, "Shadow" off-leash by the river. Again distracted Monts with liver, other dog was insistent on coming over. They smelt each other. I felt nervous. Monts hacked raised. No outward signs of aggression, wanting to lunge etc. He just seemed wary, and "stand-off-ish".
* Three Lurchers/Greyhounds mid-bridge. Plenty of time to see them coming. Did some "stop", "heel", "sit", "wait" basic obedience stuff to distract Monts at bridge end. They came and crowded him. Hackles slightly raised, but he was receptive to some more "sit" and "here". Again nothing happened but smells all around, but you can see he is nervy. Getting the impression he has been attacked in the past.

Some good loose leash work. Still a very heavy sniffer... that ain't going away. But I figure if I associate his sniffing with "go sniff", than I can convince him then it is more OK some times than others. He's slowly getting the message that tight lead either means I do not walk, or I turn abruptly around. Also getting more attention from him when just walking loose-leash.

Stopping and waiting at gates and styles, top and bottom of steps/stairs for me to go first, and then only to move on a "let's go". Is pretty damned good.

After cricket... came back some 7 hours later to an effusive welcome which I ignored untl he was calm, then just patted him. PnP up the yard then smacked an old baseball around with him for a bit. That was a hit! Gonna have to teach the fella not to chase red balls, however! Nothing chewed up or urinated on... so more signs that is following me around is not a precursor to separation anxiety.

### Week two here: [http://stephen.yearl.us/rescue-doggie-diary-week-2](http://stephen.yearl.us/rescue-doggie-diary-week-2)
