---
author: admin
origin: wordpress
comments: true
date: 2015-07-21T21:35:37+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: window-managers-for-os-x
title: Window Managers for OS X
wordpress_id: 954
categories:
- Tech
- Apple
tags:
- OS X
- window management
---

Playing with [Karabiner](https://pqrs.org/osx/karabiner/) and searching for others' keybindings has led me somewhat down the rabbit hole of window management on OS X. There seem to be a bunch, some light-weight, some heavy. Some under active development, others that seem to be abandoned.

Managing windows can be a PITA, lots of precision dragging and resizing and all to get the windows into the desired position. Several window managers exist for OS X, and a short, short list would include:

- moom: [http://manytricks.com/moom/](http://manytricks.com/moom/)

- hyperdock: [https://bahoom.com/hyperdock/](https://bahoom.com/hyperdock/)

- spectacle: [http://spectacleapp.com/](http://spectacleapp.com/)

- divvy: [http://mizage.com/divvy/](http://mizage.com/divvy/)

- slate: [https://github.com/jigish/slate](https://github.com/jigish/slate)


 Slate seems to have been top dog for a while, but is no longer in active development. It would seem that it was replaced by mjolnir [https://github.com/sdegutis/mjolnir/](https://github.com/sdegutis/mjolnir/) which is not a window manager _per se_ but an automation enviroment (using Lua). Indeed a little like— and superseded by:

-  Hammerspoon: http://www.Hammerspoon.org

[Installing Hammerspoon](http://stephen.yearl.us/installing-Hammerspoon/)


The keyboard shortcuts in the Hammerspoon example are predicated on setting up a "hyper" key, a meta modifier in Karabiner. More on that later.

Of course all these “auxiliary” apps that make life a little easier on OS X seem to want a place on the menu bar… and that leads into apps that tidy up that menu bar:

# bartender: [http://www.macbartender.com/](http://www.macbartender.com/)
