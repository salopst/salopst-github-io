---
author: admin
origin: wordpress
comments: true
date: 2012-01-29T14:02:46+00:00
layout: post
slug: wordpress
title: Wordpress
wordpress_id: 99
categories:
- Tech
tags:
- blog
- web
- wordpress
---

Fsked the [http://mary.yearl.us](http://mary.yearl.us) WP install last night whilst arsing with names. One single bloody typo in "site URL" and good luck getting back into the admin panel. WP does not even offer a warning that changing this might be a bad thing, or potentially so.

Fortunately having access to the database I could edit the value in-place in the tables... now all is well again with the world.

...except for the fact that I felt I had to sign up--finally-- for a twitter account just so I could get an API key in order to explore some things on Mar's blog. And it turns out another yearl.* has @yearlus as his handle. Grrr. for some reason this bothers me. Bothers me to the extent that I offerered him a free subdomain of his choosing under yearl.us. I'd take that deal if I were him.
