---
author: admin
origin: wordpress
comments: true
date: 2012-03-16T19:03:14+00:00
lastMod: 2021-09-21T02:22:00+01:00
draft: false
layout: post
slug: pipe-rustication-experiment
title: Pipe rustication experiment
wordpress_id: 208
categories:
  - Uncategorized
tags:
  - pipes
  - poetry
  - tobacco
  - ebay
---

{{< image src="/wp-uploads/2012-03-16-captain-kidd-pipe.jpg" alt="Captain Kidd smoking pipe" position="center" style="border-radius: 50px;" >}}

I purchased this pipe from Ebay at a reasonable price. This was the only picture of the pipe, and I was a little dissapointed to find that on the shank (on the other side, obviously) there was a sizeable chunk of filler, about 2mm in width, 1mm in height rouhtly in the shape of the outline of Autralia.

I was tempted to repackage it and list the item as described by the original seller and just hope that I managed to get my money back. But I didn't, for, you see, Capt. Kidd-- despite the ridiculous name-- is a 'seconds' line of the famed [BBB](http://pipedia.org/index.php?title=BBB) brand. BBB (Britain's Best Briars) is reputedly Britain's oldest trademark, with the more famous red triangle of Bass ale being second, and the first to employ an image. Funny how the two oldest trademarks are for products that involve smoking and drinking, is it not?

Filling her up with some of Rattray's [Hal o' the Wynd](http://tobaccoreviews.com/blend_detail.cfm?ALPHA=H&TID=954) (see the poem, below), I enjoyed a very nice smoke. It is a nice pipe-- no gurgle, no hiss, no whistle. The air-hole is drilled dead on centre. I'll keep the pipe, but oh how the filler vexeth me.

There seems but one course of action open to me. Hop on over to youtube and get some instruction on rusticating the darn thing. Two videos were of immense help, the first proving it was do-able, and the second more confidence inspiring: so thanks to "flieger671" for [Pipe Carving - Appendix 1, Rustication](http://www.youtube.com/watch?v=mtd3laeISuc) and "soulmirrors" for [A Very Rustic Pipe Rustication](http://www.youtube.com/watch?v=V0VQYTczlj4&lc)!

Step one was to remove the existing polish and stain, this I did with isopropyl alcohol and some sanding with 320 grit wet-and-dry paper. The stain was some old walnut wood stain I had lying around. I understand that pipemakers generally use alcohol-based leather dyes as this penetrates deeper into the briar, but this was all I had lying around.

Then I hacked at the briar in little semi-circles, randomly over the sueface of the briar with a 3mm chisel I found lying in the botton of a toolbox. I did try to use a power drill initially, one that I had clamped in a broken desk vice. Yes the drill fell out. Yes one more that one occasion the bit skidded along the bowl. No this was not safe.

After ten minutes of hacking and re-staining, first in "antique pine" and then again the walnut I had:

{{< image src="/wp-uploads/2012-03-16-captain-kidd-pipe-rusticated.jpg" alt="Capt. Kidd smoking pipe 'rusticated'" position="center" style="border-radius: 50px;" >}}


And I am pretty happy with that.

Oh, and of course I polished the stem. Starting with 600 grit emery cloth and running progressively though 600, 800, 1000, 1500, 2000 and finally 3000. Pictures do not do the stem credit. It is like black glass.

-----
-----
-----

### Hal o' the Wynd (1942)
#### William Souter
<audio controls>
  <source src="/wp-uploads/hal-o-the-wynd.mp3" type="audio/mpeg">
Your browser does not support the audio element.
</audio>

```poetry
Hal o' the Wynd, he taen the field
Alang be the skinklin Tay:
And he hackit doun the men o' Chattan;
Or was it the men o' Kay?

Whan a' was owre he dichted his blade
And steppit awa richt douce
To draik his drouth in the Skinner's Vennel
At clapperin Clemmy's house.

Hal o' the Wynd had monie a bairn;
And bairns' bairns galore
Wha wud speer about the bloody battle
And what it was fochten for.

"Guid-faith! My dawties, I never kent;
But yon was a dirlin day
Whan I hackit doun the men o' Chattan;
Or was it the men o' Kay?"
```

