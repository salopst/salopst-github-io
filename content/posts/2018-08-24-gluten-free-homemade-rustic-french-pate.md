---
author: admin
origin: wordpress
comments: true
draft: false
date: 2018-08-24T20:32:07+00:00
layout: post
slug: gluten-free-homemade-rustic-french-pate
title: Gluten Free Homemade Rustic French Pâté
wordpress_id: 1433
categories:
- French
- Gluten free
- good life
- Recipes
---

{{< image src="/wp-uploads/2018-08-24-rustic-pate.jpg" alt="Gluten Free Homemade Rustic French Pâté" position="center" style="border-radius: 50px;" >}}

### Delia Smith's (kinda) --- le pâté grossier

It's in foreign because Number One Son with his most excellent French shamed me into realising that I have been resting on my _regarde-mon-stylo_, pre-O-level French for too long, and that I really should make more of an effort. How hard can it be, really? On the similarities betwixt English and French, did Dumas not say that English was just French badly pronounced?

Soooo, with apologies to Francophones, and recognition that no google translates were harmed in my butchering of the language, an homage to pâté...



### Les ingrédients


- 350 g [450] de porc haché (ca. 90% belly; 10% shoulder)
- 1 cuillère à café pleine de thym frais haché
- 120 ml de vin blanc sec
- 25 ml de cognac (single malt whisky),
- [1 oeuf]
- 450 g [see #1 and belly pork] de rashers de porc britanniques, avec autant de gras que possible
- 275 g [235] de bacon britannique fumé à sec
- 225 g [650] de foie de porc britannique (of fucking course)
- 20 baies de genièvre
- 20 grains de poivre noirs entiers
- 1 cuillère à café pleine de sel
- ¼ [NONE] cuillère à café arrondie macis moulu 
-  2 [4] grosses gousses d'ail écrasées


**[]**s == my modifications

**Pour garnir:**
- feuilles de laurier fraîches
- quelques baies de genièvre supplémentaires



### le Méthode


- Préchauffez le four ... 170°C [150ºC]

- Hacher la viande très finement (ou utiliser un robot culinaire, le foie en dernier parce qu'il est le plus sale).

- Mélangez bien le vin et l'armagnac (ou [Glenlivet Founder's Reserve](https://www.thewhiskyexchange.com/p/28323/glenlivet-founders-reserve) FTW), l'ail, le poivre noir, le sel marin de Maldon, le thym du jardin, que l'on a broyé avec soin dans un pilon et un mortier.

- Mettez en une terrine ou une moule de pain, ~~~faites des mots grossiers avec~~~ décorer des baies de genièvre et des feuilles de laurier, au bain-marie au four pendant 90 minutes.


- Laisser refroidir (ne pas égoutter les jus environnants) puis, lorsque le pâté est froid, placer une double bande de papier d'aluminium sur le dessus et mettre quelques poids pour le presser pendant au moins quelques heures.


- EAT!

Purchased this on a whim. Quite decent, really:
{{< rawhtml >}}
<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=GB&source=ac&ref=tf_til&ad_type=product_link&tracking_id=yearlus-21&marketplace=amazon&amp;region=GB&placement=B00TSAH04M&asins=B00TSAH04M&linkId=cd12ae73f0a953fd3b31c256b901851d&show_border=false&link_opens_in_new_window=false&price_color=333333&title_color=0066c0&bg_color=ffffff">
</iframe>
{{< /rawhtml >}}