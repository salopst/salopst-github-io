---
author: mary.yearl
origin: wordpress
comments: true
date: 2012-01-31T21:10:02+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
slug: sr-71-maiden-launch
title: SR-71 Blackbird Maiden launch
wordpress_id: 385
categories:
- Rockets
tags:
- rockets
- craft
- model
---

{{< image src="/gallery/sr-71-blackbird-launch/img_2537.png" alt="SR 71 Rocket Launch" position="center" style="border-radius: 50px;" >}}

So, school for Joe and Sam was out early today and we thought this afternoon an appropriate time to put the [model SR-71 Blackbird rocket](/sr-71-model-rocket-construction/) that Joe and Stephen put together over the weekend out for its maiden launch. There was quite some interest amongst Joe's 3rd Grade class as Mary yesterday had helped Joe at his "show and tell", where he told his peers about the launches he has been apart of, both at structured events like at [Goddard Space Flight Center](http://www.nasa.gov/centers/goddard/home/index.html), MD, and also private launches with his friends and family.

This pictures from this afternoon's launch are in this gallery: [http://mary.yearl.us/sr-71-blackbird-launch/](/gallery/sr-71-blackbird-launch/).

We had three flights, each using B6-4 motors. Given the mass of the model (0.79 KG with motor), a B-class motor seemed the most appropriate for the maiden launch. The fact that we were in a rather small area with other folk' children present, and we were unsure of the zoning restrictions placed on such activity also led us to the rather lower thrust motor. In hindsight a B6-2 would have been perfect for the afternoon as the four second delay until recovery parachute deployment was a little too long: wice the model took a nose dive and planted itself firmly in the ground.

Wikipedia has more information on [model rocket engine nomenclature](http://en.wikipedia.org/wiki/Model_rocket#Motor_nomenclature) and [classification](http://en.wikipedia.org/wiki/Model_rocket#Motor_nomenclature). Basically, the letter indicates class of rocket in impulse velocity. The first letter he average thrust, and the final number represents the delay from engine thrust burnout until ignition of the ejection charge (and therefore recovery parachute deployment.

Youtube video #1 ([SR-71 Model rocket maiden launch](http://www.youtube.com/watch?v=5PHiSkwIh54))

Youtube video #2 ([ SR-71 Model rocket 2nd flight ](http://www.youtube.com/watch?v=AjJQ4oAjgpY))
