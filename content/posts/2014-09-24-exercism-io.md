---
author: admin
origin: wordpress
comments: true
date: 2014-09-24T09:11:11+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: exercism-io
title: exercism.io
wordpress_id: 614
categories:
- Tech
tags:
- code
- education
- websites
---

So, I came across this site last night when scouting around for tools/sites that Eldest child might like to use as he gets to grips with programming, that is to do so without my necessarily having to hold his hand. The decisions I might make may or may not be reasonable or even appropriate. Better that he get a variety of experienced feedback from others (I will always be there), and in whatever language he chooses.

[exercism.io](http://exercism.io) is, essentially a community. Programming exercise sets, in numerous languages, are submitted and vetted by the community. One downloads these exercises, writes code, and submits waiting for feedback. It is not a learning environment _per se_, well not in the "instruct me" sense at least.

I find it fascinating because it is NOT based on a spiffy AJAXed-up website with its own editing environment and submit process. One installs a simple CLI tool, and uses that to download and submit to [git](https://github.com). Submitted ruby code for calculating [hamming distances](http://rosalind.info/problems/hamm/) in genetic sequences this AM to see how it all worked together, and it did. Flawlessly. I guess the downside is that one needs an environment set up on a machine, and a workflow, and #1 Son doesn't have that yet. That, and I don't think the problem sets would exactly tickle his curiosity.

There are several options for the CLI program install, one of which uses [homebrew](http://brew.sh/). I chose that, and in the process [cleaned-up](http://stephen.yearl.us/cleaning-up-a-homebrew-installation/) a three-plus-year-old-install of that.
