---
author: admin
origin: wordpress
comments: true
date: 2012-02-11T23:54:59+00:00
layout: post
slug: buckwheat-pancakes
title: Buckwheat pancakes
wordpress_id: 386
categories:
- Food
- Recipes
tags:
- buckwheat
- gluten-free
- recipe
---

We are big lovers of cornmeal pancakes in our family, but the boys were hankering for something closer to traditional wheat ones, so we figured it was time to throw together a batch of buckwheat pancakes.


{{< image src="/wp-uploads/Buckwheat-pancakes.jpg" alt="Buckwheat pancakes" position="center" style="border-radius: 50px;" >}}

Yield: 7 large pancakes

### Ingredients

- 1 cup (120 g) buckwheat flour
- ½ cup (20 g) masa harina
- ¼ tsp. baking soda
- 2/3 tsp. baking powder
- pinch of salt
- 1 tbsp. maple syrup
- 1 ¼ cup (300 ml) milk
- 1 tbsp. butter, melted
- 3 eggs*

The original recipe called for 1 egg, but I wanted additional egg whites to make the pancakes nice and fluffy. I did add the yolks to the batter, but one needn’t do so.


## Method

- Whisk the dry ingredients together in a bowl.

- Mix together the milk, syrup, melted butter, and egg yolk in another bowl.

- Combine the wet and dry ingredients and stir until they are just mixed

- Beat the egg whites until they are stiff but not dry
	
- Fold the egg whites into the batter until just mixed
	
- Cook away! We use a cast-iron griddle on medium-high heat


These went down very well indeed, though next time I might alter the mixture to make them more crêpe-like, as requested by one of the lads.
