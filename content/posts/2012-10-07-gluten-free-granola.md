---
author: admin
origin: wordpress
comments: true
date: 2012-10-07T17:30:29+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: gluten-free-granola
title: Gluten-free granola
wordpress_id: 394
categories:
- Gluten free
- Recipes
tags:
- gluten-free
- recipe
- food
---

Please, someone, tell me why I did not make granola earlier: for years, I have shied away from purchasing the stuff at the store because it all seems too “fancy” with artificial fruit flavours, etc., that simply do not appeal. What set me to work was seeing “GF granola” on our shopping lists, and realizing just how expensive it is. Really? For what is basically glorified oats? So, I set to; and behold!

{{< image src="/wp-uploads/GF-granola-final-product.jpg" alt="Yummy home-made (gluten-free) granola" position="center" style="border-radius: 50px;" >}}

## Ingredients AKA **What I used**
(apologies in advance I did not measure out this recipe with the scale):
- 4 cup gluten-free oats  (note that these should be "old-fashioned" oats)
- ¼ cup chopped almonds
- ¼ cup dark brown sugar
- ¼ cup peanut oil
- ¼ cup maple syrup
- raisins and dried cranberries. Oh, and more almonds.

## Method
1. Preheat the oven 300F/150C.
1. Mix together the almonds, oats, and brown sugar.
1. Warm the oil and syrup, then pour it slowly over the oats mixture while stirring.
1. Stir together with a spoon, then make it uniform by using your hands.
1. Spread the mixture evenly on a baking tray (or trays).
1. Bake for 40 minutes, stirring the mixture on the tray every 10 minutes.
1. Let the granola cool on the baking trays.
1. Add nuts, raisins, or other dried fruit as desired.


## Future variations

- Experiment with coconut, olive, and other oils.

- For the liquid sugar, try agave, honey, molasses, or treacle.

- Additions may include any variety of nuts, dried fruits, and other morsels: our favourites include cashews, dark chocolate chunks, macadamia nuts, and sunflower seeds.
