---
author: "yearluk"
origin: "scratch"
title: Making ginger beer-- the elusive "plant"
slug: "making ginger beer at home slugs vs the GB plant"
date:   2021-06-01T09:45:47+01:00
lastMod: 2021-09-06T09:45:47+01:00
draft: false
noLicense: false
weight: 1001
summary: "Ginger beer was once a common thing in UK homes, and everyone's mum had a plant. Mine did. Where are plants these days?"
description: "Ginger beer was once a common thing in UK homes, and everyone's mum had a plant. Mine did. Where are plants these days?"
categories:
- Recipes
tags:
- food
- recipex
- fermentation
- beverage
---



> The ginger beer plant was, indeed, very common in Britain probably up until the 1970’s. To this day everyone in the UK still remembers an uncle, a grandmother, a friend or a neighbour brewing their own ginger beer. No children’s picnic was complete without lashings of the stuff. Most homes had a stone bottle of ginger beer in their pantry or cellar. A whole swathe of English legal principle rests upon what went on inside a ginger beer bottle. There is no doubt that this delightful, effervescent, fizzing drink plays a staring role in the heritage of British food and drink
>
> ~~ https://masterinthekitchen.com/2015/04/06/yahoo-and-the-peculiar-tale-of-the-missing-ginger-beer-plant/



## Sources for the plant, US and UK:
- https://www.yemoos.com/collections/cultures/products/genuine-live-ginger-beer-plant
- https://gingerbeerplant.net/recipes.html#ginger-beer

## Recetta 1

1. about a tablespoon of ginger beer plant (the exact amount isn't vital)
2. 200 - 300 g of sugar
3. 2 litres of water
4. 1/2 teaspoon of cream of tartar
5. the juice of a fresh lemon.
6. 2-5 inches of fresh ginger (a teaspoon of dried ginger) 

## Recetta 2
I put 350 - 400 gr sugar in plastic dough raising bowl, add 2 liters tap water, stir with a woden spoon til the sugar is dissolved, grate lemon zest in a piece cloth, cut the lemon in 2, squeeze the juice into the bowl, drop the lemon into the bowl, grate an inch or 2.....or 3 of ginger in the cloth with lemon zest, tie it up and drop it into the bowl, then pour in the gbp and snap on the plastic lid. It pops off at a certain pressure with a nice loud 💥 pop 💥.

