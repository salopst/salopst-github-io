---
author: salopst
origin: posterous
comments: true
date: 2011-06-04T13:08:03+00:00
lastMod: 2021-08-31T06:23:00+01:00
layout: post
slug: now-to-the-ever-elusive-1500
title: Now to the ever elusive 1500
wordpress_id: 94
categories:
- Networking
- Tech
- Travel
tags:
- IPV6
- networking
- England
- selfie
---


{{< image src="/wp-uploads/ipv6-sage-cert.png" alt="IPv6 cert from Hurricane Electric" position="center"style="border-radius: 50px;" >}}


I hope the coveted T-shirt arrives before the celebration on board the USS Hornet.

**Update:**

It did not, but it did just in time for my England trip with the Samster...

{{< image src="/wp-uploads/ipv6-sage-shirt-england.jpg" alt="Yes that's ginger ale. Honest!" position="center" width="800" style="border-radius: 50px;" >}}
Yes that's ginger ale. Honest!
