---
author: admin
origin: wordpress
comments: true
date: 2013-04-25T09:13:19+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
slug: u-penn-greek-myth-notes-week-1
title: 'U. Penn. Greek Myth: Notes (Week 1)'
wordpress_id: 290
categories:
- Uncategorized
tags:
- Greek
- course
---

## 1.1/1.2 Myth (Μυτηος):

  * A falsehood
  * "bucket" containing all that is important to a given culture-- may be universally true; it certainly is specific to a given cultural identity
  * Something fundamental (or primal) about our condition. Also primal as in "old", "primitive"
  * μυτηος-- literally speech; later a narrative speech/story; still later, specifically a false story; later still-- a tall tale, but with some elemental truth
  * Constantly re-invented for the ages/different cultures; always a re-telling. E.g Hercules from Heracles.

Mediterranean-- "the corrupting sea" ([book of same name](http://www.amazon.com/Corrupting-Sea-Study-Mediterranean-History/dp/0631218904)). There is no cultural/linguistic purity. The Med. is a great interconnecting force leading to the mingling of cultures and ideas, of stories and languages and ways of interpreting the world.


### Oedipus-- metaphor for man's quest for knowledge, ever asking.


## 1.3 Some ancients on myth

**_Plato (Πλάτων ca. 428 - 348 BCE)_**: Generally critical of the poets, largely considered myths to be pretty indecent, having a powerfully deleterious effect on culture and cultural values. **Myth constructs culture**.

**_Xenophanes of Colophon (Ξενοφάνης ὀ Κολοφώνιος ca. 570 - 475 BCE)_**: Almost the reverse of Plato in that it is our culture, environment and experience that shapes our myths. He notes that egyptian gods are black, and that if cattle could speak and reason, so their myths would be cattle-like. **Culture reflects/constructs myth**.

**_Metrodorus of Lampascus (Μητρόδορθς Λαμπψακησος 5th BCE)_**: Myths are symbolic (allegoric) representations of deep truths and understandings of the world. As such they might be considered "pre-scientific". E.g. Achilles is the Sun, Helen the Earth, Demeter the liver, Dionysus the spleen. **Allegory**.

**_Aristarchus of Samothrace (Ἀρίσταρχος ca. 220 - 143 BCE)_**: Established historically important critical editions of Homer… hence the modern lit. crit. term "aristarch" for a judgmental critic. Believed myths to be literally stories for stories' sake, and that they had no hidden meaning. **Literary/ anti-allegorical**.

**_Euhemerus (Εὐήμερος 4th BCE)_**: Myth is based on real events, places and peoples that over time have taken on unreal or exagerated tones. Like, perhaps, the Bible. In the sense that the gods are deified mortals/heros one might consider beatification/canonisation a similar process... **Euhemerism**.

Of these beliefs, euhemerism and allegorical interpretation held the majority sway in the pre-modern, post-antique era. During the medieval period the graeco-roman myths and legends were generally just considered _fabulae_, tall tales with perhaps a nuggets of wisdom therein.


## 1.4 Some moderns on myth

**_Bernard de Fontanelle (1657 - 1757)_**: Myths grew  to explain the natural world, helping to explain the unexplicable. In this sense he considered myth to be **proto-science**.

**_David Hume (1711 - 1776)_**: Slap-bang in the middle of the high Enlightenment he was impatient with myth, and discounted myth as primitive and superstitious thought, the result of comforts and erroneous and irrational assumptions now superseded by logic and rationality.** Myth is an Irrational response to fear of the unknown**.

**_Christian Gottlob Heyne (1729 - 1812)_**: A classicist in the, err, classical sense. Was generally interested in the ancient world, and one can't understand the ancients without trying to understand every part of them, especially the 'background', that myths provide. Myth is not so much driven by fear of the unknown as with Hume, but with awe and wonder of nature. Resurrected μυτηος for use instead of the much more limited _fabula_. **Myth is the concretization of the abstract**.

**_Johann Gottfried Herder (1744 - 1803)_**: Somewhat anti-elightenment, and a precursor ro the Romantic spin on graeco-roman myths and legends; viz. myth undergird profound truths about the human spirit,  express an innate characteristic of humans identical to poetry, music, art, and religion. **Romantic**.

**_Walter Burkert (1931 - )_**: scholar of Greek myth, U. Zurich. Myth is a traditional tale told with secondary partial reference to something of collective importance told by someone for some reason.  **Definition used in this course.**

## **1.5 / 1.7 Trojan War / Reading Homer**

  * Trojan War -- 13th BCE
  * Homer -- 8th BCE
  * Athens -- 5th BCE
  * (Classical) Rome -- 1st BCE/CE

Heinrich Schliemann (1822 - 1890) unearthed in the 1870s the remnants of a city in Asia Minor (Turkey) consistent with the age and location of Troy; also "Priam's Treasure" in 1976, a stash of gold and ornaments and such.

Trojan War takes place over ten years, the Iliad compresses these to about three days. Helen's face said to launch 100 ships, therefore approx. 100, 000 Greek warriors. First word is Μῆνις (wrath), and in the Epic tradition the first word sets the tone for the rest of the poem. The wrath in question might rightly be Menelaus' (from whom Paris abducted Helen, Menelaus' wife), Agamemnon's (who as Menelaus' brother also feels his shame), or more likely Achilles'. Achilles' wrath is all encompassing, although he does bend a little toward Priam, assenting to allow him to perform the customary funereal rites on his son, Hector.

"Prequel" for the Iliad is Helen's abduction. Paris feels free to do this because of a bargain with Aphrodite. At the wedding of [Peleus](http://en.wikipedia.org/wiki/Peleus) and [Thetis](http://en.wikipedia.org/wiki/Thetis) (a sea nymph, nereid) the pantheon is invited except Eris (goddess of discord, lit. "strife"). She tosses an apple "for the fairest" (τῇ καλλίστῃ) amongst the guests. Aphrodite, Athena and Hera ask Paris to choose, and he in return gets something. He chooses Aphrodite who promises the most beautiful mortal woman to him. … Athena is somewhat maltreated later by the Greeks-- he palladium is stolen from her temple at Troy (thus removing Athena's aegis of protection over that city), and Ajax rapes princess Cassandra on her altar.

By and large Homer does not gloss over Greeks poor behavior in the Iliad; his recollection of event is not particularly complimentary, with the internal squabbles and the various profanities to Athena and Neptune. Homer has no problem relating that the Greeks went too far, even by their own standards.

The Nostoi (from νοσςος, home): _journeys' home_. Nestor gets an easy ride home. Menelaus gets blown of course to Egypt. Agamemnon faces horros on his return. Odysseus faces trials and tribulations on his way back, recounted in the Odyssey… first word "man", Ανδρα, thus indicating that the Odyssey is about what it is to me a man.

Homer writes in dactylic hexameter, some 15k lines in the Iliad, 12K in the Odyssey. Dactyl is long-short-short, and a spondee (long-long) may replace a dactyl. He write in several dialects, principally Ionic and Aeolic. He was probably writing very shortly after the arrival of the alphabet from the Phoenicians that supplanted the (Cretan) Linear A and Linear B, pictographic systems not used for literary works, but rather for recording events and tracking accounts and such.

### First ten lines of Odyssey and Illiad encapsulate each poem.

#### Odyssey
##### [https://www.perseus.tufts.edu/hopper/text?doc=Perseus:text:1999.01.0135](https://www.perseus.tufts.edu/hopper/text?doc=Perseus:text:1999.01.0135)

```poetry
ἄνδρα μοι ἔννεπε, μοῦσα, πολύτροπον, ὃς μάλα πολλὰ
πλάγχθη, ἐπεὶ Τροίης ἱερὸν πτολίεθρον ἔπερσεν:
πολλῶν δ᾽ ἀνθρώπων ἴδεν ἄστεα καὶ νόον ἔγνω,
πολλὰ δ᾽ ὅ γ᾽ ἐν πόντῳ πάθεν ἄλγεα ὃν κατὰ θυμόν,
5ἀρνύμενος ἥν τε ψυχὴν καὶ νόστον ἑταίρων.
ἀλλ᾽ οὐδ᾽ ὣς ἑτάρους ἐρρύσατο, ἱέμενός περ:
αὐτῶν γὰρ σφετέρῃσιν ἀτασθαλίῃσιν ὄλοντο,
νήπιοι, οἳ κατὰ βοῦς Ὑπερίονος Ἠελίοιο
ἤσθιον: αὐτὰρ ὁ τοῖσιν ἀφείλετο νόστιμον ἦμαρ.
10τῶν ἁμόθεν γε, θεά, θύγατερ Διός, εἰπὲ καὶ ἡμῖν.
```


#### Illiad
##### [https://www.perseus.tufts.edu/hopper/text?doc=Perseus%3atext%3a1999.01.0133](https://www.perseus.tufts.edu/hopper/text?doc=Perseus%3atext%3a1999.01.0133)

```poetry
μῆνιν ἄειδε θεὰ Πηληϊάδεω Ἀχιλῆος
οὐλομένην, ἣ μυρί᾽ Ἀχαιοῖς ἄλγε᾽ ἔθηκε,
πολλὰς δ᾽ ἰφθίμους ψυχὰς Ἄϊδι προΐαψεν
ἡρώων, αὐτοὺς δὲ ἑλώρια τεῦχε κύνεσσιν
5οἰωνοῖσί τε πᾶσι, Διὸς δ᾽ ἐτελείετο βουλή,
ἐξ οὗ δὴ τὰ πρῶτα διαστήτην ἐρίσαντε
Ἀτρεΐδης τε ἄναξ ἀνδρῶν καὶ δῖος Ἀχιλλεύς.
τίς τ᾽ ἄρ σφωε θεῶν ἔριδι ξυνέηκε μάχεσθαι;
Λητοῦς καὶ Διὸς υἱός: ὃ γὰρ βασιλῆϊ χολωθεὶς
10νοῦσον ἀνὰ στρατὸν ὄρσε κακήν, ὀλέκοντο δὲ λαοί,
οὕνεκα τὸν Χρύσην ἠτίμασεν ἀρητῆρα
Ἀτρεΐδης: ὃ γὰρ ἦλθε θοὰς ἐπὶ νῆας Ἀχαιῶν
λυσόμενός τε θύγατρα φέρων τ᾽ ἀπερείσι᾽ ἄποινα,
στέμματ᾽ ἔχων ἐν χερσὶν ἑκηβόλου Ἀπόλλωνος
15χρυσέῳ ἀνὰ σκήπτρῳ, καὶ λίσσετο πάντας Ἀχαιούς,
Ἀτρεΐδα δὲ μάλιστα δύω, κοσμήτορε λαῶν:
Ἀτρεΐδαι τε καὶ ἄλλοι ἐϋκνήμιδες Ἀχαιοί,
ὑμῖν μὲν θεοὶ δοῖεν Ὀλύμπια δώματ᾽ ἔχοντες
ἐκπέρσαι Πριάμοιο πόλιν, εὖ δ᾽ οἴκαδ᾽ ἱκέσθαι:
20παῖδα δ᾽ ἐμοὶ λύσαιτε φίλην, τὰ δ᾽ ἄποινα δέχεσθαι,
ἁζόμενοι Διὸς υἱὸν ἑκηβόλον Ἀπόλλωνα.
```
