---
author: salopst
origin: posterous
comments: true
date: 2011-05-10T12:16:04+00:00
lastMod: 2021-08-31T06:23:00+01:00
layout: post
slug: administrative-distance
title: Administrative Distance
wordpress_id: 1355
categories:
- networking
- tech
tags:
- cisco
- network
- routing
---

|||
|--- |--- |
|Connected interface|0|
|Static route|1|
|Enhanced Interior Gateway Routing Protocol (EIGRP) summary route|5|
|External Border Gateway Protocol (BGP)|20|
|Internal EIGRP|90|
|IGRP|100|
|OSPF|110|
|Intermediate System-to-Intermediate System (IS-IS)|115|
|Routing Information Protocol (RIP)|120|
|Exterior Gateway Protocol (EGP)|140|
|On Demand Routing (ODR)|160|
|External EIGRP|170|
|Internal BGP|200|
|Unknown*|255|
