---
author: admin
origin: wordpress
comments: true
date: 2013-07-08T18:46:50+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: oopsies-ii
title: Oopsies II
wordpress_id: 310
categories:
- Uncategorized
tags:
- anatomy
- medical
- wrist
---

They took out 25 stitches today and replaced them with the butterfly stitches to reduce superficial scaring. Nice of them, but skin scars do no really matter much to me.)

{{< image src="/wp-uploads/wpid-Gray4211.png" alt="Cross-section of the antebrachium" position="center" style="border-radius: 50px;" >}}

## Anyway the big stuff:

- Flexor Digitalis Superficialis @ 2, 3 and 4 was completely severed as was the Flexor Carpi Radialis and the Flexor Digitalis Profundus of 3.

- FDP-2 and FDP-4 cut but not severed

- FDS-5 was damaged. 

- A portion of FDS-3 was transferred and sutured to FDP-3.

All internal suturing was done with [FiberWire](http://www.arthrex.com/fiberwire) using modified (Tajima) [Kessler grasping knots](http://www.wheelessonline.com/ortho/flexor_tendon_repair_techniques_core_suture_techniques).

No damage to nerves.... that's good news! Damned close call on the radial, however.

Back on July 23 to see the surgeon, and in the meantime I have hand therapy scheduled for this July 11 and the following Monday (July 15).

Fingers and wrist are immobilized in a splint-- and will be for another six weeks, except for scheduled exercises.

Still fucking painful; still feels like molten lead is flowing through my forearm!
