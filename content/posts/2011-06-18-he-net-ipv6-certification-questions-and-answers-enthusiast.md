---
author: salopst
origin: posterous
comments: true
date: 2011-06-18T18:29:02+00:00
lastMod: 2021-08-31T06:23:00+01:00
layout: post
slug: he-net-ipv6-certification-questions-and-answers-enthusiast
title: HE.net IPV6 Certification questions and answers -- \#1 Enthusiast
wordpress_id: 146
categories:
- networking
- tech
tags:
- ipv6
- networking
- he.net
---


## [http://ipv6.he.net/certification/](http://ipv6.he.net/certification/)


  * [0. Newbie Test](/he-net-ipv6-certification-questions-and-answers-newbie/)

	
  * [2. Administrator Test](/he-net-ipv6-certification-questions-and-answers-administrator/)

	
  * [3. Professional Test](/he-net-ipv6-certification-questions-and-answers-professional/)

	
  * [4. Guru Test](/he-net-ipv6-certification-questions-and-answers-guru/)

	
  * [5. Sage Test](/he-net-ipv6-certification-questions-and-answers-sage/)


## Covers technical knowledge of ping and traceroute commands on Linux and Windows.

#### 1. What command do you use to ping an IPv6 address on Free Open Source UNIX platforms such as Linux, FreeBSD, etc?
- _ ping -A inet6
- ping --ipv6
- ping6   ✅
- pingsix_

#### 2. What command do you use to traceroute to an IPv6 address on Free Open Source UNIX platforms such as Linux, FreeBSD, etc?
- traceroute -A inet6
- traceroute --inet6
- traceroute6   ✅
- traceroutesix

#### 3. What command do you use to ping an IPv6 address on a Microsoft Windows platform?
- pingsix
- ping --inet6
- ping   ✅
- ping --ipv6

#### 4. What command do you use to traceroute to an IPv6 address on a Microsoft Windows platform?
- traceroute
- traceroute6
- tracert --ipv6
- tracert   ✅

#### 5. IPv6 addresses are written using what number base?
- hexadecimal (base 16)   ✅
- octal (base 8)
- binary (base 2)
- decimal (base 10)

#### 6. Hexadecimal digits are represented by:
- 0 to 9
- 0 to 9 and A to F   ✅
- 0 to 7
- 0 and 1
