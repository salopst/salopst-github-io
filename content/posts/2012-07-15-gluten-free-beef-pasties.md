---
author: admin
origin: wordpress
comments: true
date: 2012-07-15T18:10:53+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: gluten-free-beef-pasties
title: Gluten-free beef pasties
wordpress_id: 391
categories:
- Gluten free
- Recipes
- Food
tags:
- gluten-free
- recipe
- pastry
---

Gluten-free beef pasties. (This is one where you’ll have to use your own judgment regarding amounts: we just threw it together, but the addition of lamb stock made the filling especially rich. The point of this post? You can be gluten-free and still feed that craving for a pasty.)

### Beef fresh from the village butcher
{{< image src="/wp-uploads/GF-pasties-cubed-beef.jpg" alt="Beef fresh from the village butcher" position="center" style="border-radius: 50px;" >}}


## Filling (ingredients)

- Fresh string beef from the local butcher
- Gluten-free flour
- Olive oil or animal fat
- Red wine
- Lamb stock (made with bones from yesterday’s lamb chops)
- Peas and parboiled carrots and potatoes (we used small new potatoes)

### We tossed the beef in gluten-free flour
{{< image src="/wp-uploads/GF-pasties-beef-tossed.jpg" alt="We tossed the beef in gluten-free flour" position="center" style="border-radius: 50px;" >}}


## Filling (method)

- Dice the meet into small- and medium-sized cubes

- Toss the beef in gluten-free flour

- Brown the beef in oil/fat

- Remove the beef from the pan and deglaze with red wine

- Restore beef to the pan

- Add lamb stock, carrots, peas, and potatoes

- Cook until the mixture is nice and thick


## Preparing the pasties:

- Roll out pastry (look at other entries for gluten-free pastry)

- Cut the dough into large ovals (roughly double the size you want your pasty to be)

- Spoon a heap of filling onto one half of each cut-out portion of dough, being careful to leave room at the edges

- Fold the remaining dough over the filling so that you can crimp the sides together to create a filled packet of pastry

- Brush the pastry with milk if desired

- Bake at about 400 degrees Fahrenheit until the pastry is golden

### Nice, meaty filling
{{< image src="/wp-uploads/YYYYY" alt="Nice, meaty filling" position="center" style="border-radius: 50px;" >}}


Note: gluten-free pastry does not hold together as well as regular pastry, so you will probably want to eat these on a plate with a knife and fork, for they are unlikely to stay together if you try to eat them with your hands.

### The pasties might not look perfect, but they tasted delicious
{{< image src="/wp-uploads/GF-pasties-cooked.jpg" alt="The pasties might not look perfect, but they tasted delicious" position="center" style="border-radius: 50px;" >}}
