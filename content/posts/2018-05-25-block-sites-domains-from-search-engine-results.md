---
author: admin
origin: wordpress
comments: true
draft: false
date: 2018-05-25T06:44:17+00:00
layout: post
slug: block-sites-domains-from-search-engine-results
title: Block sites/domains from search engine results
wordpress_id: 1382
categories:
- Tech
tags:
- internet
- search engines
---

Happy GDPR Day!

With the slew of new cookie warnings and privacy notifications and such, it's time to do something with those annoying domains from content farms that so often pepper search engine results.

#### Step 1: install [Tampermonkey](https://tampermonkey.net) extension

#### Step 2: set to writing a bunch of JS. But wait, surely someone else has already done this... they have:
[https://greasyfork.org/en/scripts/1682-google-hit-hider-by-domain-search-filter-block-sites](https://greasyfork.org/en/scripts/1682-google-hit-hider-by-domain-search-filter-block-sites) So a big, BIG thank you to Jefferson Scher.

**EDIT**: Another script to restore "old" youtube. The non-sucky version (the one with 'up next' right below the video being played)... [https://cable.ayra.ch/tampermonkey/data/youtube_old_design.user.js](https://cable.ayra.ch/tampermonkey/data/youtube_old_design.user.js)
