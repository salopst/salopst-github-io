---
author: admin
origin: wordpress
comments: true
date: 2015-05-10T14:41:32+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: fontawesome-4-3-0-yearlusbitbucket
title: fontawesome 4.3.0 (yearlus@bitbucket)
wordpress_id: 912
categories:
- Tech
tags:
- latex
- fonts
- fontawesome
---

### EDIT:: 2015-07-11:: please see ==> [https://github.com/xdanaux/fontawesome-latex](https://github.com/xdanaux/fontawesome-latex)


## **** THIS IS BY NO MEANS TO BE CONSIDERED CANONICAL ****
## **** USE AT YOUR OWN DISCRETION ****

[Listing by hex value: bitbucket PDF](https://bytebucket.org/yearlus/fontawesome/raw/a7338a6dde9e3caeae5f0ddd3439d230c4675b3e/fontawesome.pdf")

Source: [https://bitbucket.org/yearlus/fontawesome/src/master/](https://bitbucket.org/yearlus/fontawesome/src/master/)
