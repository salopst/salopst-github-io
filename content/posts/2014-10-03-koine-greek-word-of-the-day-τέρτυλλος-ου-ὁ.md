---
author: kword
comments: true
date: 2014-10-03T11:07:16+00:00
layout: post
slug: koine-greek-word-of-the-day-τέρτυλλος-ου-ὁ
title: 'Koinē Greek Word of the Day: Τέρτυλλος, ου, ὁ'
wordpress_id: 756
categories:
- Language
tags:
- Greek
- Bible
- words
- koine
- κοινή
---

# Τέρτυλλος, ου, ὁ

Tertullos
Noun, Masculine
*Tertullus, a barrister acting as professional prosecutor of Paul at Caesarea.*
Strong's: **5061**

2 occurrences in GNT

```text
Τέρτυλλος (N-NMS), Acts 24:2
Τερτύλλου (N-GMS), Acts 24:1
```

# Acts 24:2

```text
κληθέντος δὲ αὐτοῦ ἤρξατο κατηγορεῖν ὁ Τέρτυλλος λέγων· Πολλῆς εἰρήνης τυγχάνοντες διὰ σοῦ καὶ διορθωμάτων γινομένων τῷ ἔθνει τούτῳ διὰ τῆς σῆς προνοίας
```
~~SBL Greek New Testament (SBLGNT)


```text
et citato Paulo coepit accusare Tertullus dicens cum in multa pace agamus per te et multa corrigantur per tuam providentiam
```
~~Biblia Sacra Vulgata (VULGATE)


```text
And when he was called forth, Tertullus began to accuse him, saying, Seeing that by thee we enjoy great quietness, and that very worthy deeds are done unto this nation by thy providence,
```
~~King James Version (KJV)

* * *
kwords 0.3
