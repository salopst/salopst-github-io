---
author: admin
origin: wordpress
comments: true
date: 2012-03-05T14:26:14+00:00
layout: post
slug: pipes-not-chained-data-streams
title: Pipes (not chained data streams)
wordpress_id: 194
categories:
  - Uncategorized
tags:
  - pipes
  - tobacco
---

So. I inherited a few [Peterson](http://www.peterson.ie/pipes/) tobacco pipes from my father who has been gone a couple of years now. He had what one might call a modest collection, and all were smoked regularly. Nothing fancy 'just for show' except one piece. Anyway, I decided that I would do my level best to clean a few of them. I did this just because I was bored... but the more I read, the more interested I became. To the point at which I have listed one on Ebay (pictured below), and have several of the remainer on what pipe smokers call a rotation. I've been enjoying a bowl or two every night recently.

There's something a bit geeky about pipe smoking, so many avenues to venture down. It is anything but casual, and surely the pipe smoker is the most encumbered of smokers with reamers and tampers and pouches, let alone the pipe itself. The arcanery of tobaccos, the lore, the literary allusions; the peace, the relaxation. It requires substantially more effort to enjoy a bowl of tobacco than to grab a quick ciggie at a rest stop on the highway. I like that.


{{< image src="/wp-uploads/2012-03-05-no-name-smoking-pipe.png" alt="No name smoking pipe" position="center" style="border-radius: 50px;" >}}

-----
-----
-----

{{< image src="/wp-uploads/2012-03-05-no-name-smoking-pipe-in-case.png" alt="Brown trout are the best trout" position="center" style="border-radius: 50px;" >}}


Yes, yes. "Smoking kills", and I understand it is not a flowery death either. But that should be an individual's choice. Aftter enjoying a bowl of [Squadron Leader](http://tobaccoreviews.com/blend_detail.cfm?ALPHA=S&TID=1040) from some vintage briar wood that used to belong to your father, and that you have loving restored back to smoking condition you might think somewhat more liberally.
