---
author: kword
comments: false
date: 2014-09-28T18:50:22+00:00
layout: post
slug: koine-greek-word-of-the-day-koine-greek-word-of-the-day-ἑξακόσιοι-αι-α
title: 'Koinē Greek Word of the Day: ἑξακόσιοι, αι, α'
wordpress_id: 715
categories:
- Language
tags:
- Greek
- Bible
- words
- koine
- κοινή
---

# ἑξακόσιοι, αι, α
asd
hexakosioi
Adjective
*six hundred.*
Strong's: **1812**
2 Occurrences in GNT:

```text
ἑξακοσίων (Adj-GMP), Revelation 14:20
ἑξακόσιοι (Adj-NMP), Revelation 13:18
```

# revelation 13:18

```text
ὧδε ἡ σοφία ἐστίν· ὁ ἔχων νοῦν ψηφισάτω τὸν ἀριθμὸν τοῦ θηρίου, ἀριθμὸς γὰρ ἀνθρώπου ἐστίν· καὶ ὁ ἀριθμὸς αὐτοῦ ἑξακόσιοι ἑξήκοντα ἕξ.
```
~~Society of Biblical Languages Greek New Testament

```text
hic sapientia est qui habet intellectum conputet numerum bestiae numerus enim hominis est et numerus eius est sescenti sexaginta sex
```
~~Jerome's Vulgate

```text
Here is wisdom. Let him that hath understanding count the number of the beast: for it is the number of a man; and his number is Six hundred threescore and six.
```
~~King James Version
