---
author: admin
origin: wordpress
comments: true
date: 2013-07-23T23:54:45+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
slug: oopsies-iv
title: Oopsies IV
wordpress_id: 549
draft: false
categories:
- Uncategorized
tags:
- anatomy
- wrist
- medical
---


{{< image src="/wp-uploads/suck-meter.jpg" alt="suck-o-meter" position="center" style="border-radius: 30px;" >}}

Another consultation with surgeon again today-- he's not happy with the scarring; not happy at all. All kinds of adhesions going on in that Zone V of mine. The plan now is for another month of increasing exercises, vibration and ultrasound therapy, then, maybe, cortisone injections. At that point a determination will be made about his potentially opening me up again for remedial tenolytic work.

"It has been documented the up to 20% of patients will develop adhesions which requires tenolysis or tendon grafts [2]"


> In most cases, full and normal movement of the injured area does not return after surgery. If it is hard to bend the finger using its own muscle power, it could mean that the repaired tendon has pulled apart or is bogged down in scar tissue. Scarring of the tendon repair is a normal part of the healing process. But in some cases, the scarring can make bending and straightening of the finger very difficult. Depending on the injury, your doctor may prescribe therapy to loosen up the scar tissue and prevent it from interfering with the finger’s movement. If therapy fails to improve motion, surgery to release scar tissue around the tendon may be required.
~ [http://www.assh.org/Public/HandConditions/Documents/Web_Version_PDF/flexor_injuries.pdf](http://www.assh.org/Public/HandConditions/Documents/Web_Version_PDF/flexor_injuries.pdf)</blockquote>


**Ugh.** We shall never know the concert pianist I might have become.

> [scar mobilization](http://www.handtherapyresources.com/content/scar-mobilization)
Adherent scars can significantly limit AROM and can be difficult to mobilize. It is important to mobilize this adherent scar tissue from the underlying structures. A technique I have found useful is to use [dycem](http://www.pattersonmedical.com/app.aspx?cmd=getProduct&key=IF_921017326) to increase the hold the therapist has of the scar tissue. The patient can move the involved joint while pushing the scar in the opposite direction to free the scar from the underlying tissue. For example, have the patient make a fist while the therapist uses dycem placed over the scarred tissue and pushes the scar proximally. This can be aggressive scar management so the therapist must be careful to respect the patients response and monitor their levels of discomfort.
>
>~~ http://www.handtherapyresources.com/content/scar-mobilization</blockquote>


- [1] [The Cellular Biology of Flexor Tendon Adhesion Formation](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2774058/), Am J Pathol. 2009 November; 175(5): 1938–1951

- [2] [An Overview of the Management of Flexor Tendon Injuries](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3293389/), Open Orthop J. 2012; 6: 28–35.

- [3] [Outcome of early active mobilization after flexor tendons repair in zones II–V in hand](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC2911933/?report=printable), Indian J Orthop. 2010 Jul-Sep; 44(3): 314–321.

- 4] [The resurgence of barbed suture and connecting devices for use in **flexor** tendon tenorrhaphy](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3153618/)

- [5] [Early Active Motion after Flexor Tendon Repair](http://www.msdlatinamerica.com/ebooks/HandSurgery/sid544732.html) (cap. 39 _Hand Surgery_, 2004)
