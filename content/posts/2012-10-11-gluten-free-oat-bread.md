---
author: admin
origin: wordpress
comments: true
date: 2012-10-11T15:51:03+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: gluten-free-oat-bread
title: Gluten-Free Oat Bread
wordpress_id: 396
categories:
- Gluten free
- Recipes
tags:
- gluten-free
- oat bread
- recipe
- food
---

Lest y'all think that everything works out in our GF kitchen, I've decided to add that the following did not. Well, not quite. The bread is darned tasty, and is wonderful toasted with a bit of fresh butter. We are BIG butter fans, especially locally made stuff (not Cabots or Land o' Lakes, or anything 'stick' form, really), the sort of butter that is pretty close to tasting of cheese.

{{< image src="/wp-uploads/GF-oat-bread.jpg" alt="" position="center" style="border-radius: 50px;" >}}

I think it is really just a question of getting the right flour combination down, and practising technique. Bread making is hard enough to get right with regular wheat flour, let alone with the added complexity of a bread sans gluten.

As it was [World Porridge Day](http://www.goldenspurtle.com/world-porridge-day/) we went with the following:

## Ingredients
- 80g GF porridge oats
- 150g rice flour
- 50g tapioca flour
- 60g buckwheat flour
- 1tsp GF baking powder
- 1tsp xanthan gum
- 1 tsp honey
- 1 packet dried bakers' yeast
- 50g butter
- 30g sesame seeds
- 1/2 tsp salt

I should repeat again that we use a nifty [electronic scale](http://www.polyvore.com/bamboo_kitchen_scale/thing?id=37829006), which is just so convenient for baking. Place on a bowl, zero (or 'tare') it, add ingedient, zero once more... add ingredient. Rinse and repeat.

## Method
1. Set the yeast in a warm sugar solution and cover
1. 'Biltz' the oats in a food processor to break them up
1. Add other dry ingredients, and give the processor a few spins to mix the ingredients
1. Add honey and butter and rub in until one has what looks and feels like fine breadcrumbs
1. When the yeast starter is starting to bubble, add it and 400ml of warm water to the dry ingredients and beat to form a batter. Cover and set aside to let the yeast get all saccharomycidal on those complex sugars, and the batter to rise
1. Preheat oven to 200C
1. Grease a 450g loaf tin, pour in batter
1. Bake until golden brown and cooked through (45-50 minutes)

A few thoughts for next time... Higher oven temperature at least initially. Buckwheat is, we think, a little bit heavy. Perhaps 50g less rice flour, and replace that deficit AND the tapioca for 100g of potato flour. Maybe also leave out the sesame seeds (add a little sesame oil instead), and give longer for the yeast to really get going.
