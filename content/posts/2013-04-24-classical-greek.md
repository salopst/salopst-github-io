---
author: admin
origin: wordpress
comments: true
date: 2013-04-24T21:13:27+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: classical-greek
title: Classical Greek
wordpress_id: 262
Categories:
- Uncategorized
tags:
- course
- Greek
- language
---

So, I've signed up for "Greek and Latin Mythology" from U. Penn at [coursera.org](http://www.coursera.org). It should be an interesting diversion in these times of my doing relatively little. A little Homer, Aristarchus, Thucydides, Aristophanes, Vergil, Ovid and the like would be welcome relief. Read all-- or almost all-- of these in English translation as a late teenager, and some of the Latin authors in the original. The Greek however… well, I never got very far with that in High School. Not saying I will this time, but there's a niggling thing at the back of my mind saying that any time learning more of the aorist and the optative etc. would not entirely be wasted.

I may get around to posting my "lecture" notes, but in the meantime some links.

## Software:

- [Kalós](http://www.kalos-software.com/index.php) -- a dictionary and morphological analysis tool. Java. Free as in beer.
- [Unicorn](http://www.kalos-software.com/index.php) -- a text editor with built in Latin & Greek dictionaries. Java. Free as in beer.
- [Unicode Greek fonts](http://www.russellcottrell.com/greek/fonts.asp) -- note **Ariscarcoj**
- [TITUS Cyberbit](http://titus.fkidg1.uni-frankfurt.de/unicode/tituut.asp) font -- I remember this puppy from setting Devangaari script a half-lifetime ago at Yale.
- [Gentium](http://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&item_id=Gentium&_sc=1) font has support for polytonic Greek



## Websites:

- [http://www.textkit.com/](http://www.textkit.com/) General Greek and Latin resources for learning
- [http://www.utexas.edu/cola/centers/lrc/eieol/grkol-TC-X.html](http://www.utexas.edu/cola/centers/lrc/eieol/grkol-TC-X.html) Classical Greek at U. Texas, Austin  

- [http://www.open.ac.uk/Arts/greek/](http://www.open.ac.uk/Arts/greek/) Classical Greek from the OU.
- [http://socrates.berkeley.edu/~ancgreek/](http://socrates.berkeley.edu/~ancgreek/) UC Berkeley's Ancient Greek.
- [http://www.ancientgreekonline.com/](http://www.ancientgreekonline.com/) Decent poke around.
