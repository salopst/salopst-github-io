---
title: "Changes to Pop!_OS (Ubuntu 21.04) after install"
slug: changes-to-ubuntu-21-04-after-install
author: yearlus
origin: hugo
date: 2021-07-13T09:30:20+01:00
lastMod: 2021-09-19T14:03:23+01:00
draft: false
toc: false
noLicense: false
weight: 1001
images: null
categories:
  - Linux
  - Tech
tags:
  - software
  - apps
  - flatpack
  - pop os
  - ubuntu
  - apt
description: "Semi-runnable half-a-script detailing changes made after Pop OS (Ubuntu 21.04) installation."
---

This is essentially a list, and is not really meant to be run as a script. Perhaps this will change.

{{< gist salopst 89bf87ceecd666901d09841dbd0aef22 >}}