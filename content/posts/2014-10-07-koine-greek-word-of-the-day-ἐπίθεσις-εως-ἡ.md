---
author: kword
comments: true
date: 2014-10-07T05:14:42+00:00
layout: post
link: http://stephen.yearl.us/koine-greek-word-of-the-day-ἐπίθεσις-εως-ἡ
slug: koine-greek-word-of-the-day-ἐπίθεσις-εως-ἡ
title: 'The Last... Koinē Greek Word of the Day: ἐπίθεσις, εως, ἡ'
wordpress_id: 777
categories:
- Language
tags:
- Greek
- Bible
- words
- koine
- κοινή
---

Yeah, so the program runs just fine, but it seems a little silly to clog up the blogosphere (do people still use that term?) with such nonsense, so I'll limit this to this, and just have it mailed to me instead. Seems more polite, really, doesn't it?


# ἐπίθεσις, εως, ἡ

epithesis
Noun, Feminine
*a laying on; an attack, assault.*
Strong's: **1936**

4 occurrences in GNT

```text
ἐπιθέσεώς (N-GFS), Hebrews 6:2
ἐπιθέσεως (N-GFS), 2 Timothy 1:6
ἐπιθέσεως (N-GFS), 1 Timothy 4:14
ἐπιθέσεως (N-GFS), Acts 8:18
```

# Acts 8:18

```text
ἰδὼν δὲ ὁ Σίμων ὅτι διὰ τῆς ἐπιθέσεως τῶν χειρῶν τῶν ἀποστόλων δίδοται τὸ πνεῦμα προσήνεγκεν αὐτοῖς χρήματα
```
~~SBL Greek New Testament (SBLGNT)


```text
cum vidisset autem Simon quia per inpositionem manus apostolorum daretur Spiritus Sanctus obtulit eis pecuniam
```
~~Biblia Sacra Vulgata (VULGATE)


```text
And when Simon saw that through laying on of the apostles' hands the Holy Ghost was given, he offered them money,
```
~~King James Version (KJV)

* * *
kwords 0.3
