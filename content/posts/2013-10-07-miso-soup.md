---
author: admin
origin: wordpress
comments: true
date: 2013-10-07T11:10:54+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
slug: miso-soup
title: Miso Soup
draft: false
wordpress_id: 399
categories:
- Gluten free
- Recipes
tags:
- food
- recipes
---

We have been making miso soup for so long that, like many other cooked (rather than baked) dishes, we no longer follow recipes so much as we follow principles. Miso has been a winner in our house with the boys, and is a 'go to' soup. It is a relatively rare day when there is not a pot simmering on the stove. It works as a warming drink around about breakfast time, a soothing drink about bed time, and it works as a decent starter to any meal. 'Porked up' a little, it can even stand as a main, especially with a side of rice.

I cannot think of a dish that is lower in calories than this, but is equally filling. Like fresh tortillas the ingredients are simple (if you can get them), but there is some technique involved. If you cannot get the ingedients then don't bother; instant miso soup is an abomination, and is without doubt the foulest thing you will ever taste outside of a cucumber.

## Ingredients
- Konbu (kelp; [dulse](http://en.wikipedia.org/wiki/Dulse) if youse is from Ulster)
- Bonito flakes
- Filtered water
- Miso paste **miso paste is naturally gluten free, but some contain non-traditional ingredients, viz. barley (麦、mugi) or wheat (小麦、komugu)**
- Firm tofu

**Optional**
- Nori/ wakame/ seaweed of choice
- Scallions/ spring onions
- Meat of choice-- very low fat pork loin chops we find ideal

## Method
Again we are without accurate measurements, so these are 'ballpark' estimates.

1. Filter ~2L of cold water and set on a medium heat
2. Rdd ~100g of konbu and simmer until liquid is a pale green, and steam is rising from the surface
3. Remove the konbu and add a handful (50g ??) of bonito flakes
4. The bonito will rise then sink then rise again. At this point the broth (dashi) should be seived again, removing the bonito flakes.
5. The dashi can be used, at this point, for any number of things.... numer one here is that it goes towards miso soup, but we often use it instead of plain water to flavor rice
6. Remove some dashi to a separate bowl and slowly add ~50g of miso, whisking, adding back to the dashi
7. Add ca. 250/500g firm tofu that has been cut into 1cm cubes
8. Season with wakame or nori, slices into strips, and scallions
9. Add 1 cm cubes of VERY lean meat... we generally go for loin pork chops with all the fat removed. Not because we are squeamish about animal fat, but just because it detracts from the [umami](http://en.wikipedia.org/wiki/Umami) taste of the soup
