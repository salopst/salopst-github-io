---
author: yearlus
origin: scratch
title: Chuck Norris Jokes
slug: chuck-norris-jokes
date: 2020-03-10T16:30:17+01:00
lastMod: 2021-09-01T02:30:00+01:00
draft: false
noLicense: false
weight: 1001
categories:
- Humour
- Random
tags:
- jokes
---

Reddit tells me that today is Chuck Norris' 800th birthday. Here, some collected "Chuckisms".

### Chuck Norris can kill two stones with one bird.  

### Chuck Norris can cut through a hot knife... with butter.  

### Chuck Norris can slam a revolving door.  

### Chuck Norris once visited the Virgin Islands. They are now The Islands. 

### Chuck Norris doesn't wear a watch. He simply decides what time it is. 

### Death once had a near-Chuck experience.  

### Chuck Norris doesn’t call the wrong number, you picked up the wrong phone.

### If chuck Norris stares at the sun, the sun goes blind  

### Chuck Norris doesn't do a push *up*s, he's pushing the Earth down. 

### When Chuck Norris chops onions, the onions cry.  