---
author: kword
comments: false
date: 2014-09-26T12:34:47+00:00
layout: post
slug: koine-greek-word-of-the-day-koine-greek-word-of-the-day-βιβλίον-ου-τό
title: 'Koinē Greek Word of the Day: βιβλίον, ου, τό'
wordpress_id: 695
categories:
- Language
tags:
- Greek
- Bible
- words
- koine
- κοινή
---

# βιβλίον, ου, τό

biblion
Noun, Neuter
*a papyrus roll.*
Strong's: **975**

34 Occurrences in GNT


# matthew 19:7

```text
λέγουσιν αὐτῷ· Τί οὖν Μωϋσῆς ἐνετείλατο δοῦναι βιβλίον ἀποστασίου καὶ ἀπολῦσαι
```
~~Society of Biblical Languages Greek New Testament


```text
dicunt illi quid ergo Moses mandavit dari libellum repudii et dimittere
```
~~ Jerome's Vulgate


```text
They say unto him, Why did Moses then command to give a writing of divorcement, and to put her away?
```
~~ King James Version

* * *

```text
Nom:  τό βιβλίον________τά βιβλία
Gen:  τοῦ βιβλίου________τῶν βιβλίων
Dat:  τῷ βιβλίῳ________τοῖς βιβλίοις
Acc:  τό βιβλίον________τά βιβλία
```
