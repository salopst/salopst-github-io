---
author: yearluk
origin: scratch
title: "\"Better Days\" by Natalia Gutierrez Y Angelo"
slug: better days Natalia Gutierrez Y Angelo
date: 2019-11-12T13:00:00+00:00
lastMod: 2019-11-12T13:00:00+00:00
draft: false
noLicense: false
weight: 1001
categories:
  - Language
  - Media
tags:
  - spanish
  - politics
  - song
  - refx
  - lyrics
  - music
---


The song "*Better Days*" was broadcast to over 3 million people ca 2010, and eventually reached hostages captured and held by the Fuerzas Armadas Revolucionarias de Colombia ([FARC](https://en.wikipedia.org/wiki/Revolutionary_Armed_Forces_of_Colombia)). In the song was encoded a sercret message in Morse Code. Amazingly, the plan worked, and many soldiers were able to escape the camp and reach Colombian military forces nearby.

- https://soundcloud.com/michael-zelenko/better-days-by-natalia-gutierrez-y-angelo

- https://www.youtube.com/watch?v=7CqOYM7cCX8
It includes a secret Morse code message to the hostages:

**“19 people rescued, you’re next. Don’t lose hope”**


 > .---- ----. / .--. . --- .--. .-.. . / .-. . ... -.-. ..- . -.. --..-- / -.-- --- ..- .----. .-. . / -. . -..- - .-.-.- / -.. --- -. .----. - / .-.. --- --- ... . / .... --- .--. . .-.-.-
 >
 >~~ https://cryptii.com/pipes/morse-code-translator


```lyrics
Cuando estoy en medio de la noche
Pensando en lo que mas quiero
Siento ganas de cantar
Lo que mi corazón tiene para dar

Hablo de los que amo
De cuanto los extraño
Hablo del orgullo y las fuerzas
Que late dentro de mi corazón

Un nuevo amanecer cantando este mensaje desde mi corazón
Aunque este solo atado, yo me siento a tu lado, escucha este mensaje hermano

Quiero seguir luchando
Por mis amigos, mi familia, mis hijos
Pronto volveremos a vernos
Estoy seguro, vienen mejores días

Un nuevo amanecer cantando este mensaje desde mi corazón
Es este canto el que me lleva de la mano, escucha este mensaje hermano (x2)
```
-----
-----
-----

```lyrics
When I'm in the middle of the night
Thinking about what I want most
I feel like singing
What my heart has to give

I speak of those I love
How much I miss them
I speak of pride and strength
That beats inside my heart

A new dawn singing this message from my heart
Even though I'm just tied up, I'm sitting next to you, listen to this message, brother

I want to keep fighting
For my friends, my family, my children
Soon we will see each other again
I'm sure, better days are coming

A new dawn singing this message from my heart
It is this song that takes me by the hand, listen to this message brother (x2)
```
