---
author: kword
comments: false
date: 2014-09-29T12:11:14+00:00
layout: post
slug: koine-greek-word-of-the-day-ἐμπίπτω
title: 'Koinē Greek Word of the Day: ἐμπίπτω'
wordpress_id: 720
categories:
- Language
tags:
- Greek
- Bible
- words
- koine
- κοινή
---

# ἐμπίπτω


empiptó
Verb
*I fall in, am cast in, am involved in.*
Strong's: **1706**

7 Occurrences in GNT


```text
ἐμπεσεῖν (V-ANA), Hebrews 10:31
ἐμπίπτουσιν (V-PIA-3P), 1 Timothy 6:9
ἐμπέσῃ (V-ASA-3S), 1 Timothy 3:7
ἐμπέσῃ (V-ASA-3S), 1 Timothy 3:6
ἐμπεσόντος (V-APA-GMS), Luke 10:36
ἐμπεσοῦνται (V-FIM-3P), Luke 6:39
ἐμπέσῃ (V-ASA-3S), Matthew 12:11
```

# Matthew 12:11

```text
ὁ δὲ εἶπεν αὐτοῖς· Τίς ἔσται ἐξ ὑμῶν ἄνθρωπος ὃς ἕξει πρόβατον ἕν, καὶ ἐὰν ἐμπέσῃ τοῦτο τοῖς σάββασιν εἰς βόθυνον, οὐχὶ κρατήσει αὐτὸ καὶ ἐγερεῖ;
```
~~Society of Biblical Languages Greek New Testament


```text
ipse autem dixit illis quis erit ex vobis homo qui habeat ovem unam et si ceciderit haec sabbatis in foveam nonne tenebit et levabit eam
```
~~Jerome's Vulgate


```text
And he said unto them, What man shall there be among you, that shall have one sheep, and if it fall into a pit on the sabbath day, will he not lay hold on it, and lift it out?
```
~~King James Version
