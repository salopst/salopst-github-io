---
author: kword
comments: false
date: 2014-10-02T10:35:34+00:00
layout: post
slug: koine-greek-word-of-the-day-ῥακά
title: 'Koinē Greek Word of the Day: ῥακά'
wordpress_id: 752
categories:
- Language
tags:
- Greek
- Bible
- words
- koine
- κοινή
---

# ῥακά
rhaka
Aramaic Transliterated Word (Indeclinable)
*empty, foolish.*
Strong's: **4469**

1 occurrences in GNT

```text
Ῥακά (Aram), Matthew 5:22
```


# Matthew 5:22

```text
ἐγὼ δὲ λέγω ὑμῖν ὅτι πᾶς ὁ ὀργιζόμενος τῷ ἀδελφῷ αὐτοῦ ἔνοχος ἔσται τῇ κρίσει· ὃς δ’ ἂν εἴπῃ τῷ ἀδελφῷ αὐτοῦ· Ῥακά, ἔνοχος ἔσται τῷ συνεδρίῳ· ὃς δ’ ἂν εἴπῃ· Μωρέ, ἔνοχος ἔσται εἰς τὴν γέενναν τοῦ πυρός.
```
~~SBL Greek New Testament (SBLGNT)


```text
ego autem dico vobis quia omnis qui irascitur fratri suo reus erit iudicio qui autem dixerit fratri suo racha reus erit concilio qui autem dixerit fatue reus erit gehennae ignis
```
~~Biblia Sacra Vulgata (VULGATE)


```text
But I say unto you, That whosoever is angry with his brother without a cause shall be in danger of the judgment: and whosoever shall say to his brother, Raca, shall be in danger of the council: but whosoever shall say, Thou fool, shall be in danger of hell fire.
```
~~King James Version (KJV)


* * *
kwords 0.3
