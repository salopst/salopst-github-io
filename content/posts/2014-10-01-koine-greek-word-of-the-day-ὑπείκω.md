---
author: kword
comments: false
date: 2014-10-01T06:08:28+00:00
layout: post
slug: koine-greek-word-of-the-day-ὑπείκω
title: 'Koinē Greek Word of the Day: ὑπείκω'
wordpress_id: 729
categories:
- Language
tags:
- Greek
- Bible
- words
- koine
- κοινή
---

# ὑπείκω

hupeikó
Verb
*I yield, submit to authority.*
Strong's: **5226**

1 occurrence in GNT

```text
ὑπείκετε (V-PMA-2P), Hebrews 13:17

```

# Hebrews 13:17

```text
Πείθεσθε τοῖς ἡγουμένοις ὑμῶν καὶ ὑπείκετε, αὐτοὶ γὰρ ἀγρυπνοῦσιν ὑπὲρ τῶν ψυχῶν ὑμῶν ὡς λόγον ἀποδώσοντες, ἵνα μετὰ χαρᾶς τοῦτο ποιῶσιν καὶ μὴ στενάζοντες, ἀλυσιτελὲς γὰρ ὑμῖν τοῦτο.
```
~~1881 Westcott-Hort New Testament

```text
oboedite praepositis vestris et subiacete eis ipsi enim pervigilant quasi rationem pro animabus vestris reddituri ut cum gaudio hoc faciant et non gementes hoc enim non expedit vobis
```
~~Jerome's Vulgate

```text
Obey them that have the rule over you, and submit yourselves: for they watch for your souls, as they that must give account, that they may do it with joy, and not with grief: for that is unprofitable for you.
```
~~King James Version
