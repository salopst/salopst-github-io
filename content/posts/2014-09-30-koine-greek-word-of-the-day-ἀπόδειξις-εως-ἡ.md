---
author: kword
comments: false
date: 2014-09-30T12:05:28+00:00
layout: post
slug: koine-greek-word-of-the-day-ἀπόδειξις-εως-ἡ
title: 'Koinē Greek Word of the Day: ἀπόδειξις, εως, ἡ'
wordpress_id: 726
categories:
- Language
tags:
- Greek
- Bible
- words
- koine
- κοινή
---

# ἀπόδειξις, εως, ἡ

apodeixis
Noun, Feminine
*demonstration, proof; a showing off.*
Strong's: **585**

1 occurrence in GNT

```text
ἀποδείξει (N-DFS), 1 Corinthians 2:4
```
# 1 corinthians 2:4

```text
καὶ ὁ λόγος μου καὶ τὸ κήρυγμά μου οὐκ ἐν πειθοῖ σοφίας ἀλλ’ ἐν ἀποδείξει πνεύματος καὶ δυνάμεως,
```
~~1881 Westcott-Hort New Testament

```text
et sermo meus et praedicatio mea non in persuasibilibus sapientiae verbis sed in ostensione Spiritus et virtutis
```
~~Jerome's Vulgate

```text
And my speech and my preaching was not with enticing words of man's wisdom, but in demonstration of the Spirit and of power:
```
~~King James Version
