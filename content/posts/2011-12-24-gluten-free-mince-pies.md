---
author: mary.yearl
origin: wordpress
comments: true
date: 2011-12-24T19:00:44+00:00
layout: post
slug: gluten-free-mince-pies
title: Craving a mince pie but you're following a gluten-free diet?
wordpress_id: 382
categories:
- Gluten free
- Recipes
tags:
- food
- baking
- Christmas
- gluten-free
- mince pie
- pie
---

{{< image src="/wp-uploads/GF_mince_pies.scaled1000.jpeg" alt="Mince Pies" position="center" style="border-radius: 50px;" >}}


How did we do it? First of all, I picked up a new [kitchen scale](http://www.polyvore.com/bamboo_kitchen_scale/thing?id=37829006). It's digital and switches easily between metric and US standard units. This makes it easy to fool around with different types of gluten-free flour since **the only thing that really matters is that there be double the mass of flour to fat.**

Right. What about those pies?

- Cranberry mince pies. For the filling, I pretty much followed the recipe given [here](http://oaktree-cottage.blogspot.com/2008/12/nigellas-cranberry-mince-pies.html).

- I also made traditional mince pies (again, GF pastry) inspired by [this](http://britishfood.about.com/od/christmas/r/mincemeat.htm) recipe.

I may have added some prunes into the mix and also currents. I did not use any candied fruit and had to substitute Granny Smiths for Bramleys.

We are fortunate to have access to a great farm stand that sells excellent quality locally-raised meat at reasonable prices. I bought a large block of [suet](http://en.wikipedia.org/wiki/Suet) from them. When I need a bit, I just take the block out of the freezer, grate what I need, and put the rest back for later.

For the pastry, I played around a bit. In the last batch, I combined roughly equal parts of:
	
- Rice flour (white and brown)
- Garbanzo (chickpea) / fava bean flour
- Added in a bit of corn starch

The above three ingredients came to about 150gm.

Then I put in a couple of teaspoons of:

- xanthan gum
- baking powder


Now for the fat. I'm not sure if it made much difference, but I froze all of my fat (lard and butter).

**Total fat used: 75 gm**


Sometimes I used only butter, sometimes a combination of lard and butter. The latter was a bit crumblier, as you might imagine.

Rather than cut the fat through the flour, I grated it, or at least sliced it into tiny pieces before cutting through the flour.

I did use my fingers for less than a minute to crumble the mixture together, then added the cold water-- about 5-6 tablespoons and mixed everything together with a fork.

One the mixture held together, I separated it into two balls and put it in the fridge.

Half and hour later, I was ready to roll.

One note of warning: gluten-free pastry is not as pliable as regular pastry because of the lack of gluten. For me, the mini pies worked really well because each shell was small enough that it did not fall apart. When making a large apple pie, I found I had to do a bit a patching because of cracks in the dough.

I did find that using buckwheat flour sometimes helped with the pliability issues, but one has to be careful since the strong taste does not suit all types of filling.

Other links re. gluten-free flours and pastry (in no particular order). I took my ideas from several of these. As should be clear from the above, I am still experimenting. Even though I've been happy with some of the results, I'm eager to test other methods so that I can find a comfort zone within the gluten free world. A lot of the recipes linked below look quite involved. The great thing about using a scale is that there is less fussing with small individual measurements. Play away!

### References
[http://www.tarteletteblog.com/2010/04/recipe-gluten-free-puff-pastry.html](http://www.tarteletteblog.com/2010/04/recipe-gluten-free-puff-pastry.html)

[http://www.artofglutenfreebaking.com/2010/03/puff-pastry-gluten-free/](http://www.artofglutenfreebaking.com/2010/03/puff-pastry-gluten-free/)

[http://glutenfreegirl.com/gluten-free-rough-puff-pastry/](http://glutenfreegirl.com/gluten-free-rough-puff-pastry/)

[http://www.flour-arrangements.com/2010/10/gluten-free-pastry.html](http://www.flour-arrangements.com/2010/10/gluten-free-pastry.html)

[http://www.ellenskitchen.com/faqs/glutfree.html](http://www.ellenskitchen.com/faqs/glutfree.html)
