---
author: admin
origin: wordpress
comments: true
date: 2013-05-05T22:42:21+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
slug: masa-chocolate-chip-cookies-gluten-free-of-course
title: Masa chocolate chip cookies (gluten-free, of course)
wordpress_id: 474
draft: false
categories:
- Gluten free
- Recipes
tags:
- food
- gluten free
- recipe
---

It's been a while since we've baked cookies in this house. In honour of 5 de mayo, we decided to try our hand at some masa-based cookies. The result? Gluten free goodies that went down in the GF and non-GF corners of the house, and that were appealing to the sweet tooth and those who prefer things less sweet.

{{< image src="/wp-uploads/GF-masa-chocolate-cookies.jpg" alt="Gluten free masa and chocolate-chip cookies" position="center" style="border-radius: 50px;" >}}

The recipe I used for guidance was [this](http://www.celiac.com/articles/477/1/Masa-Chocolate-Chip-Cookies-Gluten-Free/Page1.html) one.

## Ingredients
We tend to reduce the added sugar by about one half, so the resulting recipe was (roughly!):

- 225g (1 c.) unsalted butter
- 115g (1/2 c.) granulated sugar
- 105g (1/2 c.) brown sugar (we used dark brown sugar)
- 2 large eggs
- 1 tsp. vanilla
- 250g (2 c.) masa harina
- 1 tsp. baking soda
- pinch salt (optional)
- One bag (~340g or 2c.) chocolate chips (and/or nuts, other desired additions)

## Method
- Have all ingredients at room temperature; the butter should be soft (in our cold kitchen, I put ours in the microwave for 10-15 seconds)

- Preheat oven to 375 degrees F

- Blend together butter and sugar until light and fluffy; then add eggs and vanilla

- Gradually add masa mixture until just blended

- Mix in chocolate chips (and/or other additions)

- Drop small balls of dough onto a greased cookie sheet

- Bake for ~ 10 minutes, until golden brown
