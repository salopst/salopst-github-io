---
title: "what happens why you buy from your own Amazon Affiliate links?"
slug: buying-from-your-own-amazon-affiliate-links
date: 0101-01-001T01:01:01+01:01
layout: post
draft: false
author: yearluk
origin: hugo
description: "A little experiementation into advertising and buying products on amazon... AKA buying my own shit."
categories:
- Tech
- Products
tags:
- webdev
- SEO
- Amazon
---

I don't think anyone is making bank with Amazon affiliate links. Still, is there something you can claw back by grabbing an affiliate link, smacking it on the web somewhere and using that yourself before a purchase?

Here are some products that I've either purchased or am thinking about purchasing.

## Gifts

### Lynx Africa and Marmite
<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=GB&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=yearlus-21&language=en_GB&marketplace=amazon&region=GB&placement=B08H5TJXYY&asins=B08H5TJXYY&linkId=fef909ca25abd328a42546015400a9c7&show_border=true&link_opens_in_new_window=true"></iframe>

-----
-----

## Food
### Hela German Sauce Curry Ketchup BBQ Currywurst Tomato Fries Burger HOT Spicy (800ml)
<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=GB&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=yearlus-21&language=en_GB&marketplace=amazon&region=GB&placement=B003RGCIWM&asins=B003RGCIWM&linkId=1b32ac21131325f8c66b3f90e01b7c7c&show_border=true&link_opens_in_new_window=true"></iframe>


### Ras El Hanout Moroccon spice blend
<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=GB&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=yearlus-21&language=en_GB&marketplace=amazon&region=GB&placement=B06XBBJBM8&asins=B06XBBJBM8&linkId=fc521ff8fef6a8ab60f857bfc3ca8031&show_border=true&link_opens_in_new_window=true"></iframe>

-----
-----
## Home and garden

## Daffodil bulbs
<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=GB&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=yearlus-21&language=en_GB&marketplace=amazon&region=GB&placement=B00F6YUP5A&asins=B00F6YUP5A&linkId=6b3a54b411861f4fc128dafa1332dd56&show_border=true&link_opens_in_new_window=true"></iframe>


## Mixture of Spring Bulbs
<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=GB&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=yearlus-21&language=en_GB&marketplace=amazon&region=GB&placement=B00F3X9V04&asins=B00F3X9V04&linkId=b3707cba1adb0291d0dd1d78159d9c24&show_border=true&link_opens_in_new_window=true"></iframe>
-----
-----

## Tech and gadgets
### Crucial 1TB SSD
<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=GB&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=yearlus-21&language=en_GB&marketplace=amazon&region=GB&placement=B07YD579WM&asins=B07YD579WM&linkId=d122c37b66a68fd72dd55fabb33d23a2&show_border=true&link_opens_in_new_window=true"></iframe>


### Focusrite Scarlett 2i2 3rd Gen USB Audio Interface
<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=GB&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=yearlus-21&language=en_GB&marketplace=amazon&region=GB&placement=B07QR73T66&asins=B07QR73T66&linkId=df14342d30b7805902b14d9c6913a3b3&show_border=true&link_opens_in_new_window=true"></iframe>


### Moondrop Starfield Carbon Nanotube Diaphragm Dynamic Earphone
<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=GB&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=yearlus-21&language=en_GB&marketplace=amazon&region=GB&placement=B082NQFW16&asins=B082NQFW16&linkId=ed0932a9821c860080c61775604006e8&show_border=true&link_opens_in_new_window=true"></iframe>

-----
-----

## Tools and houhold
### Suehiro CERAX 1010 Whetstone: Medium (#1000) grit. 205 x 73 x 29 mm
<iframe style="width:120px;height:240px;" marginwidth="0" marginheight="0" scrolling="no" frameborder="0" src="//ws-eu.amazon-adsystem.com/widgets/q?ServiceVersion=20070822&OneJS=1&Operation=GetAdHtml&MarketPlace=GB&source=ss&ref=as_ss_li_til&ad_type=product_link&tracking_id=yearlus-21&language=en_GB&marketplace=amazon&region=GB&placement=B01E5AKQ04&asins=B01E5AKQ04&linkId=8cb4a50c5f5da37ddf5617d70cf8998e&show_border=true&link_opens_in_new_window=true"></iframe>
