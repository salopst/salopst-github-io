---
author: admin
origin: wordpress
comments: true
date: 2012-02-14T15:07:14+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: chocolate-peanut-butter-cups
title: Chocolate peanut butter cups
wordpress_id: 388
categories:
- Recipes
- Food
tags:
- chocolate
- peanut butter
- recipe
---

The boys like peanut butter and they like chocolate, but the peanut butter cups from the store are too sweet and too salty. Moreover, we like bittersweet chocolate, so we decided to make our own. After all, I had a bunch of mini muffin cups I'd picked up in the post-Christmas sale at IKEA.

{{< image src="/wp-uploads/2012-02-14-Peanut-butter-cups.jpg" alt="Peanut butter cups" position="center" style="border-radius: 50px;" >}}

This is a project the kids can help with: mine not only helped fill the cups, they were wholly responsible for the decorations.

## Ingredients
- 1 cup creamy peanut butter, separated into two equal parts
- 1 1/2 tbsp. butter
- 1 tsp. honey
- One bag (about 2 cups) dark chocolate chips
- Some extra semi-sweet chips (to make a generous 2 cups of chips)
- Roughly 4 oz. of milk chocolate (this I might remove from the recipe next time)
- Decorations / Jimmies / Sprinkles
- Mini muffin cups (sized to suit)

To be sure, one could substitute soy butter, sunflower butter, or something similar for those cases where peanut butter is unwelcome.

## Method

- In a double boiler,[^1] combine chocolate and about half of the peanut butter

- Stir the chocolate peanut butter mixture until smooth

- In a separate bowl, combine 1/2 cup peanut butter, honey, and butter

- Mix until smooth (I put the bowl over the double boiler for a few minutes to speed up the process)

- Using a teaspoon, drip the melted chocolate into the bottom of the muffin cups until just covered.

- At the end of this step, I rolled the cups around just enough to bring the chocolate up the sides, but this is not necessary

- Carefully put a small dab of the peanut butter mixture in the middle of each muffin cup. Be careful not to let the peanut butter touch the sides!

- Drip the remaining chocolate over the peanut butter and fill the cup.

- Finally, decorate if desired and put them in the refrigerator to set.


[^1] **A microwave might work, too.**
