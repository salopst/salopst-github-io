---
author: admin
origin: wordpress
comments: true
date: 2016-04-25T19:57:52+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: rescue-doggie-diary-week-2
title: Rescue Doggie Diary -- Week 2
wordpress_id: 1033
categories:
- Dog
- Good life
tags:
- animal
- behavior
- canine
- dog
---

{{< image src="/wp-uploads/2016-04-26-monty-well-meadow.jpg" alt="Monty free on Well Meadow" position="center" style="border-radius: 50px;" >}}

- First week here: [http://stephen.yearl.us/rescue-doggie-diary](http://stephen.yearl.us/rescue-doggie-diary)

## 2016-04-25 Monday
Another walk through Well Meadow, along the eastern bank of the Severn, late this morning. All pretty standard stuff. Monty's leash walking is getting better. Did come across a male black lab-- off leash, of course-- who came over to invade his space again. Monts did kick off a little, as the other dog refused to be called back to his owner.

More apologies from the other dog owner. Wondering if Monts feels more constrained/threatened by being on a leash when the others are not? Tricky thing there is, how can I trust him off leash if he does not show complete awesomeness off it? Maybe try a long trailing thing?

More baseball cricket action in the yard late afternoon as I attached some wire stock fencing to the main gate. It was rather an unintentional game, for I had left the Montster in the house, back door shut. And that sort of reminded me that yesterday, on the way back from cricket, I swore that I had closed an internal door. Apparently the dawg opens lever -handled doors.

And he eats green olives. Smart fellow.


## 2016-04-26 Tuesday (wrote Mr. Kipling)
So the fella is still following me around the house, from room to room. Not pushy, and hasn't gotten in the way yet, even in the darkness of night. He has taken himself into the bedroom a couple of times, but this is never really for very long.

Monts behaved somewhat beautifully when meeting a couple of cats... and he had some free run chasing sticks in a public place. Admittedly there was no one around, and I could see a good distance if anyone were to come into our world, but still. Gotta start the trust somewhere.

## 2016-04-27 Wednesday
No structured walk today, just fetch-the-ball in the garden.

And early-evening the dog that has too many names[^0] just went run about again. Irony of all Ironies when I was adding more stock fence to the gateway out front. So I took to driving around the village all slow and shit looking in people’s gardens, and at kids and dog walkers looking for signs of distress.

Next up on Highley Facebook Page will be "watch out for paedo in blue car!” ☺

I did spot him up by the old telephone exchange, and he did come back and jump in the car when I opened the door and called. That’s good. But still. GRRRR! Assdog has no road sense at all. I don’t mind him going out by himself. He just needs to let me know where’s he’s going, who he will be with, and what time he will be back. Otherwise he is GROUNDED!

[^0] Montgomery, Montesquieu, Monty, Monts, Monticello, Montessori, Montreal, the Montster


## 2016-04-28 Thursday
So he has the idea that balls and sticks are to be fetched and brought back; balls are dropped for a human to re-throw, sticks are for holding on to, and playing tug. Dropping something on command is not something that he is used to, but I'm sure we can use his doing so with a ball to advantage there. No concept of "leave it" either. I go to pick up a non-ball, and he's on it. Again using the ball knowledge as a base, I think there is something definite there from which to work.

Lots of anal licking... some anti-worm medicine ordered.


## 2016-04-29 Friday
Met a couple of doggies off leash down by the Donkey Bridge, and all went where there. Encountered the same dogs on another loop, and again no issue. No hackles at all second time around as Montster was sort on involved in trying to understand "drop" (stick), "leave it", and "pick up". This with one of the dogs-- some kind of rough haired terrier-- somewhat in his face.

Monts walking again excellently at heel, coming back when called (but not yet tested under stress), and fetching stick on a trailing lead. All good stuff!

Walk through Bridgnorth High Street.... no probs with other dogs, yelling kids out of school, and people eating pasties and chips and shit. Bit of shopping in TFM, and again sitting nicely. Stopping and sitting at road crossings, all good.

## 2016-05-30/31 Saturday/Sunday
Brook walks, but for the most part just hangin' in the garden cutting grass and getting fence posts in place. Lots of fetch and recall. Being cautious and locking the back door when I am not paying attention to him-- since he can open the door. Git! Grabbed a 1/2Kg of cheddar off the counter on Sunday. Grrr. Cheese banditry will not be tolerated :-)
