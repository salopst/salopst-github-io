---
author: admin
origin: wordpress
comments: true
date: 2012-10-12T19:28:04+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
draft: false
slug: gf-oatcakes
title: GF Oatcakes
wordpress_id: 397
categories:
- Gluten free
- Recipes
tags:
- gluten-free
- oat bread
- recipe
- food
---

Well it does not get any simpler than this, well maybe the [granola](http://stephen.yearl.us/gluten-free-granola/) is, but it is yet another example of a face-plant. Almost every trip to the supermarket in the UK led to the purchase of several boxes of oatcakes. Why? Why did we not think of making these before? Again the impetus has been going gluten-free since, as you might have guessed, the majority of easily/commercially available oatcakes have wheat flour in them.

## Ingredients
- 225g gluten-free oats
- 10g butter
- 1/2 tsp salt (nix if using salted butter)
- 1/2 tsp bicarbonate of soda
- 100ml warm water

## Method
1. ‘Biltz’ the oats in a food processor to break them up into a meal/flour
1. Add other dry ingredients, and give the processor a few spins to mix
1. pre-heat oven to 180 ºC ≈ 350 ºF
1. Add warm water to the butter to melt, and add liquid to dry ingredients to form a dough
1. Roll out the dough to ca. 0.5cm thickness, cut using desired size of cookie cutter
1. Place oatcakes on a baking sheet lined with parchment paper and bake for about 25 minutes

NOTE: It is important to move quickly with the oatmeal dough as it dries out VERY quickly.

Set to cool on a rack, and enjoy!

{{< image src="/wp-uploads/GF-oatcakes.jpg" alt="" position="center" style="border-radius: 50px;" >}}


The following updated ratios lead to a more malleable dough. Much improved over the above, and just perfect with some damson jam and a glob of clotted cream.

## Ingredients (2015-10-27)
- 250g gluten-free oats
- 25g butter
- 1/2 tsp salt (nix if using salted butter)
- 1/2 tsp bicarbonate of soda
- 1/2 teaspoon xanthan gum
- 130ml warm water
