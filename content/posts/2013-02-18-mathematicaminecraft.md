---
author: admin
origin: wordpress
comments: true
date: 2013-02-18T17:28:22+00:00
lastMod: 2021-09-21T02:22:00+01:00
layout: post
slug: mathematica-minecraft
title: Mathematica/Minecraft
wordpress_id: 58
categories:
- Code
- Games
tags:
- boys
- code
- mathematica
---

Since summer I've been running a little minecraft server on a VPS in the UK. It is a pretty low-end system, and I didn't imagine more than a handful of the primary school chums of 74111101 playing on it. A nice, secure, white-listed place away from the craziness of the net. As is inevitable with such things, numbers grew and shrank. I was waiting for a stable population before setting something more concrete-- allocating relevant resources, branching out into mods and so forth.

Well, the population is yet to stabilise. I neglected to factor-in the the volatility of elementary school relationships, and how flat-out nasty little boys can be. Some of the wonderful worlds created by x player have been thoroughly destroyed by y and z.... and on and on. EVERYONE wants to be an 'op', and of course it just take one rogue op to spoil the fun for all. Why a single user on a multiplayer server would want to ban everyone else is beyond me. OK, so you have the server to yourself. Have fun. Next time try playing off-line.

I'd like to say that my boys have not been involved in this miniature world politicking, but they have not. They understand the power of root, but still 74111101 has handed out op privileges like candy, and has frequently found himself banned from the server I set up for him. And even with recognition of confirmation bias, I think I am right to say that I 'picked' those kids who were going to be trouble IRL/AFK as being trouble online. If I were a sociologist....

Anyway the following mathematica code came my way via email... a cousin's friend. It is a little sluggish using Mathematica 8.0.4.0 32bit on OS X 10.8.2, but then I am giving over a fair amount of CPU/GPU cycles to CERN's computing grid, and that is also taking about 25% of my network throughput right now.

[This is the Mathematica code](http://stephen.yearl.us/minecraft-in-mathematica/).
