---
author: mary.yearl
origin: wordpress
comments: true
date: 2012-01-30T14:20:41+00:00
layout: post
slug: sr-71-model-rocket-construction
title: SR-71 Model Rocket Construction
wordpress_id: 384
categories:
- Rockets
tags:
- rockets
- craft
- models
---


{{< image src="/mary-wp-uploads/img_2515.png" alt="Model SR-71 Blackbird Rocket" position="center" style="border-radius: 50px;" >}}

> TiGeRrrr gave me a model SR-71 Blackbird for Christmas and Aye-Dye built it this weekend. Thank you Tiger and AyeDye. I hope we can launch it on Tuesday because that's when we get home from school early. Mimi found out we can launch it at the baseball diamond. My favorite plane is the F-14A Tomcat. Sammy's is the Blackbird because it is faster but it cannot land on an aircraft carrier like the CVA-12 Hornet we went on in Alameda California.
>
>~JBAY
-----

The rocket was from [Estes Rockets](http://www.estesrockets.com/),  probably the biggest name in the US for amateur rocketry at this very introductory level. One can pick up pretty much everything one needs for an >cough< explosive afternoon of fun for less than $50. The squeals of delight alone is worth the price admission. So, below in a few pics, the "soup to nuts" o' the project.

I'd say putting this together was pretty darned simple. It's not exactly.... drum-roll... rocket science. It can get a bit fiddly, and sharp blades and patience and fast-curing adhesives do not work well in the hands of any little human less than say 10 years old. One slip with a razor blade and the fun sort of goes away does it not? With all the adhesive curing and the sanding and the spray-painting and such we were all pretty much ready to fly after about 48 hours.




### 1. The model and the work space


{{< image src="/mary-wp-uploads/img_2401.png" alt="Project Workspace" position="left" style="border-radius: 50px;" >}}

-----
-----
-----

{{< image src="/mary-wp-uploads/img_2460.png" alt="Project Tools" position="right" style="border-radius: 50px;" >}}


<!--
OOH. Comments in Markdown!
Not sure about the table format...
| ![01_workspace](/mary-wp-uploads/img_2401.png)  | ![02_tools](/mary-wp-uploads/img_2460.png)  |
|---|---|
-->

Not all tools are pictured here... for example, additional cutting tools such as scissors, a general purpose utility knife and a hacksaw. In some of the other pictures here and in the [gallery](/gallery/sr-71-blackbird-model/) for this project there are clamps of various sorts, and spray paints.






### 2. Cutting wings, fins, and rudders


{{< image src="/mary-wp-uploads/img_2404.png" alt="Sand the surfaces" position="left" style="border-radius: 50px;" >}}

-----
-----
-----

{{< image src="/mary-wp-uploads/img_2414.png" alt="Dry-fit the wings" position="left" style="border-radius: 50px;" >}}

-----
-----
-----

{{< image src="/mary-wp-uploads/img_2418.png" alt="Glued wings" position="left" style="border-radius: 50px;" >}}


Wings, rudders and stabilizing fins (not present on an SR-71, but present here because it is a model rocket) are cut from ca. 1.5mm thick balsa wood sheets. Here we see the pre-etched sheets, and "dry alignment" of the wing components.

of the inner and outer wings, and the various components glued into their respective configuration. It seems unlikely that the 1.5mm edges of the wood will adhere, but the wood blue we used produces a bond stronger than the balsa itself. If the wing assemblies split anywhere, it will not be at the glued interface.




### 3. Assembling "inner" wings, and skinning


{{< image src="/mary-wp-uploads/img_2435.png" alt="Inner wing attatchment" position="left" style="border-radius: 50px;" >}}

-----
-----
-----

{{< image src="/mary-wp-uploads/img_2436.png" alt="Inner wing attatchment" position="left" style="border-radius: 50px;" >}}

-----
-----
-----

{{< image src="/mary-wp-uploads/img_2459.png" alt="Wing skin" position="left" style="border-radius: 50px;" >}}


Of course, the SR-71 has a [delta wing ](http://en.wikipedia.org/wiki/Delta_wing)so the term inner wing here is a misnomer. It really is an extension of the fuselage. The inner wings here and the central cardboard cylinder are later "skinned"

with a thin sheet of cardboard to give the impression of a uni-body design. To the lateral edges of these inner wings are attached the [nacelles](http://en.wikipedia.org/wiki/Nacelle).

It was curiously hard to get those elastic bands to hold those 1.5mm wings to a circular surface. It helped that we allowed the poly cement to become a little tacky before the wings were attached, but still there was some slippage, and one wing rides a little higher than another. This is not obvious once the skin is applied.

The edges of the skin were later sanded flat to the balsa wood on edges that were to be bonded to the nacelles.




### 4. Nacelles

{{< image src="/mary-wp-uploads/img_2442.png" alt="Nacelle intake exhaust" position="left" style="border-radius: 50px;" >}}

-----
-----
-----

{{< image src="/mary-wp-uploads/img_2428.png" alt="Cutting air intake exhaust in" position="left" style="border-radius: 50px;" >}}

-----
-----
-----

{{< image src="/mary-wp-uploads/img_2430.png" alt="Completed nacelle" position="left" style="border-radius: 50px;" >}}

-----
-----
-----

{{< image src="/mary-wp-uploads/img_2480.png" alt="Attaching the nacelles" position="left" style="border-radius: 50px;" >}}

-----
-----
-----

The air intake spikes and nacelle exhausts are cut from a single piece of plastic. The carboard cylinder was a little wider that the plastic components, so the circumference of these was increased with two wraps of masking tape. Again the problem of gluing a narrow surface to the curved edge of a cylinder, but lessons were learned from earlier and some clamps were used instead. Arguably this was easier than when adding the inner wings to the central tube, because the inner wings were that much thicker after the addition of the wing skin.

#### The rudders were later glued onto the nacelles at a slight inward angle, and counterbalancing fins added to the underside of the model:

{{< image src="/mary-wp-uploads/img_2487.png" alt="17. Attaching the rudders" position="center" style="border-radius: 50px;" >}}


#### And after inspecting the drying of the black matte primer coat

{{< image src="/mary-wp-uploads/img_2494.png" alt="1st coat of primer" position="center" style="border-radius: 50px;" >}}


#### Sanding, and the application of two coats of matte black spray we have:

{{< image src="/mary-wp-uploads/img_2506.png" alt="2nd coat of matte finish" position="center" style="border-radius: 50px;" >}}





### 5. Application of decals


| ![21_decals](/mary-wp-uploads/img_2509.png) | ![22_decals1](/mary-wp-uploads/img_2510.png) | ![23_decals2](/mary-wp-uploads/img_2512.png) |
|---------------------------------------------|----------------------------------------------|----------------------------------------------|

-----
-----
-----

Some of these are small and damned fiddly. The sort of thing that makes one chew ones tongue in concentration. Following the application of decals and and final coat of clear matte spray paint we have:

{{< image src="/mary-wp-uploads/img_2513.png" alt="Checking alignment" position="left" style="border-radius: 50px;" >}}



-----
-----
-----



If ye have a Mac and you need batch processing, like say the images in this post try sips in Terminal.app:
`$ sips -Z 800 *.JPG -i -s format png --out ~/Desktop/SR-71-rocket-imgs/png`

Which basically says take every JPEG in the current directory, make the longest axis 800 pixels (preserving aspect ratio), also create a PNG version, and copy the results to a directory on my desktop. If you are running Windows go get [ImageMagick](http://www.imagemagick.org) at once. In fact, regardless of OS, go get it. And donate a few $$, ££, €€, whatever.
