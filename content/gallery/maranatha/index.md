---
title: "Some photos from around the house"
date: 2011/2020
author: "system"
origin: "hugo"
draft: false
categories:
  - Index
  - Gallery
  - "Pictures and Photos"
  - "Good Life"
tags:
  - Maranatha
  - gallery
---

{{< gallery dir="." />}} 
{{< load-photoswipe >}}
-----
-----
-----
{{< sjy2-gallery >}} 
